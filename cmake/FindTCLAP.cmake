# - Find TCLAP
# Find the TCLAP library
# This module defines
#  TCLAP_INCLUDE_DIR, where to find TCLAP headers, etc.
#  TCLAP_FOUND, If false,.

FIND_PATH(TCLAP_INCLUDE_DIR tclap/Arg.h)


# handle the QUIETLY and REQUIRED arguments and set TCLAP_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TCLAP DEFAULT_MSG TCLAP_INCLUDE_DIR)

