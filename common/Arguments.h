#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <unordered_set>
#include <string>
#include <vector>
#include <regex>

#include <vigra/unittest.hxx>

/**
 * @brief framesFromArgument
 * @param argument given argument for frame list
 * @return a list of discrete frame numbers
 *
 * This function creates a discrete list of frame indices.
 *
 * The input to the function can look like
 * 1,2,5:7,10
 *
 * which then returns a list of
 *
 * [1,2, 5,6,7, 10]
 *
 *
 * The input
 * 0:100:10
 *
 * will produce
 * [0,10,20,30,40,50,60,70,80,90,100]
 *
 * 0:+5:10
 * will produce
 * [0, 10, 20, 30, 40, 50]
 *
 *
 * Function throws exception if input fragments are not recognized.
 */
std::vector<int> framesFromArgument(const std::string &argument)
{

    /* first split the string up into fragments, delimited by a comma */
    std::istringstream iss(argument);
    std::list<std::string> fragments;
    do {
        std::string sub;
        std::getline(iss, sub, ',');
        sub.erase(std::remove_if(sub.begin(), sub.end(), isspace),
                  sub.end());

        /* if the segment is empty, we don't need to store it */
        if(sub.empty())
            continue;

        fragments.push_back(sub);
    } while(iss);


    /* now try to match each fragment as a frame specifier */
    std::unordered_set<int> frameSet;
    std::regex reRange(R"(^([0-9]+):?(([\+]?)([0-9]+))?:?([0-9]+)?$)");

    enum {
        START = 1,
        END_SIGN = 3,
        END,
        STEP = 5
    };

    for(auto fragment: fragments) {

        std::smatch m;
        if(!std::regex_match(fragment, m, reRange))
            throw std::runtime_error("frame specifier is not valid: " + fragment);

        /* single number */
        if(!m[END].length() && !m[STEP].length()) {
            int frame;
            std::istringstream(m[1]) >> frame;
            frameSet.insert(frame);
        } else {

            int frame1, frame2, step = 1;
            std::istringstream(m[START]) >> frame1;
            std::istringstream(m[END]) >> frame2;

            /* check if we need to read the stepping parameter */
            if(m[STEP].length()) {
                std::istringstream(m[STEP]) >> step;
            }

            if(m[END_SIGN].length()) {
                frame2 = frame1 + frame2;
            }

            if(frame1 > frame2)
                throw std::out_of_range("First frame must be smaller than second frame: "
                                        + std::to_string(frame1)
                                        + "<="
                                        + std::to_string(frame2));

            frameSet.reserve(frame2-frame1);
            for(int i = frame1; i <= frame2; i += step) {
                frameSet.insert(i);
            }
        }
    }


    std::vector<int> frames;
    std::copy(frameSet.begin(), frameSet.end(), std::back_inserter(frames));
    std::sort(frames.begin(), frames.end());

    return frames;

}

void test_frame_argument() {

    try {

        auto test0 = framesFromArgument("0,1:+8:4,10");
        should(test0.size() == 5);
        should(test0[0] == 0);
        should(test0[1] == 1);
        should(test0[2] == 5);
        should(test0[3] == 9);
        should(test0[4] == 10);


        auto test1 = framesFromArgument("0,1,5:6,10");

        should(test1.size() == 5);
        should(test1[0] == 0);
        should(test1[3] == 6);
        should(test1[4] == 10);


        auto test2 = framesFromArgument("0:99,90:199");
        should(test2.size() == 200);
        should(test2[0] == 0);
        should(test2[100] == 100);
        should(test2[199] == 199);
        should(test2.front() == 0);
        should(test2.back() == 199);

        auto test5 = framesFromArgument("0:100:10");
        should(test5.size() == 11);
        should(test5[0] == 0);
        should(test5[10] == 100);

        /* test cases where something should go wrong */
        try {
            auto test = framesFromArgument("0:-99,1");
            should(false);
        } catch (std::runtime_error & e) {}

        try {
            auto test = framesFromArgument("-1,2");
            should(false);
        } catch (std::runtime_error & e) {}

        try {
            auto test = framesFromArgument("50:20");
            should(false);
        } catch(std::out_of_range & e) {}


    } catch (std::exception &e) {
        std::cout << "[TEST] Failed: " << e.what() << std::endl;
    }
}


#endif // ARGUMENTS_H
