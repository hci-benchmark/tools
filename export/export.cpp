#include <iostream>
#include <tclap/CmdLine.h>
#include <string>
#include <vector>
#include <cctype>
#include <regex>
#include <unordered_set>

// Hookers Interface
#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/NumericTypes.h>

// Hookers Toolbox
#include <hookers/toolbox/export/Sources/ExporterSource.h>
#include <hookers/toolbox/export/Sources/MapSource.h>
#include <hookers/toolbox/export/Sources/RawSource.h>
#include <hookers/toolbox/export/Sources/ViewSource.h>
#include <hookers/toolbox/export/Sources/PosesSource.h>
#include <hookers/toolbox/export/Sources/ContourSource/ContourSource.h>

#include <hookers/toolbox/export/Sinks/ExporterSink.h>
#include <hookers/toolbox/export/Sinks/MapExporterSink.h>
#include <hookers/toolbox/export/Sinks/HDFExporterSink.h>
#include <hookers/toolbox/export/Sinks/PNGExporterSink.h>
#include <hookers/toolbox/export/Sinks/VideoExporterSink.h>
#include <hookers/toolbox/export/Sinks/DisplaySink.h>

#include <hookers/toolbox/Sampling/GroundtruthSource.h>
#include <hookers/toolbox/Projection/OpenGLProjection.h>
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>

#include <vigra/unittest.hxx>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "../common/Arguments.h"

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

std::string ExporterSink::m_path(".");
std::string ExporterSink::m_sequenceName("unamed_sequence");




// Similar to quadratic prior, but the residuals are relative to a second
// parameter, i.e.
//      res = (x1+prior) - x2      
template<int N>
struct binaryPrior
{

    typedef ceres::AutoDiffCostFunction<binaryPrior<N>, N, N, N> AutoDiff;

    double prior[N];
    double weight[N];

    binaryPrior(double * d, double * w)
    {
        for (int ii = 0; ii < N; ++ii)
        {
            weight[ii] = 1.0/w[ii];
            prior[ii] = d[ii];
        }
    }

    template<class T>
    bool operator()(const T* const x1, const T* const x2, T* res) const
    {
        for (int ii = 0; ii < N; ++ii) {
            res[ii] = T(weight[ii]) * (x1[ii] + T(prior[ii]) - x2[ii]);
        }
        return true;
    }
};


int main(int argc, char ** argv){


//    /* test some stuff here.
//     * comment me out if not required */
//    {
//        test_frame_argument();
//        return 0;
//    }


    TCLAP::CmdLine cmd("hookers exporter", ' ', "0.1");

    TCLAP::ValueArg<std::string> projectNameArg(
                std::string(), "project",
                "project name (eg. main)",
                false,
                std::string(),
                "string",
                cmd);


    /* --- main modes */
    TCLAP::ValueArg<std::string> sequenceModeArg(
                std::string(), "sequence",
                "sequence name (eg. 0_0000)",
                true,
                std::string(),
                "string",
                cmd);

    /* viewa*/
    TCLAP::MultiArg<std::string> viewsArg(
                "v", "view",
                "a view name that can be 'stereo' or 'base',",
                false,
                "string",
                cmd);

    /* --- stuff */
    TCLAP::MultiArg<std::string> mapsArg(
                "m", "map",
                "a map name that can be found in Frames, eg. 'depth'",
                false,
                "string",
                cmd);

    TCLAP::MultiArg<std::string> generatorsArg(
                "g", "generator",
                "a data generator name, eg. groundtruth",
                false,
                "string",
                cmd);

    TCLAP::SwitchArg metaArg(std::string(),
                     "meta",
                     "write out meta information from frame",
                     cmd,
                     false);


    /* --- common arguments */
    TCLAP::SwitchArg useFramePoseEstimationArg(std::string(),
                     "use-pose-estimation",
                     "by default the groundtruth generator uses stored poses. Switch enables use of solvePnP instead.",
                     cmd,
                     false);

    TCLAP::SwitchArg useBundleAdjustmentArg(std::string(),
                     "use-bundle-adjustment",
                     "by default the groundtruth generator uses stored poses. Switch enables use of bundle adjustment.",
                     cmd,
                     false);

    TCLAP::SwitchArg storeFramePosesArg(std::string(),
                     "store-frame-pose",
                     "setting this flag stores the calculated frame poses to the sequence files",
                     cmd,
                     false);


    TCLAP::ValueArg<std::string> framesArg(
                std::string(), "frames",
                "list of frames to be exported. Examples: all; 0-25,30,31,200-500",
                true,
                "all",
                "comma separated list of frames",
                cmd);

    TCLAP::ValueArg<std::string> bundleAdjustmentFramesArg(
                std::string(), "ba-frames",
                R"DESC(
                list of frames to be used in bundle adjustment. Examples: all; 0-25,30,31,200-500.
                If nothing provided, the set of "frames" arg will be taken.
                )DESC",
                false,
                std::string(),
                "comma separated list of frames ",
                cmd);

    TCLAP::SwitchArg bundleAdjustmentTriangulateArg(
                std::string(),
                "ba-no-triangulate",
                "don't use unlinked tracks by triangulating them using the provided camera pose estimates",
                cmd,
                true);

    TCLAP::SwitchArg bundleAdjustmentStoreVisArg(
                std::string(),
                "ba-store-vis",
                "store visualisation of bundle adjustment",
                cmd,
                false);

    TCLAP::SwitchArg bundleAdjustmentUseStereoView(std::string(),
                     "ba-use-stereo-view",
                     "include points from the right view in the bundle adjustment",
                     cmd,
                     false);

    TCLAP::SwitchArg bundleAdjustmentUseManualTracks(std::string(),
                     "ba-use-manual-tracks",
                     "only use manualy created correspondencies for SfM in bundle adjustment",
                     cmd,
                     false);

    TCLAP::SwitchArg bundleAdjustmentUsePipelineTracks(std::string(),
                     "ba-use-pipeline-tracks",
                     "only use tracks created by pipeline for correspondencies of SfM in bundle adjustment",
                     cmd,
                     false);

    TCLAP::ValueArg<double> bundleAdjustmentTriangulatedUncertainties(
                std::string(), "ba-triangulated-unc",
                "the uncertainty in px to be used for triangulated tracks",
                false,
                2.0,
                "double",
                cmd);



    TCLAP::MultiArg<std::string> sinksArg(
                "s", "sink",
                "export sink we want to use. Eg. map, hdf, png, video. Default is video.",
                false,
                "string",
                cmd);



    /* --- custom arguments for sources/generators/sinks */

    TCLAP::ValueArg<std::string> groundtruthFlowStepArg(
                std::string(), "flow-steps",
                "list of steps",
                false,
                std::string("1,8"),
                "comma seperated list of numbers",
                cmd);

    TCLAP::ValueArg<int> groundtruthSamplingCountArg(
                std::string(), "sampling-count",
                "samppling count for groundtruth source",
                false,
                50,
                "integer",
                cmd);
    
    TCLAP::SwitchArg samplingDisableArg(std::string(),
                     "sampling-disable",
                     "disable sampling altogether",
                     cmd,
                     false);

    TCLAP::ValueArg<int> groundtruthHistSizeArg(
                std::string(), "sampling-binsize",
                "histogram size for groundtruth sampling",
                false,
                11,
                "integer",
                cmd);


    TCLAP::ValueArg<int> colorStyleArg(
                std::string(), "colorstyle",
                "color style of the depthmap : 	AUTUMN = 0, BONE = 1, JET = 2, WINTER = 3, RAINBOW = 4, OCEAN = 5, SUMMER = 6, SPRING = 7, COOL = 8, HSV = 9, PINK = 10, HOT = 11, GRAY = 12",
                false,
                0,
                "integer",
                cmd);

    TCLAP::ValueArg<double> colorMapThresholdArg(
                std::string(), "colormap-threshold",
                "colormapthreshold determine maximum distance in meter (-1 == no threshold)",
                false,
                -1,
                "double",
                cmd);

    TCLAP::ValueArg<std::string> contourJsonPathArg(
                std::string(), "contour-file",
                "path to contour json file",
                false,
                std::string(),
                "path",
                cmd);

    TCLAP::ValueArg<std::string> maskDirectoryPathArg(
                std::string(), "mask-dir",
                "path to masking dircetory",
                false,
                std::string(),
                "path",
                cmd);



	try {
		cmd.parse(argc, argv);

	}
	catch (TCLAP::ArgException &e)  {
		std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
		return 0;
	}


    try {

        auto frameIndicesExport = framesFromArgument(framesArg.getValue());

        auto frameIndicesBA = frameIndicesExport;
        if(bundleAdjustmentFramesArg.isSet()) {
            frameIndicesBA = framesFromArgument(bundleAdjustmentFramesArg.getValue());
        }

        /* ------ open project and sequence */
        std::string projectName = projectNameArg.getValue();
        std::string sequenceName = sequenceModeArg.getValue();


        /*open requested project */
        Project project = Project::open(projectName);

        if (!project.isValid()) {
            throw std::runtime_error("Project '" + projectName + "' could not be found.");
        }

        SequenceList sequences = project.sequences();
        Sequence sequence = sequences.byLabel(sequenceName);
        if (!sequence.isValid()) {
            throw std::runtime_error("Sequence '" + sequenceModeArg.getValue() + "' could not be found in project.");
        }

        FrameList frames = sequence.frames();




        if(frames.empty() || frames.front().intrinsics() == Vector4d())
            throw std::runtime_error("Could not find frame intrinsics.");

        Geometry g = sequence.geometry();

        /*
         *
         *  ---- setup sources  -----
         *
         */
        std::vector<ExporterSource*> sources;

        /* create map sources for all requested maps */
        for (auto & mapName : mapsArg.getValue()) {
            sources.push_back(new MapSource(mapName));
        }

        /* create sources for all requested views */
        for (auto & viewName : viewsArg.getValue()) {
            for(auto v: viewsArg.getValue()) {
                if(v == "base") {
                    sources.push_back(new ViewSource(Frame::View::Base));
                }
                if(v == "stereo") {
                    sources.push_back(new ViewSource(Frame::View::Stereo));
                }
            }
        }

        GroundtruthSource * gtSourcePtr = nullptr;

        /* create sources for all requested generators */
        for (auto generatorName : generatorsArg.getValue()) {
            if(generatorName == "groundtruth") {
                gtSourcePtr = new GroundtruthSource();
                gtSourcePtr->enablePoseEstimation(useFramePoseEstimationArg.getValue());
                gtSourcePtr->setSampleCount(groundtruthSamplingCountArg.getValue());
                gtSourcePtr->setHistogramSize(groundtruthHistSizeArg.getValue());
                gtSourcePtr->setFlowTargets({8});
                gtSourcePtr->setDisableSampling(samplingDisableArg.getValue());
                sources.push_back(gtSourcePtr);
            }

            if(generatorName == "contours") {
                auto s = new ContourSource();
                s->setJsonPath(contourJsonPathArg.getValue());
                sources.push_back(s);
            }
        }

//        /* if the source is still empty, then we create a default source that displays the
//         * base view. */
//        if (sources.empty()) {
//            sources.push_back(new ViewSource(Frame::View::Base));
//        }

        /* create a list of requested frames to export */
        FrameList framesExport;
        for(auto i: frameIndicesExport) {
            framesExport.append(Frame(frames[i]));
        }

        /* create a list of requested frames for bundle adjustment */
        FrameList framesBA;
        for(auto i: frameIndicesBA) {
            framesBA.append(Frame(frames[i]));
        }

        /* ---- bundle adjustment */
        if(useBundleAdjustmentArg.getValue())
        {

            auto adjustmentSource= new PosesSource(frames, framesBA, framesExport);
            adjustmentSource->setTriangulatedImageUncertainties(bundleAdjustmentTriangulatedUncertainties.getValue());
            adjustmentSource->setAnnotatedImageUncertainties(2.0);

            std::unordered_set<Project::Context> contexts;
            if(bundleAdjustmentUseManualTracks.getValue()) {
                contexts.insert(Project::ManualContext);
            }

            if(bundleAdjustmentUsePipelineTracks.getValue()) {
                contexts.insert(Project::PipelineContext);
            }

            adjustmentSource->setMaskDirectory(maskDirectoryPathArg.getValue());
            adjustmentSource->setTrackContext(contexts);
            adjustmentSource->setTriangulate( bundleAdjustmentTriangulateArg.getValue() );
            adjustmentSource->setUseRightView( bundleAdjustmentUseStereoView.getValue() );
            adjustmentSource->setStoreVis(bundleAdjustmentStoreVisArg.getValue());
            adjustmentSource->solve();

            auto ps = adjustmentSource->poses();
            auto pcovs = adjustmentSource->covariances();
           
            sources.push_back( adjustmentSource );

            /* set the list of manual poses and covariances in groundtruth generator
             * and, if requested, store it in data file */
            for(unsigned i = 0; i < framesExport.size()-1; i++) {

                Frame f = framesExport[i];

                /* get the frame difference in sequence */
                int framesDelta = frames.find(framesExport[i+1])-frames.find(framesExport[i]);

                if(gtSourcePtr) {
                    gtSourcePtr->addFramePoses(framesExport[i]  , ps[i],
                                               framesExport[i+1], ps[i+1], pcovs[i]);
                }

                if(storeFramePosesArg.getValue()) {

                    std::stringstream cpath;
                    cpath << "pose/cov/" << framesDelta;
                    std::stringstream ppath;
                    ppath << "pose/pose";

                    Frame::List currentPose(Array2dView({6,1}, ps[i].data()));
                    Frame::List currentCVar(pcovs[i]);

                    f.setList(cpath.str(), currentCVar);
                    f.setList(ppath.str(), currentPose);
                }
            }
        }

        /*
         *
         * ---- setup sinks -----
         *
         */
        std::vector<ExporterSink*> sinks;

        /* create all requested sinks */
        if (!sinksArg.getValue().empty()) {
            std::vector<std::string> sinkNames = sinksArg.getValue();

            if (std::count(sinkNames.cbegin(), sinkNames.cend(), "map")) {
                auto mapSink = new MapExporterSink();
                mapSink->setFrames(frames);
                sinks.push_back(mapSink);
            }

            if (std::count(sinkNames.cbegin(), sinkNames.cend(), "hdf")) {
                sinks.push_back(new HDFExporterSink());
            }

            if (std::count(sinkNames.cbegin(), sinkNames.cend(), "png")) {
                sinks.push_back(new PNGExporterSink());
            }

            if (std::count(sinkNames.cbegin(), sinkNames.cend(), "video")) {
                sinks.push_back(new VideoExporterSink());
            }

            if (std::count(sinkNames.cbegin(), sinkNames.cend(), "display")) {
                sinks.push_back(new DisplaySink());
            }

        } else {
            /* use display sink as default if nothing other specified */
            sinks.push_back(new DisplaySink());
        }


        /* collect all available sources */
        /** @todo implement checking of duplicate sources */
        ExporterSource::SourceIdentifierList sourceIds;
        for (ExporterSource * s : sources) {
            ExporterSource::SourceIdentifierList localList = s->provides();
            std::copy(localList.cbegin(), localList.cend(), std::back_inserter(sourceIds));
        }


        /* now set up the export sinks */

        for(ExporterSink * s : sinks) {
            s->m_path = project.base();
            s->m_sequenceName = sequenceName;
            s->setup(sourceIds);
        }

        ExporterSink::setSequenceName(sequenceModeArg.getValue());

        bool bPrintFilename = (sinks.size() > 0);
        for (int i : frameIndicesExport) {
            if(bPrintFilename)
            {
                std::cout << ">> Starting to Export (file: " << sinks[0]->getDataFilePath(i) << ")" << std::endl;
                bPrintFilename = false;
            }

            std::cout << std::endl << std::endl << "###############" << std::endl;
            std::cout << "[STATUS]: Exporting Frame: " << i << std::endl;

            ExporterSource::SourceMap sourceMap;

            /* iterate over all sources and push respective result to sinks */
            for (ExporterSource *src : sources){
                try {
                    auto res = src->sources(frames[i]);
                    for (ExporterSink * s : sinks) {
                        s->push(res, i);
                    }
                } catch (std::exception & e) {
                    std::cerr << "[ERROR|" << i << "]: " << e.what()
                              << std::endl << std::endl;
                }
            }

            /* check if we need to update meta information and
             * dispatch it to sinks */
            if(metaArg.isSet()) {
                for (ExporterSink * s : sinks) {
                    s->pushMeta(i);
                }
            }

        }


        /* delete all sources */
        for (ExporterSource * s : sources) {
            delete s;
        }

        /* delete all sinks. This should signal the sinks that we are finished and
        * that they should clean up as well */
        for (ExporterSink * s : sinks) {
            delete s;
        }

    } catch (std::exception & e) {
        std::cerr << "[ERROR]: Exporting failed: " << e.what() << std::endl;
    }
}
