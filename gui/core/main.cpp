#include <QtGui/QVector3D>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCore/QDebug>
#include <QtCore/QTime>
#include <QtCore/QCommandLineParser>
#include <QtCore/QCommandLineOption>

#include "gui/MainWindow.h"

int main(int argn, char ** argc) {
	
	QApplication app(argn, argc);


    QCommandLineParser parser;
    parser.setApplicationDescription("GUI for feature to landmark annotations");
    parser.addHelpOption();

    QCommandLineOption sequenceOpt(QStringList()<< "s" << "sequence",
                                   "the sequence to use",
                                   "name");

    parser.addOption(sequenceOpt);
    parser.process(app);

    qDebug() << "issset: " << parser.isSet(sequenceOpt);
    QString sequence = parser.value(sequenceOpt);


    qDebug() << "Using sequence: " << sequence;

	MainWindow win;
    win.m_sequenceName = sequence;
	win.setWindowTitle("boschGT");
	win.show();
	app.exec();
}
