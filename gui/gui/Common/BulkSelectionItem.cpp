/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug>
#include <QtWidgets/QGraphicsScene>
#include <QtGui/QPainter>

#include "gui/Common/BulkSelectionItem.h"

const qreal PATH_THICKNESS = 3.0;

BulkSelectionItem::BulkSelectionItem() :
	m_isActive(true)
{
	m_active.append(QPolygonF());
	m_passive.append(QPolygonF());
}


QRectF BulkSelectionItem::boundingRect() const
{
	return m_boundingRect;
}

void BulkSelectionItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	/* common to both path types */
	QPen pen;
	pen.setCosmetic(true);
	pen.setWidthF(PATH_THICKNESS);
	pen.setCapStyle(Qt::RoundCap);
	pen.setJoinStyle(Qt::RoundJoin);

	/* active portion of selection path is yellow */
	QPen activePen(pen);
	activePen.setColor(Qt::yellow);

	/* passive is gray */
	QPen passivePen(pen);
	passivePen.setColor(Qt::gray);

	/* first draw the active portion */
	painter->save();
	painter->setOpacity(0.7);
	painter->setPen(activePen);
	foreach(const QPolygonF p, m_active) {
		painter->drawPolyline(p);
	}
	painter->restore();

	/* secondly draw the passive portion */
	painter->save();
	painter->setOpacity(0.7);
	painter->setPen(passivePen);
	foreach(const QPolygonF p, m_passive) {
		painter->drawPolyline(p);
	}
	painter->restore();

}

void BulkSelectionItem::addPoint(const QPointF &p)
{

	if(!scene())
		return;

	if(m_isActive) {
		m_active.last().append(p);
	} else {
		m_passive.last().append(p);
	}

	/* this is needed for the scene cache to be updated */
	this->prepareGeometryChange();

	/* update the bounding rect.
	 * simply unite the current bounding rect with this one */
	m_boundingRect = m_boundingRect | QRectF(p, QSizeF(0.4,0.4));

	/* we are not in active mode, so we don't need to handle selections */
	if(!m_isActive)
		return;

	/* for the current point, get the current hovered SelectionHandler and
	 * add it's parent to the list of selected items */
	QList<QGraphicsItem*> items = this->scene()->items(p, Qt::IntersectsItemBoundingRect);

	/* get all items that have a SelectionHandler installed */
	const QList<SelectionHandlerItem*> shItems = SelectionHandlerItem::toSHList(items);

	QGraphicsItem * nearest = SelectionHandlerItem::getNearestParent(shItems, p);

	if(!nearest)
		return;

	/* if the current nearest item is already in list
	 * than we don't add this one again */
	if(m_selected.contains(nearest))
		return;

	/* finally add item to selection list and select it in the scene */
	m_selected.append(nearest);
	nearest->setSelected(true);

}

void BulkSelectionItem::setActiveTracking()
{
	m_isActive = true;

	if(m_active.last().isEmpty())
		m_active.takeLast();
	m_active.append(QPolygonF());
}

void BulkSelectionItem::setPassiveTracking()
{
	m_isActive = false;

	if(m_passive.last().isEmpty())
		m_passive.takeLast();
	m_passive.append(QPolygonF());
}

QList<QGraphicsItem*> BulkSelectionItem::selectedItems()
{
	return m_selected;
}

