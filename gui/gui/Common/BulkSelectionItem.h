/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BULKSELECTIONITEM_H
#define BULKSELECTIONITEM_H

#include <QtWidgets/QGraphicsItem>
#include <QtGui/QPolygonF>
#include <QtCore/QList>

#include "gui/Common/SelectionHandlerItem.h"


/** @brief bulk selection indicator
 *  This Item is used to store a bulk selection path.
 *  This item also handle intersections of the selection-path
 *  with SelectionHandlerItems and create a list of selected items
 *  that is ordered by selection time.
 *  It also draws the selection path.
 *
 *  This item has two modes:
 *  - active: selects items when tip of path intersecs with any SelectionHandlerItem
 *  - passive: only collects the selection path
 *
 *  You can set one of the modes by setActiveTracking() or
 *  setPassiveTracking().
 *
 *  Finally you get all selected items by selectedItems() in the order they were
 *  selected.
 */
class BulkSelectionItem : public QGraphicsItem
{
private:
	QList<QPolygonF> m_active;
	QList<QPolygonF> m_passive;

	bool m_isActive;

	QRectF m_boundingRect;

	QList<QGraphicsItem*> m_selected;

public:
	BulkSelectionItem();

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	/** adds a point to the selection path */
	void addPoint(const QPointF & p);

	/** sets this item into active tracking mode,
	 *  where each point given by addPoint() will count to active
	 *  portion of selection path */
	void setActiveTracking();

	/** set this item to passive tracking.
	 * @see setActiveTracking()
	 */
	void setPassiveTracking();

	bool isActive() const {
		return m_isActive;
	}

	/** @returns a list of all selected items by this path */
	QList<QGraphicsItem *> selectedItems();

private:


};

#endif // BULKSELECTIONITEM_H
