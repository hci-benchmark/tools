/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QtDebug>

#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsRectItem>

#include <QtGui/QPainter>
#include <QtGui/QBrush>
#include <QtGui/QPen>

#include <QtCore/qmath.h>
#include <QtGui/QQuaternion>
#include <QtGui/QVector2D>
#include <QtGui/QVector3D>

#include "gui/Common/SelectionHandlerItem.h"

SelectionHandlerItem::SelectionHandlerItem()
{


}

void SelectionHandlerItem::paint(QPainter *painter,
                                 const QStyleOptionGraphicsItem *option,
                                 QWidget *widget)
{

}

QGraphicsItem *SelectionHandlerItem::getNearestParent(const QList<SelectionHandlerItem *> &shs,
                                                      const QPointF &where)
{

	if(shs.isEmpty())
		return 0;

	QMap<qreal, QGraphicsItem*> distances;

	/* go through all items and check their relative distance to given point. */
	foreach(const SelectionHandlerItem * s, shs) {
		QGraphicsItem * parent = s->parentItem();

        if(!parent)
			continue;
        if(!parent->isEnabled())
            continue;

		distances.insert(QVector2D(where-parent->pos()).length(), parent);
	}

    /* okay, we have no point found */
    if(distances.empty())
        return nullptr;

	/* QMap is ordered. we want the item with smallest distance */
    return distances[distances.firstKey()];
}

QRectF SelectionHandlerItem::boundingRect() const
{
	return QRectF(-0.5, -0.5, 1.0, 1.0);
}



