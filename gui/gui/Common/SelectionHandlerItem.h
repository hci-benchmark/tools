/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SELECTIONHANDLERITEM_H
#define SELECTIONHANDLERITEM_H

#include <QtWidgets/QGraphicsItem>


/** @brief handle selections for items
 *  This item acts as an invisible region around the parent's item.
 *  This way, all selections will be done much more convinient, as the user
 *  doesn't need to precisely point onto a selectable item.
 *
 * Simply add this class as  a child to a selectable item and query
 * for all SelectionHandlerItems under a given position.
 *
 * Finally use getNearestParent() to get the nearest QGraphicsItem. This is independent
 * of the z-ordering of the items.
 *
 * This item has a unit-square as shape. So scale this according to your needs.
 */
class SelectionHandlerItem : public QGraphicsItem
{

public:
	enum { Type = UserType + 1 };

private:


public:
	SelectionHandlerItem();

	QRectF boundingRect() const;

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	int type() const {
		return Type;
	}

	/** @returns the item that has the smallest distance to point */
	static QGraphicsItem * getNearestParent(const QList<SelectionHandlerItem*> & shs,
	                                        const QPointF & where);

	/** @returns from a list of QGraphicItems only SelectionHandlerItems */
	inline static QList<SelectionHandlerItem*> toSHList(const QList<QGraphicsItem*> & list) {
		QList<SelectionHandlerItem*> shList;
		foreach(QGraphicsItem * i, list) {
			SelectionHandlerItem * sh = qgraphicsitem_cast<SelectionHandlerItem*>(i);
			if(sh)
				shList.append(sh);
		}
		return shList;
	}
};

#endif // SELECTIONHANDLERITEM_H
