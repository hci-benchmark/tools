/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FRAMESCENE_H
#define FRAMESCENE_H

#include <QtGui/QVector3D>
#include <QtWidgets/QGraphicsScene>

class FrameScene : public QGraphicsScene
{
	Q_OBJECT
public:
	explicit FrameScene(QObject *parent = 0);

signals:

public slots:

};

#endif // FRAMESCENE_H
