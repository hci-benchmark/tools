/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>

#include <QtCore/QDebug>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtWidgets/QScrollBar>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QGraphicsItem>
#include <QtCore/QSet>
#include <QtWidgets/QMenuBar>

// Hookers Toolbox
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>
#include <hookers/toolbox/intrinsics2mat.cpp>


#include "gui/gui/MainWindow.h"
#include "gui/Common/SelectionHandlerItem.h"
#include "gui/Frames/FrameViewer.h"
#include "gui/Frames/FrameScene.h"
#include "gui/Frames/TrackpointItem.h"
#include "gui/Parts/Part3DItem.h"

using namespace hookers;
using namespace hookers::interface;

FrameViewer::FrameViewer(QWidget *parent) :
    QGraphicsView(parent), m_mode(NormalViewMode),
     m_partMenu(nullptr), m_frameViewItem(nullptr)
{


    m_selections[TrackSelectionMode];
    m_selections[PartSelectionMode];

    qDebug() << "lalala" << m_selections.size() << m_selections.keys();

    m_currentSelection = m_selections.begin();

	this->setScene(new FrameScene(this));

//    this->setupSceneContent();

	this->setRenderHint(QPainter::Antialiasing, true);
	this->setInteractive(false);

	/* enable of panning of the scene is zoomed in */
	this->setDragMode(ScrollHandDrag);

	this->setResizeAnchor(NoAnchor);
	this->setTransformationAnchor(NoAnchor);

	/* disable all scrollbars */
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	/* get continous mouse movent events */
	this->setMouseTracking(true);


	/* load default message */
	this->updateSceneContent();


//    double dumy_cameraMatrix[3][3] =
//    {
//        { 1850.0,    0.0, 1300.0 },
//        { 0.0,    1850.0,  550.0 },
//        { 0.0,       0.0,    1.0 }
//    };

//    cv::Mat(3, 3, CV_64FC1, &dumy_cameraMatrix).copyTo(m_cameraMatrix);
//    m_cameraMatrix.convertTo(m_cameraMatrix, CV_32F);

}

Frame FrameViewer::frame() const
{
	return m_frame;
}

void FrameViewer::setFrame(const Frame &frame)
{

	/* same frame, do nothing */
	if(m_frame == frame)
		return;

	/* only change the previous frame,
	 * if current and new frame are not the same */
	if(frame.isValid()) {
		m_prevFrame = m_frame;
	}

	m_frame = frame;
    hookers::toolbox::intrinsics2mat(m_frame.intrinsics(), m_cameraMatrix);
    this->updateContent();
}

void FrameViewer::setSequence(Sequence &project)
{


	/* clear anything before setting a new project */
	this->clear();

	if(!project.isValid())
		return;

    m_sequence = project;
    const FrameList frames = project.frames();

	/* if the project has a frame,
	 * set this view to the first frame in sequence */
    if(!frames.empty()) {
        this->setFrame(frames.front());
    }

	/* make sure that the scenes fits into the view */
	this->fitInView(scene()->sceneRect(), Qt::KeepAspectRatio);
	emit projectChanged();
}

TrackList FrameViewer::selectedTracks() const
{

    const SelectionModeState & state = const_cast<SelectionModeMap&>(m_selections)[TrackSelectionMode];

    if(!state.selectionItem)
        return TrackList();

    TrackList tracks;

    QList<QGraphicsItem*> items = state.selectionItem->selectedItems();
	foreach(QGraphicsItem * i, items) {
		TrackpointItem * p = qgraphicsitem_cast<TrackpointItem*>(i);
		if(!p || !p->track().isValid())
			continue;

        tracks.push_back(p->track());
	}

	return tracks;
}

Track FrameViewer::selectedTrack() const
{
    TrackList selectedTracks = this->selectedTracks();
    if(selectedTracks.empty())
		return Track();

    return selectedTracks.back();
}

LandmarkList FrameViewer::selectedLandmarks() const
{
    const SelectionModeState & state = const_cast<SelectionModeMap&>(m_selections)[PartSelectionMode];

    if(!state.selectionItem)
        return LandmarkList();

    LandmarkList landmarks;

    QList<QGraphicsItem*> items = state.selectionItem->selectedItems();
    foreach(QGraphicsItem * i, items) {
        FeatureItem * p = qgraphicsitem_cast<FeatureItem*>(i);
        if(!p || !p->feature().isValid())
            continue;

        landmarks.push_back(p->feature());
    }

    return landmarks;


}

void FrameViewer::clear()
{

	m_trackpoints.clear();
	m_prevFrame = Frame();
	m_frame = Frame();
    m_sequence = Sequence();

	if(scene())
		this->scene()->clear();

	this->updateContent();
}

void FrameViewer::clearSelection()
{

    for(SelectionModeMap::Iterator it = m_selections.begin();
        it != m_selections.end(); it++)
    {

        SelectionModeState & state = it.value();
        if(state.selectionItem) {
            delete state.selectionItem;
            state.selectionItem = 0;
        }
    }

	if(scene()) {
		scene()->clearSelection();
	}
}

void FrameViewer::enableNormalMode()
{
	m_mode = NormalViewMode;
	this->updateContent();
}

void FrameViewer::enableDeltaMode()
{
	m_mode = DeltaViewMode;
	this->updateContent();
}

void FrameViewer::toggleViewMode()
{
	if(m_mode == NormalViewMode) {
		this->enableDeltaMode();
	} else {
		this->enableNormalMode();
    }
}


void FrameViewer::setSelectionMode(FrameViewer::SelectionMode mode)
{

    if(!m_selections.contains(mode))
        return;


    if(m_currentSelection != m_selections.end()) {
        SelectionModeState & lastState = m_currentSelection.value();
        for(QGraphicsItem * i: lastState.items) {
            i->setEnabled(false);
            i->setZValue(10);
        }

        if(lastState.selectionItem) {
            lastState.selectionItem->setEnabled(false);
            lastState.selectionItem->setZValue(15);
        }
    }

    m_currentSelection = m_selections.find(mode);

    qDebug() << "[MODE] " << m_currentSelection.key();

    SelectionModeState & state = m_currentSelection.value();

    for(QGraphicsItem * i: state.items) {
        i->setEnabled(true);
        i->setZValue(50);
    }

    if(state.selectionItem) {
        state.selectionItem->setEnabled(true);
        state.selectionItem->setZValue(55);
    }

}

void FrameViewer::cycleSelectionMode()
{

    if(m_selections.empty())
        return;

    SelectionModeMap::Iterator newMode = m_currentSelection;
    newMode++;

    if(newMode == m_selections.end())
        newMode = m_selections.begin();

    this->setSelectionMode(newMode.key());

}

void FrameViewer::updateContent()
{
    hookers::toolbox::intrinsics2mat(m_frame.intrinsics(), m_cameraMatrix);
    /* reset all selection state */
    for(SelectionModeState & s : m_selections) {
        if(!s.selectionItem)
            continue;

        delete s.selectionItem;
        s.selectionItem = nullptr;
    }

	this->updateSceneContent();
    this->updatePartContent();
    this->setSelectionMode(m_selections.firstKey());
}

void FrameViewer::partSelectionActionTriggered()
{
    QAction * sender = qobject_cast<QAction*>(this->sender());

    if(!sender)
        return;

    QGraphicsItem * itm = (QGraphicsItem*)sender->data().value<void*>();

    if(!itm)
        return;

    if(sender->isChecked()) {
        itm->setVisible(true);
    } else {
        itm->setVisible(false);
    }
}

void FrameViewer::wheelEvent(QWheelEvent *event)
{

	/* we don't support zooming while in selection mode */
	if(event->modifiers() & Qt::ControlModifier)
		return;



	/* store the current position the pointer points to */
	QPointF posInScene = this->mapToScene(event->pos());

	const qreal scale = 1.5;
	if(event->angleDelta().y() > 0) {
		this->scale(scale, scale);
	} else if(event->angleDelta().y() < 0) {
		this->scale(1.0/scale, 1.0/scale);
	}

	/* get the new position in scene, where the cursor points on */
	QPointF newPos = this->mapToScene(event->pos());

	QPointF delta = (newPos-posInScene);
	this->translate(delta.x(), delta.y());

	/* note, that we are not using the default implementation of QGraphicsView, as
	 * this will pan the view */
	event->accept();

}

void FrameViewer::keyPressEvent(QKeyEvent *event)
{
	
	if(event->modifiers() & Qt::ShiftModifier) {
		this->viewport()->setCursor(QCursor(Qt::CrossCursor)) ;
		event->accept() ;
	}

	else if(event->modifiers() & Qt::ControlModifier) {
		/* if we are in selection mode, we do not want to have any interaction with the scene
		 * that comes from the default implementation of QGraphicsVew */
		this->setDragMode(NoDrag);

		/* set a pointing cursor to indicate that we are in selection mode */
		this->viewport()->setCursor(QCursor(Qt::PointingHandCursor));

        SelectionModeState & state = m_currentSelection.value();
		/* clear bulk selection handler */
        if(state.selectionItem) {
			this->clearSelection();
		}

		event->accept();

	} else {
		QGraphicsView::keyPressEvent(event);
	}
}

void FrameViewer::keyReleaseEvent(QKeyEvent *event)
{
	if(event->modifiers() & Qt::ShiftModifier) {
		this->viewport()->setCursor(QCursor()) ;
		event->accept() ;
	}
	/* handle releae of ctrl-key (leaving selection mode) */
	else if(!(event->modifiers() & Qt::ControlModifier)) {
		this->viewport()->setCursor(QCursor());

		/* enable default interaction (panning around) again */
		this->setDragMode(ScrollHandDrag);
		event->accept();
	} else {
		QGraphicsView::keyReleaseEvent(event);
	}
}

void FrameViewer::mousePressEvent(QMouseEvent *event)
{

//    /* add a new track */
//    if((event->modifiers() & Qt::ShiftModifier)) {
//		//insert new track at mouse position
//		emit newTrackInserted(mapToScene(event->pos()), m_frame.id()) ;
//	}
	
	if((event->modifiers() & Qt::ControlModifier)) {

        SelectionModeState & state = m_currentSelection.value();

		/* if no current bulk-selection item is available,
		 * create a new one */
        if(!state.selectionItem) {
            state.selectionItem = new BulkSelectionItem();
            scene()->addItem(state.selectionItem);
		}

        state.selectionItem->setActiveTracking();
        state.selectionItem->addPoint(mapToScene(event->pos()));

		event->accept();

	} else {
		/* if no ctrl-key is pressed, than handle mouse input as
		 * usual */
		QGraphicsView::mousePressEvent(event);
	}
}


void FrameViewer::mouseReleaseEvent(QMouseEvent *event)
{

	/* handle mouse release events independant of modifier state, such that
	 * dragging can stop, even if ctrl-key is pressed */
	QGraphicsView::mouseReleaseEvent(event);

    if(event->button() == Qt::RightButton) {

        QMenu menu;

        auto itemTmp = itemAt(event->pos());
        auto item = dynamic_cast<TrackpointItem*>(this->itemAt(event->pos())->parentItem());
        auto where = mapToScene(event->pos());

        if(item) {
            auto t = item->track();

            menu.addAction("Delete Track", [&]{
                qDebug() << "Deleting Track is not implemented.";
            });

            menu.addAction("Unlink", [&]{
                m_frame.unlinkTrack(item->track());
                this->updateContent();
            });

            menu.addAction("Add to next Frame", [&]{
                auto frames = m_frame.sequence().frames();
                auto it = std::find(frames.begin(), frames.end(), m_frame);

                /* frame has not been found in list */
                if(it == frames.end())
                    return;

                it++;
                if(it == frames.end())
                    return;

                auto nextFrame = (*it);
                nextFrame.linkTrack(t, t.posInFrame(m_frame));
            });


        } else {
            menu.addAction("Add new Track", [&]{
                auto t = m_frame.sequence().addTrack();
                m_frame.linkTrack(t, {where.x(), where.y()});
                this->updateContent();
            });
        }
        menu.exec(QCursor::pos());
    }

	/* if the Control-mod is not set, we do nothing */
	if(!(event->modifiers() & Qt::ControlModifier)) {
		return;
	}

    SelectionModeState & state = m_currentSelection.value();
	/* set bulk selection item to passive mode */
    if(state.selectionItem) {
        state.selectionItem->setPassiveTracking();
        state.selectionItem->addPoint(mapToScene(event->pos()));
	}

}

/** @todo unhighlighting should be made faster by using a cache
 * for all highlighted items. */
void FrameViewer::mouseMoveEvent(QMouseEvent *event)
{

//	/* make any highlighted item unhighlighted */
//	foreach(TrackpointItem * i, m_trackpoints) {
//		i->setHighlighted(false);
//	}

    SelectionModeState & state = m_currentSelection.value();
	if((event->modifiers() & Qt::ControlModifier)) {
		/* we are in selection mode, so handle the bulk selection */
        if(state.selectionItem)
            state.selectionItem->addPoint(mapToScene(event->pos()));
	} else {

        /* handle normal mouse movements for panning in scene */
        QGraphicsView::mouseMoveEvent(event);


//		/* highlight single correspondences */
//		/* search for a TrackpointItem under the mouse pointer */
//		QList<QGraphicsItem*> gItems = this->items(event->pos());

//		/* get all items that have a SelectionHandler installed */
//		const QList<SelectionHandlerItem*> shItems = SelectionHandlerItem::toSHList(gItems);


//		/* to the event-position, get the nearest selection handler and it's parent item */
//		QGraphicsItem * nearest = SelectionHandlerItem::getNearestParent(shItems, mapToScene(event->pos()));
//		TrackpointItem * p  = qgraphicsitem_cast<TrackpointItem*>(nearest);


//		/* if there is no TrackpointItem found, emit that we have deselected any trackpoint */
//		if(!p) {
//			emit trackFocused(Track());
//			return;
//		}

//		/* get the track and check if track is valid */
//		Track t = p->track();
//		emit trackpointSelected(t) ;

//		if(!t.hasLink())
//			return;

//		/* finally set the track beeing highlighted and announce this as well */
//		p->setHighlighted(true);
//		emit trackFocused(t);

	}
}

void FrameViewer::enterEvent(QEvent * event)
{
	this->viewport()->setCursor(QCursor());
	this->setDragMode(ScrollHandDrag);

	this->setFocus();
    event->accept();
}

void FrameViewer::setupSceneContent()
{
}

void FrameViewer::updateSceneContent()
{

//	qDebug() << "Updating Scene content for frame: " << m_frame.id();
    hookers::toolbox::intrinsics2mat(m_frame.intrinsics(), m_cameraMatrix);
	/* reset the selection and clear the scene */

    SelectionModeState & state = m_selections[TrackSelectionMode];

    for(TrackpointItem * f : m_trackpoints) {
        scene()->removeItem(f);
        delete f;
    }
    m_trackpoints.clear();
    state.items.clear();


    Image view;
    if(m_frame.isValid())
        m_frame.getView(view);


    if(!view.hasData()) {
		/* there is no valid frame */
		scene()->addText("No frame selected");
	} else {
		/* there is a valid frame.
		 * load the scene with the pixmap and
		 * all track-points belonging to this frame */


        QImage qView(view.data(), view.width(), view.height(), QImage::Format_Indexed8);
        QPixmap img = QPixmap::fromImage(qView);

        if(m_frameViewItem) {
            scene()->removeItem(m_frameViewItem);
            delete m_frameViewItem;
        }
        m_frameViewItem = scene()->addPixmap(img);


    }

    /* get the full list of all Tracks that are present in current frame */
    TrackList tracks = m_frame.tracks();

    TrackSet trackSet = TrackSet::fromList(tracks);

    /* handle the delta view mode */
    if(m_mode == DeltaViewMode && m_prevFrame.isValid()) {
        TrackSet prevTrackSet = TrackSet::fromList(m_prevFrame.tracks());
        trackSet -= prevTrackSet;
    }

    /* add a TrackpointItem for each Track that is related to the current frame */
    foreach(Track t, trackSet) {
        TrackpointItem * p = new TrackpointItem(t);

        if(!p)
            continue;

        p->setZValue(100);
        p->setFlag(QGraphicsItem::ItemIgnoresTransformations);

        m_trackpoints.append(p);
        state.items.append(p);

        Vector2d pos = m_frame.trackPosition(t);
        p->setPos(pos[0], pos[1]);
        scene()->addItem(p);
    }

}

void FrameViewer::setupPartContent()
{
    if(m_cameraMatrix.empty())
        return;

    hookers::toolbox::intrinsics2mat(m_frame.intrinsics(), m_cameraMatrix);

    SelectionModeState & state = m_selections[PartSelectionMode];

    if(m_partMenu)
        delete m_partMenu;

    m_partMenu = new QMenu("Part Selection");
    MainWindow::instance()->menuBar()->addMenu(m_partMenu);


    /* delete old parts */
    for(auto p: m_parts) {
        scene()->removeItem(p);
        delete p;
    }
    m_parts.clear();
    state.items.clear();


    PartList parts = m_sequence.project().parts();
    for(Part & p : parts) {

        /** @todo ugly hack to avoid loading the street markers */
        if(p.name().find("noAutos") != std::string::npos)
            continue;

        Part3DItem * item = new Part3DItem(p, m_cameraMatrix);
        item->setZValue(50);
        item->setVisible(false);
        item->setEnabled(false);

        m_parts.push_back(item);
        scene()->addItem(item);
        state.items.append(item);

        QAction * selectItemAction  = new QAction(QString::fromStdString(p.name()), this);
        selectItemAction->setCheckable(true);
        selectItemAction->setChecked(false);
        selectItemAction->setData(QVariant::fromValue((void*)item));
        connect(selectItemAction, &QAction::triggered, this, &FrameViewer::partSelectionActionTriggered);

        m_partMenu->addAction(selectItemAction);

    }



}

void FrameViewer::updatePartContent()
{
    try {
        if(!m_frame.isValid())
            throw false;

        if(m_cameraMatrix.empty())
            throw false;

        hookers::toolbox::intrinsics2mat(m_frame.intrinsics(), m_cameraMatrix);
        TrackList annotTracks = m_frame.tracks().annotated();

        if(annotTracks.size() < 5)
            throw false;



        try {

            LandmarkList annotlandMarks = annotTracks.landmarks();
            Vector2fArray trackPositions;
            Vector3fArray landmarkPositions;
            annotTracks.getPositionsInFrame(trackPositions, m_frame);
            annotlandMarks.getPositions(landmarkPositions);

            cv::Mat tvec = cv::Mat(3, 1, CV_32FC1);
            cv::Mat rvec = cv::Mat(3, 1, CV_32FC1);

            Vector6d pose = hookers::toolbox::PoseEstimation::solvePnPRansac(m_frame, 10.0, 10);

            for(int i = 0; i < 3; i++) {
                tvec.at<float>(i,0) = pose[i];
                rvec.at<float>(i,0) = pose[i + 3];
            }

            QRectF boundingRect = QRectF(0, 0, frame().size()[0], frame().size()[1]);
            for(Part3DItem * i : m_parts) {
                i->setBoundingRect(boundingRect);
                i->setCameraPose(rvec, tvec);
                i->updatePositions();
            }

            emit framePoseChanged(pose);

        } catch(std::exception & e) {
            std::cout << "[INFO][GUI] Could not find camera pose: " << e.what() << std::endl;
            throw false;
        }



    } catch (...) { // Who cares?
    }
}
