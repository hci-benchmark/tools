/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FRAMEVIEWER_H
#define FRAMEVIEWER_H

#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QMenu>

#include <hookers/interface/Project.h>
#include <hookers/interface/Frame.h>

#include "gui/Common/BulkSelectionItem.h"
#include "gui/Parts/Part3DItem.h"
#include "gui/Frames/TrackpointItem.h"

#include <opencv2/core/core.hpp>



class FrameViewer : public QGraphicsView
{
	Q_OBJECT

    /** keeps track of active item layer */
    struct SelectionModeState {
        BulkSelectionItem * selectionItem;
        QList<QGraphicsItem*> items;

        SelectionModeState() : selectionItem(0) {}
    };


public:
	enum ViewMode {NormalViewMode = 0, DeltaViewMode};
    enum SelectionMode {NoSelectionMode, TrackSelectionMode, PartSelectionMode};
private:

    typedef QMap<SelectionMode, SelectionModeState> SelectionModeMap;

	ViewMode m_mode;
    QMenu * m_partMenu;
    QGraphicsPixmapItem * m_frameViewItem;

    hookers::interface::Frame m_prevFrame;

    hookers::interface::Frame m_frame;
    hookers::interface::Sequence m_sequence;

    SelectionModeMap m_selections;
    SelectionModeMap::Iterator m_currentSelection;


	QList<TrackpointItem*> m_trackpoints;

    QList<Part3DItem*> m_parts;


    cv::Mat m_cameraMatrix;



public:
	explicit FrameViewer(QWidget *parent = 0);

	/** @returns the current frame. if frame is invalid, then there is no current frame */
    hookers::interface::Frame frame() const;

	/** sets the current frame
	 *  @note if new frame belongs to a project different from former frame,
	 *  the project will be changed as well.
	 */
    void setFrame(const hookers::interface::Frame & frame);

	/** @returns the current frame. */
    hookers::interface::Sequence project() const;

	/** sets a current project.
	 *  @note selects the first frame from project
	 */
    void setSequence(hookers::interface::Sequence &project);


	/** returns a list of all selected tracks.
	 *  the order corresponds to the bulk selection order */
    hookers::interface::TrackList selectedTracks() const;

	/** @returns the last selected track */
    hookers::interface::Track selectedTrack() const;


    hookers::interface::LandmarkList selectedLandmarks() const;


signals:
	void frameChanged();
    void framePoseChanged(const hookers::Vector6d & pose);
	void projectChanged();

    void trackpointSelected(hookers::interface::Track tack);


    void trackFocused(hookers::interface::Track track);

	void newTrackInserted(const QPointF p, QString frameId) ;


public slots:
	/** clears the scene and all connected data */
	void clear();

	/** clears the selection */
	void clearSelection();

	/**
	 * In normal view mode, all trackpoints will be displayed
	 * @see enableDeltaMode()
	 * @brief enables normal view mode.
	 */
	void enableNormalMode();

	/**
	 * In delta view mode, only the difference of tracks between two frames
	 * will be shown. This means when changing the frame from one to another
	 * only the trackpoints that were not present in the former will be displayed
	 * in the new frame.
	 * @brief enables delta view mode
	 */
	void enableDeltaMode();

	/** toggles between the last modes */
	void toggleViewMode();

    /**
     * handles new selection mode.
     * @see cycleSelectionMode();
     */
    void setSelectionMode(SelectionMode mode);

    /** cycles through different selection modes */
    void cycleSelectionMode();

	/** updates all content. */
	void updateContent();

protected slots:
    /** this slots should be called from an menu action that handles checking/unchecking
     * the visibility of a part inside frame viewer */
    void partSelectionActionTriggered();

protected:
	void wheelEvent(QWheelEvent *event);
	void keyPressEvent(QKeyEvent *event);
	void keyReleaseEvent(QKeyEvent *event);

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);

	void enterEvent(QEvent *event);

public:
    void setupSceneContent();
    void updateSceneContent();

    void setupPartContent();
    void updatePartContent();

};

#endif // FRAMEVIEWER_H
