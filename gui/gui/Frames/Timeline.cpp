/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtWidgets/QHBoxLayout>

#include "gui/Frames/Timeline.h"

Timeline::Timeline(QWidget *parent) :
    QWidget(parent)
{

	QHBoxLayout * layout = new QHBoxLayout();
	this->setLayout(layout);

	layout->addWidget(&m_prevprev);
	layout->addWidget(&m_prev);
    layout->addWidget(&m_spinBox);
	layout->addWidget(&m_slider, 1);
	layout->addWidget(&m_next);
	layout->addWidget(&m_nextnext);

	m_prevprev.setText("<<");
	m_prev.setText("<");
	m_next.setText(">");
	m_nextnext.setText(">>");

    m_slider.setMinimum(0);
    m_slider.setOrientation(Qt::Horizontal);
	/* disable sending continous signals, we are only interested
	 * in the final value */
	m_slider.setTracking(false);
	m_slider.setPageStep(20) ;

    m_spinBox.setMinimum(0);
    m_spinBox.setValue(0);
    m_spinBox.setKeyboardTracking(false);

	this->setEnabled(false);

	connect(&m_slider, SIGNAL(valueChanged(int)),
			this, SIGNAL(valueChanged(int)));

    connect(&m_slider, &QSlider::valueChanged,
            &m_spinBox, &QSpinBox::setValue);

    connect(&m_spinBox, SIGNAL(valueChanged(int)),
            &m_slider, SLOT(setValue(int)));

	connect(&m_prev, SIGNAL(clicked()),
			this, SLOT(handlePrev()));

	connect(&m_prevprev, SIGNAL(clicked()),
			this, SLOT(handlePrevPrev()));

	connect(&m_next, SIGNAL(clicked()),
			this, SLOT(handleNext()));

	connect(&m_nextnext, SIGNAL(clicked()),
			this, SLOT(handleNextNext()));

}

void Timeline::setStep(int step)
{
	m_slider.setValue(step);
}

void Timeline::setNumberSteps(int steps)
{
	m_slider.setRange(0, steps);
    m_slider.setRange(0, steps);
    m_spinBox.setMaximum(steps);
	this->setEnabled(true);
}

void Timeline::handleNext()
{
	m_slider.triggerAction(QSlider::SliderSingleStepAdd);
}

void Timeline::handleNextNext()
{
	m_slider.triggerAction(QSlider::SliderPageStepAdd);
}


void Timeline::handlePrev()
{
	m_slider.triggerAction(QSlider::SliderSingleStepSub);
}

void Timeline::handlePrevPrev()
{
	m_slider.triggerAction(QSlider::SliderPageStepSub);
}
