/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIMELINE_H
#define TIMELINE_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QSlider>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>

/** @brief timeline for frames */
class Timeline : public QWidget
{
	Q_OBJECT

	QPushButton m_prev;
	QPushButton m_prevprev;
	QPushButton m_nextnext;
	QPushButton m_next;
    QSpinBox m_spinBox;
	QSlider m_slider;


public:
	explicit Timeline(QWidget *parent = 0);

signals:

	void valueChanged(int step);

public slots:

	void setStep(int step);

	void setNumberSteps(int steps);

protected slots:

	void handleNext();
	void handleNextNext();
	void handlePrev();
	void handlePrevPrev();

};

#endif // TIMELINE_H
