/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui/QBrush>
#include <QtGui/QPen>

#include "gui/Frames/TrackpointItem.h"

using namespace hookers;
using namespace hookers::interface;

TrackpointItem::TrackpointItem(const Track &track) :
    m_track(track), m_isHighlighted(false)
{
	m_dot = new QGraphicsEllipseItem(this);
	m_dot->setOpacity(0.5);
	m_dot->setRect(-5, -5, 10, 10);
	m_dot->setBrush(QBrush(Qt::red));

	QPen pen;
	pen.setColor(Qt::white);
	pen.setCosmetic(true);
	m_dot->setPen(pen);

	m_selection = new SelectionHandlerItem();
	m_selection->setScale(30.0);
	m_selection->setParentItem(this);

	/* indicate that this track is already linked */
	if(track.hasLink())
		m_dot->setBrush(QBrush(Qt::green));

	this->setFlag(ItemIsSelectable, true);
	this->setFlag(ItemIsFocusable, true);

}

QRectF TrackpointItem::boundingRect() const
{
	return this->childrenBoundingRect();
}

void TrackpointItem::paint(QPainter *painter,
						   const QStyleOptionGraphicsItem *option,
						   QWidget *widget)
{

}

QVariant TrackpointItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change == ItemSelectedChange) {
		bool what = value.toBool();
		QPen pen = m_dot->pen();

		if(what) {
			pen.setWidthF(5.0);
			pen.setColor(Qt::yellow);
		} else {
			pen.setWidthF(1.0);
			pen.setColor(Qt::white);
		}

		m_dot->setPen(pen);
		return value;
	} else {
		return QGraphicsItem::itemChange(change, value);
	}


}

void TrackpointItem::setHighlighted(bool highlighted)
{
	/* if value is the same, do nothing in order to avoid
	 * updating the items over an over again */
	if(m_isHighlighted == highlighted)
		return;

	m_isHighlighted = highlighted;
	if(highlighted) {
		m_dot->setScale(3.0);
	} else {
		m_dot->setScale(1.0);
	}
}


