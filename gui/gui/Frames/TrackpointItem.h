/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TRACKPOINTITEM_H
#define TRACKPOINTITEM_H

#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QGraphicsEllipseItem>

#include <hookers/interface/Frame.h>
#include <hookers/interface/Track.h>

#include "gui/Common/SelectionHandlerItem.h"

class TrackpointItem : public QGraphicsItem
{
public:
	enum { Type = UserType + 1000 };
private:

    hookers::interface::Track m_track;
	QGraphicsEllipseItem * m_dot;
	SelectionHandlerItem * m_selection;

	bool m_isHighlighted;

public:
    TrackpointItem(const hookers::interface::Track & track);

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	int type() const {
		return Type;
	}

    const hookers::interface::Track & track() const {
		return m_track;
	}

	void setHighlighted(bool highlighted);

	bool highlighted() const {
		return m_isHighlighted;
	}



protected:
	QVariant itemChange(GraphicsItemChange change, const QVariant &value);
};

#endif // TRACKPOINTITEM_H
