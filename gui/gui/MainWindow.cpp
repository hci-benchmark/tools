/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtCore/QDebug>

#include <QtGui/QVector3D>

#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QAction>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMenu>
#include <QtWidgets/QTabBar>
#include <QtCore/QDir>
#include <QtCore/QCoreApplication>

#include <QtCore/QFile>

#include <QtGui/QKeyEvent>
#include "gui/MainWindow.h"

#include "gui/Frames/FrameViewer.h"
#include "gui/Frames/Timeline.h"
#include "gui/Parts/PartViewer.h"

#include <set>


using namespace hookers;
using namespace hookers::interface;

MainWindow * MainWindow::m_instance = nullptr;

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	
    Q_ASSERT(!m_instance);

    m_instance = this;

    /* set the context in which we are modifiying the project */
    Project::setContext(Project::ManualContext);

	m_splitter = new QSplitter(this);
	this->setCentralWidget(m_splitter);

	m_frameViewer = new FrameViewer();
	m_timeline = new Timeline();

	QWidget * intermediate = new QWidget();
	intermediate->setLayout(new QVBoxLayout());
	intermediate->layout()->setMargin(0);
	intermediate->layout()->addWidget(m_frameViewer);
	intermediate->layout()->addWidget(m_timeline);


    /* setup part/depth viewer side */

    /* we need a tab bar to change the mode between part display and
     * depth map/range annote */
    QTabBar * partTabs = new QTabBar();
    partTabs->addTab("Parts");
    partTabs->addTab("Depth");
    m_partViewer = new PartViewer();
    m_partViewer->setFrameViewer(m_frameViewer);

    QObject::connect(partTabs, &QTabBar::currentChanged,
                     m_partViewer, &PartViewer::setMode);
    QObject::connect(m_frameViewer, &FrameViewer::framePoseChanged,
                     m_partViewer, &PartViewer::setPose);
    partTabs->setCurrentIndex(0);


    /* this is a dummy widget that is only used for layouting */
    QWidget * partIntermediate = new QWidget();
    partIntermediate->setLayout(new QVBoxLayout());
    partIntermediate->layout()->setMargin(0);
    partIntermediate->layout()->addWidget(partTabs);
    partIntermediate->layout()->addWidget(m_partViewer);


	totalExtraTracks = 0 ;

	/* setup splitter */
	m_splitter->addWidget(intermediate);
    m_splitter->addWidget(partIntermediate);

	m_splitter->setStretchFactor(0, 1);
	m_splitter->setStretchFactor(1, 1);

	this->setStatusBar(new QStatusBar());

	this->statusBar()->showMessage("Welcome! Press <" +
	                               QKeySequence(QKeySequence::Open).toString() +
	                               "> to load a project.");
    QObject::connect(m_partViewer, &PartViewer::userMessage,
                     this->statusBar(), &QStatusBar::showMessage);

	this->setMenuBar(new QMenuBar(this));
	QMenu * projectMenu = this->menuBar()->addMenu("&Project");
	QMenu * framesMenu = this->menuBar()->addMenu("&Frames");
	QMenu * trackMenu = this->menuBar()->addMenu("&Tracks");
	QMenu * partMenu = this->menuBar()->addMenu("&Parts");


	/* PROJECT */
	QAction * openAction = new QAction("Open a Project", this);
	openAction->setShortcut(QKeySequence::Open);
	connect(openAction, SIGNAL(triggered()),
			this, SLOT(loadProject()));
	projectMenu->addAction(openAction);

    QAction * exportAction = new QAction("&Sample Frame", this);
	exportAction->setShortcut(QKeySequence::SaveAs);
    connect(exportAction, &QAction::triggered,
            m_partViewer, &PartViewer::sampleFrame);
	projectMenu->addAction(exportAction);

	QAction * closeAction = new QAction("Close", this);
	closeAction->setShortcut(QKeySequence::Close);
	connect(closeAction, SIGNAL(triggered()),
			this, SLOT(closeProject()));
	projectMenu->addAction(closeAction);



	/* FRAMES */
	QAction * nextFrameAction = new QAction("Next", this);
	nextFrameAction->setShortcut(QKeySequence(Qt::Key_Right));
	connect(nextFrameAction, SIGNAL(triggered()),
			this, SLOT(nextFrame()));
	framesMenu->addAction(nextFrameAction);

	QAction * prevFrameAction = new QAction("Previous", this);
	prevFrameAction->setShortcut(QKeySequence(Qt::Key_Left));
	connect(prevFrameAction, SIGNAL(triggered()),
			this, SLOT(previousFrame()));
	framesMenu->addAction(prevFrameAction);

	QAction * toggleFrameViewModeAction = new QAction("Toggle Mode", this);
	toggleFrameViewModeAction->setShortcut(QKeySequence(Qt::Key_T));
	connect(toggleFrameViewModeAction, SIGNAL(triggered()),
	        m_frameViewer, SLOT(toggleViewMode()));
	framesMenu->addAction(toggleFrameViewModeAction);


    QAction * cycleFrameSelectionModeAction = new QAction("Cycle Selection Mode", this);
    cycleFrameSelectionModeAction->setShortcut(QKeySequence(Qt::Key_Tab));
    connect(cycleFrameSelectionModeAction, SIGNAL(triggered()),
            m_frameViewer, SLOT(cycleSelectionMode()));
    framesMenu->addAction(cycleFrameSelectionModeAction);



	/* TRACKS */
	QAction * linkTrackAction = new QAction("Link with Feature", this);
	linkTrackAction->setShortcut(QKeySequence(Qt::Key_L));
	connect(linkTrackAction, SIGNAL(triggered()),
			this, SLOT(linkSelected()));
	trackMenu->addAction(linkTrackAction);

	QAction * unlinkTrackAction = new QAction("Unlink Track", this);
	unlinkTrackAction->setShortcut(QKeySequence(Qt::Key_U));
	connect(unlinkTrackAction, SIGNAL(triggered()),
			this, SLOT(unlinkSelected()));
	trackMenu->addAction(unlinkTrackAction);

	QAction * importPartAction = new QAction("Import Part", this);
	importPartAction->setShortcut(QKeySequence(Qt::Key_I, Qt::Key_P));
	connect(importPartAction, SIGNAL(triggered()),
			this, SLOT(importPart()));
	partMenu->addAction(importPartAction);



	/* general stuff */
	connect(m_frameViewer->scene(), SIGNAL(selectionChanged()),
	        this, SLOT(handleTrackSelected()));
	connect(m_partViewer->scene(), SIGNAL(selectionChanged()),
	        this, SLOT(handleFeatureSelected()));

	connect(m_frameViewer, SIGNAL(trackFocused(Track)),
	        this, SLOT(handleTrackFocused(Track)));

	connect(m_frameViewer, SIGNAL(trackpointSelected(Track)),
		this, SLOT(handleTrackHovered(Track)));	

	connect(m_timeline, SIGNAL(valueChanged(int)),
	        this, SLOT(handleTimelineChanges(int)));

	connect(m_frameViewer, SIGNAL(newTrackInserted(const QPointF, QString)),
		this, SLOT(insertNewTrack(const QPointF, QString))) ;

}


void MainWindow::nextFrame()
{
    qWarning() << "[WARNING] next frame command is currently not supported";
//	if(!m_project.isValid())
//		return;

//    FrameList frames = m_sequence.frames();
//	const Frame & curFrame = m_frameViewer->frame();

//    FrameList::const_iterator it = std::find(frames.cbegin(), frames.cend(), curFrame);

//    /* current frame cannot be localized */
//    if(it == frames.cend())
//        return;

//    if(it != std::prev(frames.end())) {
//        it++;
//        m_frameViewer->setFrame(*it);
//        m_timeline->setStep(std::distance(frames.cbegin(), it));
//    }
}

void MainWindow::previousFrame()
{
    qWarning() << "[WARNING] prev frame command is currently not supported";
//    if(!m_sequence.isValid())
//		return;

//    if(!m_project.isValid())
//        return;

//    FrameList frames = m_sequence.frames();
//    const Frame & curFrame = m_frameViewer->frame();

//    FrameList::const_iterator it = std::find(frames.cbegin(), frames.cend(), curFrame);

//    /* current frame cannot be localized */
//    if(it == frames.cend())
//        return;

//    if(it != frames.cbegin()) {
//        it--;
//        m_frameViewer->setFrame(*it);
//        m_timeline->setStep(std::distance(frames.cbegin(), it));
//    }

}



void MainWindow::loadProject(QString path)
{


	this->setCursor(QCursor(Qt::WaitCursor));

//	/* if no path is given, ask user via file dialog */
//	if(path.isNull()) {
//		path = QFileDialog::getOpenFileName(this, "Select project", QString(), "boshgt project (*.h5)");

//		if(path.isNull())
//			return;
//	}
//	QString framePath = QFileDialog::getExistingDirectory(this, "Select path to frames (Cancel to use project internal path)", QFileInfo(path).absoluteDir().path()) ;

	/* update status bar and process window events before starting */
	this->statusBar()->showMessage("Loading project...");
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

	/* if there is an active project,
	 * close it before opening a new one */
	if(m_project.isValid())
		this->closeProject();

	/* load a new project by given file */
    std::string projectName;
    m_project = Project::open(projectName);


	/* check if the loaded project is valid. if not, we are finished here */
	if(!m_project.isValid())
		return;

    SequenceList sequences = m_project.sequences();

    if(sequences.empty())
        return;

    /** @todo select which sequence we want to use */
    if(!m_sequenceName.isNull()) {
        m_sequence = sequences.byLabel(m_sequenceName.toStdString());
    } else {
        m_sequence = sequences.front();
    }

	/* tell the project views about the new project */
    m_frameViewer->setSequence(m_sequence);
    m_frameViewer->setupPartContent();
	m_frameViewer->fitInView(m_frameViewer->scene()->sceneRect(), Qt::KeepAspectRatio);

	/* setup the timeline */
    FrameList frames = m_sequence.frames();
    m_timeline->setNumberSteps(frames.size()-1);



	m_partViewer->setProject(m_project);
	m_partViewer->fitInView(m_partViewer->scene()->sceneRect(), Qt::KeepAspectRatio);

	this->setCursor(QCursor());

	emit projectLoaded();
}

void MainWindow::closeProject()
{
	/* clear all views */
	m_frameViewer->clear();
	m_partViewer->clear();
	m_timeline->setEnabled(false);

	/* delete reference to project.
	 * this will close the underlying project file as well */
    m_project.save();
    m_project = Project();
    m_sequence = Sequence();
}

void MainWindow::handleFeatureSelected()
{
    Landmark landmark = m_partViewer->selectedFeature();

    if(!landmark.isValid())
		return;

	this->statusBar()->showMessage("Feature selected at ( "
                                   + QString::number(landmark.pos()[0]) + ", "
                                   + QString::number(landmark.pos()[1]) + ", "
                                   + QString::number(landmark.pos()[2]) + ")");
}

void MainWindow::handleTrackSelected()
{
	Track track = m_frameViewer->selectedTrack();

	if(!track.isValid())
		return;

//	this->statusBar()->showMessage("Track selected: " + track.id());
}

void MainWindow::handleTrackHovered(Track track)
{
//	this->statusBar()->showMessage(QString("Track Length: %1").arg(track.trackLength())) ;
}

void MainWindow::handleTrackFocused(Track track)
{

    Landmark landmark;
    if(track.isValid())
        landmark = track.linkedLandmark();

    m_partViewer->focusFeature(landmark);
}

void MainWindow::handleTimelineChanges(int step)
{
    FrameList frames = m_sequence.frames();

    Frame cur = frames.value(step, Frame());

	if(!cur.isValid())
		return;

	m_frameViewer->setFrame(cur);
	this->statusBar()->showMessage(QString("Frame: %1").arg(step)) ;
}

void MainWindow::linkSelected()
{

	if(!m_project.isValid())
		return;

    TrackList tracks = m_frameViewer->selectedTracks();
    LandmarkList landmarksPartviewer = m_partViewer->selectedFeatures();
    LandmarkList landmarksFrameviewer = m_frameViewer->selectedLandmarks();

    LandmarkList & landmarks = landmarksPartviewer;
    if(landmarksPartviewer.empty())
        landmarks = landmarksFrameviewer;

    if(tracks.size() != landmarks.size()) {
		this->statusBar()->showMessage("Not Linking: Number of selected tracks and features does not match.");
		qDebug() << "*** Not Linking: Number of selected tracks and features does not match.";
		return;
	}

	/* decide whether to show a generalized message or a specific one */
//	bool showOne = (tracks.size()>1)?false:true;
	int numLinkings = tracks.size();

    auto itTrack = tracks.begin();
    auto itLandmark = landmarks.begin();

    while(itTrack != tracks.end() && itLandmark != landmarks.end()) {
        Track & track = *itTrack;
        Landmark & landmark = *itLandmark;

        if(!track.isValid() || !landmark.isValid())
			continue;

        track.linkToLandmark(landmark);

        qDebug() << "*** Linking track "
                 << "(" << track.posInFrame(m_frameViewer->frame())[0]  << ","
                 << track.posInFrame(m_frameViewer->frame())[1]  << ")"
                 << "with"
                 << landmark.pos()[0] << landmark.pos()[1] << landmark.pos()[2];
//		if(showOne)
//			this->statusBar()->showMessage("Linking track \""
//		                                   + track.id()
//		                                   + "\" with feature \""
//                                           + landmark.id() +"\"");

        itTrack++;
        itLandmark++;
	}

	this->statusBar()->showMessage("Linking "
	                               + QString::number(numLinkings)
	                               + " Tracks with Features.");

	/* update the frame viewer */
	m_frameViewer->clearSelection();
	m_partViewer->clearSelection();

	m_frameViewer->updateContent();

}

void MainWindow::unlinkSelected()
{
    TrackList tracks = m_frameViewer->selectedTracks();

	foreach(Track t, tracks) {

		if(!t.isValid())
			continue;

        t.unlinkLandmark();

//		this->statusBar()->showMessage("Unlinking Track \"" + t.id() + "\"");
//		qDebug() << "*** Unlinking track " << t.id();
	}

	/* update the frame view */
	m_frameViewer->updateContent();
}

void MainWindow::importPart(const QString &path)
{

	this->setCursor(QCursor(Qt::WaitCursor));

	QString p = path;
	if(p.isNull()) {
		p = QFileDialog::getOpenFileName(this, "Select Part file","e:\\");
	}

	if(p.isNull())
		return;

	qDebug() << "Opening file: " << p;

	QFile partFile(p);

	partFile.open(QFile::ReadOnly);

	if(!partFile.isOpen())
		return;
	QString fileName = p.split('/').last();
	
    QList<Vector3d> features;

	while (!partFile.atEnd()) {
		QByteArray lineBuf = partFile.readLine();
		QStringList nums = QString::fromUtf8(lineBuf).trimmed().split(",");
		if(nums.size() != 3)
			continue;

        Vector3d f;
        f[0] = nums[0].toFloat();
        f[1] = nums[1].toFloat();
        f[2] = nums[2].toFloat();
		features.append(f);
	}


    //Part part = m_project.addPart(fileName.toStdString());
    Part part;

	if(!part.isValid())
		return;

    foreach(const Vector3d & f, features) {
        /** @todo enable again using nearest neighbour search in geometry */
//        part.addLandmark(f);
	}

	qDebug() <<  "*** Import Part: " << fileName;
	this->statusBar()->showMessage("Imported Part from \"" + fileName + "\"");

	m_partViewer->clear();
	m_partViewer->setProject(m_project);

    m_project.save();

	this->setCursor(QCursor());


}

