/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSplitter>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>

#include "gui/Frames/Timeline.h"
#include "gui/Frames/FrameViewer.h"
#include "gui/Parts/PartViewer.h"

class PartViewer;

class MainWindow : public QMainWindow
{
	Q_OBJECT

	QSplitter * m_splitter;
	PartViewer * m_partViewer;
	Timeline * m_timeline;
	FrameViewer * m_frameViewer;

    hookers::interface::Project m_project;
    hookers::interface::Sequence m_sequence;


	//additions for hacked in track extension
	QMap<int, QList<QPair<QPointF, QVector3D> > > extraTracks ;
	int totalExtraTracks ;

    static MainWindow * m_instance;

public:
	explicit MainWindow(QWidget *parent = 0);

	/** @returns the current project */
    hookers::interface::Sequence project() const;

    /** name of the sequence we want to open */
    QString m_sequenceName;

protected:

signals:
	void projectLoaded();

public slots:


	void nextFrame();
	void previousFrame();


	/** loads a project from given path */
	void loadProject(QString path = QString());

	/** closes the current project */
	void closeProject();

private slots:
	void handleFeatureSelected();
	void handleTrackSelected();

    void handleTrackHovered(hookers::interface::Track track) ;

    void handleTrackFocused(hookers::interface::Track track);

	void handleTimelineChanges(int step);

	void linkSelected();
	void unlinkSelected();

	/** import from textfile with following format per line "x,y,z" as features in a new part */
	void importPart(const QString & path = QString());

public:
    static MainWindow * instance() {
        return m_instance;
    }


};


#endif // MAINWINDOW_H
