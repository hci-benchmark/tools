/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsRectItem>
#include <QtGui/QBrush>
#include <QtGui/QPen>

#include "gui/Common/SelectionHandlerItem.h"
#include "gui/Parts/FeatureItem.h"

using namespace hookers;
using namespace hookers::interface;

FeatureItem::FeatureItem(const Landmark &landmark) :
    m_landmark(landmark), m_isHighlighted(false)
{
	this->setFlag(ItemIsSelectable, true);
	this->setFlag(ItemIsFocusable, true);

	QGraphicsRectItem * shape = new QGraphicsRectItem(this);
	shape->setRect(-0.50, -0.50, 1.0, 1.0);

	m_selection = new SelectionHandlerItem();
	m_selection->setScale(3.0);
	m_selection->setParentItem(this);


	QPen pen;
	pen.setColor(Qt::red);
	pen.setWidthF(1.0);
	pen.setCosmetic(true);

	shape->setPen(pen);
	shape->setBrush(QBrush(Qt::blue));
	shape->setOpacity(0.9);
	m_shape = shape;

}

QRectF FeatureItem::boundingRect() const
{
	return this->childrenBoundingRect();
}

QPainterPath FeatureItem::shape() const
{
	QPainterPath p;
	p.addRect(this->boundingRect());
	return p;
}

void FeatureItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{

}

QList<FeatureItem *> FeatureItem::collidingFeatureItems() const
{
	QList<QGraphicsItem*> collisions = this->collidingItems(Qt::IntersectsItemBoundingRect);
	QList<FeatureItem*> featureCollisions;

	foreach(QGraphicsItem * i, collisions) {
		FeatureItem * c = qgraphicsitem_cast<FeatureItem*>(i);

		/* add this item to the list if it can be casted to a FeatureItem */
		if(c)
			featureCollisions.append(c);
	}

	return featureCollisions;
}

void FeatureItem::setHighlighted(bool highlight)
{

	if(m_isHighlighted == highlight)
		return;

	m_isHighlighted = highlight;

	if(highlight) {
		this->ensureVisible();
		m_shape->setScale(3.0);
	} else {
		m_shape->setScale(1.0);
	}

}

QVariant FeatureItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change == ItemSelectedHasChanged) {
		if(value.toBool()) {
			m_shape->setBrush(QBrush(Qt::yellow));
		} else {
			m_shape->setBrush(QBrush(Qt::blue));
		}
	}

	return QGraphicsItem::itemChange(change, value);

}

