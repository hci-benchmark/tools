/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FEATUREITEM_H
#define FEATUREITEM_H

#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QAbstractGraphicsShapeItem>

#include <hookers/interface/Landmark.h>

#include "gui/Common/SelectionHandlerItem.h"


class FeatureItem : public QGraphicsItem
{

public:
	enum { Type = UserType + 2001 };

private:

	QAbstractGraphicsShapeItem * m_shape;
	SelectionHandlerItem * m_selection;
    hookers::interface::Landmark m_landmark;

	bool m_isHighlighted;

public:
    FeatureItem(const hookers::interface::Landmark & landmark);

	QRectF boundingRect() const;
	QPainterPath shape() const;

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	int type() const {
		return Type;
	}

	/** returns a list of all colliding Feature Items */
	QList<FeatureItem*> collidingFeatureItems() const;

    const hookers::interface::Landmark & feature() {
        return m_landmark;
	}

	bool isHighlighted() const {
		return m_isHighlighted;
	}

	void setHighlighted(bool highlight);

protected:
	QVariant itemChange(GraphicsItemChange change, const QVariant &value);


private:



};

#endif // FEATUREITEM_H
