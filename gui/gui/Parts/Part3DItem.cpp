/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <QtCore/QDebug>

#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsRectItem>

#include <QtGui/QPainter>
#include <QtGui/QBrush>
#include <QtGui/QPen>

#include <QtCore/qmath.h>
#include <QtGui/QQuaternion>
#include <QtGui/QVector3D>

#include <vigra/multi_pointoperators.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/unsupervised_decomposition.hxx>
#include <vigra/quaternion.hxx>

// Hookers Interface
#include <hookers/interface/Track.h>
#include <hookers/interface/TrackList.h>
#include <hookers/interface/Landmark.h>
#include <hookers/interface/LandmarkList.h>

// Hookers Toolbox
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>


#include "gui/Parts/Part3DItem.h"
#include "gui/Parts/PartScene.h"

using namespace hookers;
using namespace hookers::interface;

Part3DItem::Part3DItem(const Part &part, const cv::Mat &cameraMatrix) :
    m_part(part), m_cameraMatrix(cameraMatrix)
{

	Q_ASSERT(m_part.isValid());

	/* read in all points and features */
    this->readFeatures();

    this->setToolTip(QString::fromStdString(m_part.name()));

//	this->setFlag(ItemIsMovable, true);


}


void Part3DItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->save();
    painter->setOpacity(0.5);
    painter->setPen(Qt::gray);
    painter->setBrush(QBrush(Qt::darkGray));
    //painter->drawRect(this->childrenBoundingRect());
    painter->restore();
}

QVariant Part3DItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    return value;
}

void Part3DItem::readFeatures()
{
    if(!m_part.isValid())
        return;

    LandmarkList landmarks = m_part.landmarks();

    for(Landmark & l : landmarks) {
        FeatureItem * li = new FeatureItem(l);
        m_features.push_back(li);
        li->setParentItem(this);
        li->setFlag(QGraphicsItem::ItemIgnoresTransformations);
        li->setScale(10);
    }
}

void Part3DItem::updatePositions()
{

    if(m_cameraMatrix.empty() || m_tvec.empty() || m_rvec.empty()) {
        qDebug() << "Camera matrix empty / pose invalid";
        throw(std::string("Camera matrix empty / pose invalid"));
        this->setVisible(false);
        return;
    }

    cv::Rect ROI;
    if(!boundingRect().isNull())
        ROI = cv::Rect(boundingRect().x(), boundingRect().y(),
                       boundingRect().width(), boundingRect().height());


    /* get the 3d position of all landmarks and place it into a list */
    std::vector<cv::Point3f> positions(m_features.size());
    for(int i = 0; i < m_features.size(); i++) {
        Landmark l = m_features[i]->feature();
        positions[i] = cv::Point3f(l.pos()[0], l.pos()[1], l.pos()[2]);
    }


    /* perspective projection of landmarks wrt. to camera pose and intrinsics */
    std::vector<cv::Point2f> positions2d(m_features.size());
    {
        cv::projectPoints(positions, m_rvec, m_tvec, m_cameraMatrix, cv::noArray(), positions2d);
    }

    /* orthogonal projection of landmarks wrt to camera pose. we need this transformation
     * to filter out points that lie behind the camera. */
    cv::Mat positions3d;
    {
        cv::Mat R;
        cv::Rodrigues(m_rvec, R);
        R.convertTo(R, CV_32F);

        cv::Point3f T(m_tvec.at<float>(0), m_tvec.at<float>(1), m_tvec.at<float>(2));
        std::vector<cv::Point3f> Ts;
        Ts.assign(m_features.size(), T);

        positions3d = R*(cv::Mat(positions).reshape(1).t()) + cv::Mat(Ts).reshape(1).t();
    }


    for(int i = 0; i < m_features.size(); i++) {

        try {

            if(!ROI.contains(positions2d[i]))
                throw false;
            if(positions3d.col(i).at<float>(2) < 0.0)
                throw false;

            m_features[i]->setPos(positions2d[i].x, positions2d[i].y);
            m_features[i]->setVisible(true);
            m_features[i]->setEnabled(true);
        } catch (...) {
            m_features[i]->setVisible(false);
            m_features[i]->setEnabled(false);
        }
    }
}

