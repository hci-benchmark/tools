/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HOOKERS_GUI_PART3DITEM_H
#define HOOKERS_GUI_PART3DITEM_H


#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <QtWidgets/QGraphicsItem>

#include <hookers/interface/Part.h>

#include "gui/Parts/FeatureItem.h"



class Part3DItem : public QGraphicsItem
{

public:
    enum { Type = UserType + 2001 };

private:

	QPointF m_center;

    hookers::interface::Part m_part;

	QList<FeatureItem*> m_features;

    cv::Mat m_cameraMatrix;
    cv::Mat m_tvec;
    cv::Mat m_rvec;

    hookers::Vector6d cameraPose;
    hookers::Vector4d intrinsics;

    QRectF m_boundingRect;

public:
    Part3DItem(const hookers::interface::Part & part,
               const cv::Mat & cameraMatrix = cv::Mat());

    QRectF boundingRect() const {
        return m_boundingRect;
    }

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	int type() const {
		return Type;
	}

	/** @todo replace this by better interfacing */
	QList<FeatureItem*> features() {
		return m_features;
	}

    hookers::interface::Part part() {
		return m_part;
	}


    void setBoundingRect(const QRectF & rect) {
        m_boundingRect = rect;
    }

    void setCameraPose(const cv::Mat rvec, const cv::Mat tvec) {
        tvec.convertTo(m_tvec, CV_32F);
        rvec.convertTo(m_rvec, CV_32F);
    }

    void setIntrinsics(const hookers::Vector4d & intr) {
        intrinsics = intr;
    }
    void setCameraPose(const hookers::Vector6d & pos) {
        cameraPose = pos;
    }






protected:
	QVariant itemChange(GraphicsItemChange change, const QVariant &value);

public:

    /** sets up the features for given part */
    void readFeatures();

    /** updates the position of all features, given the current
     *  camera pose and calibration */
    void updatePositions();

};

#endif // HOOKERS_GUI_PART3DITEM_H
