/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <QtCore/QDebug>

#include <QtWidgets/QGraphicsEllipseItem>
#include <QtWidgets/QGraphicsRectItem>

#include <QtGui/QPainter>
#include <QtGui/QBrush>
#include <QtGui/QPen>

#include <QtCore/qmath.h>
#include <QtGui/QQuaternion>
#include <QtGui/QVector3D>

#include "gui/Parts/PartItem.h"
#include "gui/Parts/PartScene.h"

#include <vigra/multi_pointoperators.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/unsupervised_decomposition.hxx>
#include <vigra/quaternion.hxx>

using namespace hookers;
using namespace hookers::interface;

PartItem::PartItem(const Part &part) :
	m_scene(0), m_part(part)
{

	Q_ASSERT(m_part.isValid());

	/* read in all points and features */
	this->readGeometry();

	m_containerItem = new QGraphicsRectItem(this);
	m_containerItem->setRect(this->boundingRect());

	QPen pen;
	pen.setColor(Qt::darkGray);
	pen.setCosmetic(true);
	pen.setWidthF(1.0);
	m_containerItem->setPen(pen);
	m_containerItem->setBrush(Qt::lightGray);
	m_containerItem->setOpacity(0.4);

    this->setToolTip(QString::fromStdString(m_part.name()));

	this->setFlag(ItemIsMovable, true);


}

QRectF PartItem::boundingRect() const
{
	return this->childrenBoundingRect();
}

void PartItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	painter->setOpacity(0.5);
}

QVariant PartItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change == ItemPositionHasChanged) {


        /** @todo set offset position on the part. */
//		m_part.setPosition(QVector2D(value.toPointF()-m_center));

		/* notify the about this change, to recalculate the scene rect */
		if(m_scene)
			m_scene->partMoved(this);

		return value;
	} else if(change == ItemSceneHasChanged){

		this->doAutoScaling();

		m_scene = qobject_cast<PartScene*>(value.value<QObject*>());
		if(m_scene)
			m_scene->partMoved(this);
		return value;
	} else {
		return QGraphicsItem::itemChange(change, value);
	}
}

void PartItem::readGeometry()
{
    /* get a list of all landmarks inside this part */
    const LandmarkList landmarks = m_part.landmarks();

    /* get an array of all landmark positions */
    Vector3fArray landmarksPos;
    landmarks.getPositions(landmarksPos);

    /* calculate a zero mean version of all positions */
    Array2fView landmarksPosExpanded(landmarksPos.expandElements(0));
    Array2f landmarksPosMeanFree(landmarksPosExpanded.shape());
    vigra::linalg::prepareRows(landmarksPosExpanded, landmarksPosMeanFree, vigra::ZeroMean);

    /* calculate the principal components in order to find out
     * the direction with least variance.
     * This direction equals to the surface normal of a plane,
     * which is a good assumption given that a part in most cases
     * is a building's fassade.
     *
     * fz are the resulting principal components where each column
     * corresponds to a PC vector.
     */
    int numFeatures = landmarksPosExpanded.shape(0);
    int numSamples = landmarksPosExpanded.shape(1);
    int numComponents = 3;

    //std::cout << m_part.name() << " " << numFeatures << "," << numSamples << std::endl;

    vigra::Matrix<float> fz(numFeatures, numComponents);
    vigra::Matrix<float> zv(numComponents, numSamples);
    vigra::principalComponents(landmarksPosMeanFree, fz, zv);


    /* calculate the center (mean) of all landmark points.
     * the features (coordinates) are along the first axis.
     * so in order to compute the mean value, we have to reduce the second
     * axis. */
    vigra::Shape2 centerPosShape = landmarksPosExpanded.shape();
    centerPosShape[1] = 1;

    vigra::Matrix<float> centerPos(centerPosShape);
    landmarksPosExpanded.sum(centerPos);
    centerPos /= float(numSamples);

    /* calculate the rotation axis and the angle such that the surface normal
     * of the part will point into z-direction after transformation */
    QVector3D center(centerPos(0, 0),centerPos(1, 0), centerPos(2, 0));
    QVector3D pca2 = QVector3D(fz(0,2), fz(1,2), fz(2,2));
    pca2.normalize();
    QVector3D rotAxis = QVector3D::crossProduct(QVector3D(0,0,1), pca2);
    qreal angle = -qAcos(QVector3D::dotProduct(QVector3D(0,0,1), pca2))/M_PI*180.0;
    QQuaternion rot = QQuaternion::fromAxisAndAngle(rotAxis, angle);



    /* Add features toPartItem.
     * Note that number of columns in landmarksPos is equal to the number of
     * landmarks in landmarks-list. So we can safely iterate over the landmark list
     * and access the corresponding entries in lPos by the same index */
    for(unsigned int i = 0; i < landmarks.size(); i++) {
        FeatureItem * item = new FeatureItem(landmarks[i]);

        if(!item)
			continue;

        QVector3D landmarkPos(landmarksPosMeanFree(0,i),
                              landmarksPosMeanFree(1,i),
                              landmarksPosMeanFree(2,i));

        /* we are using the previously computed transform in order to reproject
         * each landmark on the down folded map */
        item->setPos(rot.rotatedVector(landmarkPos).toPointF());

        item->setParentItem(this);
        m_features.append(item);
	}

    m_center = center.toPointF();

    /** @todo read offset of part in part viewer and apply offset here */
    this->setPos(center.toPointF());

}

void PartItem::doAutoScaling()
{
	/* calculate the area of this part in order to rescale all items */
	qreal area = this->boundingRect().x()*this->boundingRect().y();

	/* set the scale of items such that initially all items occupy
	 * X percent of the whole area, assuming that the shape of the feature item
	 * is a rectangle */

	qreal itemScale = 0.05*area;

	foreach(FeatureItem * f, m_features) {
		f->setScale(itemScale);
	}


	/** @todo items auto scaling: this can be done a little bit more analytic,
	 *  eg by calculating the smallest distance of all items instead of iterating */

	/* handle automatic scaling of features when they overlap.
	 * go through all items and check if they are overlapping somewhere
	 */
//	const double RESCALE = 3.0/4.0;

	/* store the maximum rescaling */
	double maxScale = 1.0;
	
//#if 0
//	foreach(FeatureItem * f, m_features) {
//		/* for each FeatureItem, rescale all colliding neighbours by a fixed factor
//		 * as long as no one of them collides again
//		 */
//		QList<FeatureItem*> collisions = f->collidingFeatureItems();

		
//		if(!collisions.isEmpty()) {

//			/* make current feature and all colliding features smaller */
//			f->setScale(f->scale()*RESCALE);
//			foreach(FeatureItem * c, collisions) {

//				double newScale = c->scale()*RESCALE;

//				if(newScale < maxScale)
//					maxScale = newScale;

//				c->setScale(newScale);
//			}

//			/* check again if still some items are colliding */
//			collisions = f->collidingFeatureItems();
//		}
//	}
//#endif
	foreach (FeatureItem * i, m_features) {
		i->setScale(maxScale);
	}

}
