/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PARTITEM_H
#define PARTITEM_H

#include <QtWidgets/QGraphicsItem>

#include <hookers/interface/Part.h>

#include "gui/Parts/FeatureItem.h"




class PartScene;

class PartItem : public QGraphicsItem
{

public:
	enum { Type = UserType + 2000 };

private:

	PartScene * m_scene;

	QPointF m_center;

    hookers::interface::Part m_part;
	QGraphicsRectItem * m_containerItem;

	QList<FeatureItem*> m_features;

public:
    PartItem(const hookers::interface::Part & part);

	QRectF boundingRect() const;

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

	int type() const {
		return Type;
	}

	/** @todo replace this by better interfacing */
	QList<FeatureItem*> features() {
		return m_features;
	}

    hookers::interface::Part part() {
		return m_part;
	}




protected:
	QVariant itemChange(GraphicsItemChange change, const QVariant &value);

private:
	void readGeometry();
	void doAutoScaling();

};

#endif // PARTITEM_H
