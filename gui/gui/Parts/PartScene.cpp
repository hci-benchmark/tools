#include "PartScene.h"

PartScene::PartScene(QObject *parent) :
    QGraphicsScene(parent)
{
}

void PartScene::partMoved(PartItem *part)
{
	m_partsRect |= part->mapToScene(part->boundingRect()).boundingRect();
	this->setSceneRect(m_partsRect);
}
