/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PARTSCENE_H
#define PARTSCENE_H

#include <QtWidgets/QGraphicsScene>

#include "gui/Parts/PartItem.h"


class PartScene : public QGraphicsScene
{
	friend class PartItem;

	QRectF m_partsRect;

	Q_OBJECT
public:
	explicit PartScene(QObject *parent = 0);

signals:

public slots:

private:
	void partMoved(PartItem * part);

};

#endif // PARTSCENE_H
