/*
	Copyright (C) 2013 Karsten Krispin <karsten.krispin@iwr.uni-heidelberg.de>

	This is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This software is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this software.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include <GL/glew.h>

#include <QtGui/QWheelEvent>
#include <QtWidgets/QFileDialog>
#include <QtCore/QDebug>
#include <QOpenGLWidget>
#include <QPoint>
#include <QPointF>
#include <QTime>
#include <QTimer>


// Hookers Toolbox
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>
#include <hookers/toolbox/Projection/SoftwareProjection.h>
#include <hookers/toolbox/Sampling/StereoSampling.h>
#include <hookers/toolbox/Sampling/GroundtruthSource.h>
#include <hookers/toolbox/export/Sinks/MapExporterSink.h>
#include <hookers/toolbox/RunningStats.h>




#include "gui/Common/BulkSelectionItem.h"
#include "gui/Common/SelectionHandlerItem.h"
#include "gui/Parts/PartViewer.h"
#include "gui/Parts/PartItem.h"
#include "gui/Parts/PartScene.h"

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

/**
 * @brief type2str Get a human readable version of the cv::Mat::type value.
 * @param type The value returned by yourMat.type() where yourMat is a cv::Mat.
 * @return A human readable string describing the type of yourMat.
 */
std::string type2str(const int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

void PartViewer::colorMap(cv::Mat& src, cv::Mat& dst, int repetitions, int mapType) {
    // accept only char type matrices
    CV_Assert(CV_32FC1 == src.depth());
    if (repetitions < 0) {
        repetitions = 0;
    }

    const int nRows = src.rows;
    const int nCols = src.cols;
    dst = cv::Mat(nRows, nCols, CV_8UC3);

    cv::normalize(src, src, 0, 255 + 256*repetitions, cv::NORM_MINMAX, CV_32SC1);

    int i,j;
    int* p;
    for( i = 0; i < nRows; ++i)
    {
        p = src.ptr<int>(i);
        for ( j = 0; j < nCols; ++j)
        {
            p[j] = p[j] % 256;
        }
    }
    src.convertTo(src, CV_8UC1);
    cv::applyColorMap(src, dst, mapType);
}

PartViewer::PartViewer(QWidget *parent) :
	QGraphicsView(parent), m_selection(0)
{

	/* create new scene */
	this->setScene(new PartScene(this));

	/* enable interaction to moving PartItems by dragging */
	this->setInteractive(true);

	/* disable anchors, on which the view tries to align the scene after
	 * something has changed */
	this->setResizeAnchor(NoAnchor);
	this->setTransformationAnchor(NoAnchor);
//	this->setTransformationAnchor(AnchorUnderMouse);

	/* enable of panning in the scene */
	this->setDragMode(ScrollHandDrag);

	this->setRenderHint(QPainter::Antialiasing, true);

	/* disable scrollbars */
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	this->setMouseTracking(true);

    /* set invalid project */
    this->setProject(Project());



}

void PartViewer::setProject(const Project &project)
{

    /* remove all existing items from the scene */
    this->clear();

    m_project = project;
    this->setMode(0);

    if (m_project.isValid()) {
        getCloud();
        getTriangles();
    }
}

LandmarkList PartViewer::selectedFeatures() const
{
	if(!m_selection)
        return LandmarkList();

    LandmarkList landmarks;

	QList<QGraphicsItem*> items = m_selection->selectedItems();
	foreach(QGraphicsItem * i, items) {
		FeatureItem * f = qgraphicsitem_cast<FeatureItem*>(i);
		if(!f || !f->feature().isValid())
			continue;

        landmarks.push_back(f->feature());
	}

    return landmarks;
}

Landmark PartViewer::selectedFeature() const
{
    LandmarkList selectedFeatures = this->selectedFeatures();
    if(selectedFeatures.empty())
        return Landmark();

    return selectedFeatures.back();
}

void PartViewer::focusFeature(Landmark feature)
{
	const QList<FeatureItem*> featureItems = m_features;
	foreach(FeatureItem * f, featureItems) {
		/* unhighlight all items except the one item
		 * that represents given feature */
		f->setHighlighted(false);
		if(f->feature() == feature) {
			f->setHighlighted(true);
		}
	}
}

void PartViewer::clear()
{
    m_project = Project();
    this->clearItems();
}

void PartViewer::clearItems() {
    m_selection = nullptr;
    m_features.clear();
    m_parts.clear();

    if(scene())
        this->scene()->clear();

}

void PartViewer::clearSelection()
{
	if(m_selection) {
		delete m_selection;
		m_selection = 0;
	}

	if(scene()) {
		scene()->clearSelection();
    }
}

void PartViewer::setMode(int mode)
{
    currentMode = mode;
    qDebug() << "PartViewer: setting mode to" << mode;

    this->clearItems();

    /* check if we can load anything and handle the false case */
    if(!m_project.isValid()) {
         /* default part mode */
        qDebug() << "Szene invalid";
        scene()->addText("No project loaded....");
        this->fitInView(scene()->sceneRect(), Qt::KeepAspectRatio);
    }

    if(mode == 0) {  /* default part mode */

        const PartList parts = m_project.parts();

        /* for each part, add a part item.
         * PartItems are responsible to setup
         * their Features and cloud-points. */
        foreach(Part p, parts) {
            PartItem * item = new PartItem(p);

            if(!item)
                return;

            item->setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
            scene()->addItem(item);

            m_parts.append(item);
            m_features += item->features();
            //qDebug() << "Number of items: " << scene()->items().size();

        }
    } else if(1 == mode) { /* range annotate */
        /**
         * @todo setup the depth map image here using
         * QGraphicsPixmapItem item. See at FrameViewer on how to use this as a background image.
         *
         */
        setRangeAnnotateForeground();

    }
    else {
        throw std::runtime_error("Invalid Mode");
    }

    /* finally make sure that the whole scene fits into the view */
    this->fitInView(scene()->sceneRect(), Qt::KeepAspectRatio);
}

void PartViewer::setRangeAnnotateForeground() {
    Image currentImg;
    if (NULL == frameViewer) {
        return;
    }


    qDebug() << "Parts: " << m_project.parts().size();
    qDebug() << "Geometries: " << m_project.geometries().size();


    /* create the background image consisting of the frame view */
    frameViewer->frame().getView(currentImg);
    QPixmap backgroundPixmap;
    backgroundPixmap.convertFromImage(QImage(currentImg.data(),
                                             currentImg.shape()[0],
                                             currentImg.shape()[1],
                                             QImage::Format_Indexed8));

    auto * backgroundPixmapItem = new QGraphicsPixmapItem();
    backgroundPixmapItem->setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
    backgroundPixmapItem->setPixmap(backgroundPixmap);
    scene()->addItem(backgroundPixmapItem);


    /* create the depth map as overlay */
    QPixmap foregroundPixmap;
    calculateRangeAnnotateForeground();
    foregroundPixmap.convertFromImage(QImage(rangeAnnotateForeground.data,
                                             currentImg.shape()[0], currentImg.shape()[1],
                                             QImage::Format_RGBA8888));

    auto * foregroundPixmapItem = new QGraphicsPixmapItem();
    foregroundPixmapItem->setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
    foregroundPixmapItem->setPixmap(foregroundPixmap);
    scene()->addItem(foregroundPixmapItem);




    scene()->setSceneRect(0,0,backgroundPixmap.width(), backgroundPixmap.height());

}

void PartViewer::setPose(const Vector6d &pose)
{
    currentPose = pose;
    currentPoseSet = true;
    changesSinceRA = true;
    calculateRangeAnnotateForeground();
    /**
     * @todo Add rendering routine here.
     * You can set up an open GL Context using
     * QOpenGLContext class.
     *
     * Use Rahuls spaghetti code here.
     * Finally store the visible landmarks as well as the generated pixmap.
     * Set the depthmap on the QGraphicsPixmapItem created in setMode().
     *
     */
    if (1 == currentMode) {
        setRangeAnnotateForeground();
    }

    qDebug() << "Setting pose....";

}


void PartViewer::wheelEvent(QWheelEvent *event)
{

	if(event->modifiers() & Qt::ControlModifier)
		return;

	/* store the current position in the scene on
	 * which the mouse points on */
	QPointF posInScene = this->mapToScene(event->pos());

	/* rescale the whole stuff */
	const qreal scale = 1.5;
	if(event->angleDelta().y() > 0) {
		this->scale(scale, scale);
	} else if(event->angleDelta().y() < 0) {
		this->scale(1.0/scale, 1.0/scale);
	}


	/* now make sure that the mouse points on the same point
	 * in scene again. */
	QPointF newPos = this->mapToScene(event->pos());
	QPointF delta = (newPos-posInScene);
	this->translate(delta.x(), delta.y());

	/* don't pass events further up */
	event->accept();
}

void PartViewer::keyPressEvent(QKeyEvent *event)
{

    if ((event->modifiers() & Qt::ControlModifier) && (event->modifiers() & Qt::ShiftModifier)
        && (event->key() == Qt::Key_Left || event->key() == Qt::Key_Right)) {
        vigra::MultiArray<2, float> kamera = OpenGLProjection::matrixFromVector(currentPose);
        std::cerr << "Old Pose: " << currentPose << std::endl;
        std::cerr << kamera << std::endl;
        kamera = vigra::inverse(kamera);
        std::cerr << "Inverted:" << std::endl;
        std::cerr << kamera << std::endl;
        Vector6d kameraVec = hookers::toolbox::PoseEstimation::vectorFromMatrix(kamera);
        std::cerr << "Kamera vector: " << kameraVec << std::endl;
        if (event->key() == Qt::Key_Left) {
            kameraVec.subarray<3,6>() *= 1.001;
        }
        if (event->key() == Qt::Key_Right) {
            kameraVec.subarray<3,6>() *= 0.999;
        }
        std::cerr << "new Kamera vector: " << kameraVec << std::endl;
        kamera = OpenGLProjection::matrixFromVector(kameraVec);
        std::cerr << kamera << std::endl;
        kamera = vigra::inverse(kamera);
        std::cerr << "Backinverted:" << std::endl;
        std::cerr << kamera << std::endl;

        currentPose = hookers::toolbox::PoseEstimation::vectorFromMatrix(kamera);
        std::cerr << "New Pose: " << currentPose << std::endl;
        this->setPose(currentPose);
        event->accept();
        return;
    }
	/* handle pressing ctrl-key (enter selection mode) */
	if(event->modifiers() & Qt::ControlModifier) {
		/* if we are in selection mode, we do not want to have any interaction with the scene
		 * that comes from the default implementation of QGraphicsVew */
		this->setInteractive(false);
		this->setDragMode(NoDrag);

		/* set a pointing cursor to indicate that we are in selection mode */
		this->setCursor(QCursor(Qt::PointingHandCursor));

		/* we have entered selection mode. make sure to remove any selection
		 * information from former times */
		if(m_selection) {
			this->clearSelection();

			delete m_selection;
			m_selection = 0;
		}
		event->accept();
	} else {
		QGraphicsView::keyPressEvent(event);
	}
}

void PartViewer::keyReleaseEvent(QKeyEvent *event)
{

	/* handle releae of ctrl-key (leaving selection mode) */
	if(!(event->modifiers() & Qt::ControlModifier)) {
		this->setCursor(QCursor());
		this->setInteractive(true);
		this->setDragMode(ScrollHandDrag);
		event->accept();
	} else {
		QGraphicsView::keyReleaseEvent(event);
	}
}

void PartViewer::rangeAnnotateClickEvent(QMouseEvent *event) {



    const QPoint clickedPos = QPoint(mapToScene(event->pos()).x(), mapToScene(event->pos()).y());
    std::cerr << "zBuffer Shape: " << zBuffer3D.shape() << std::endl;
    qDebug() << "Image was clicked at " << mapToScene(event->pos()) <<  " => " << clickedPos;
    qDebug() << "("  << zBuffer3D(0, clickedPos.x(), clickedPos.y())
             << ", " << zBuffer3D(1, clickedPos.x(), clickedPos.y())
             << ", " << zBuffer3D(2, clickedPos.x(), clickedPos.y())
             << ")";

    {
        using namespace vigra::multi_math;
        if (any(zBuffer3D.bindOuter(clickedPos.y()).bindOuter(clickedPos.x()) == std::numeric_limits<float>::infinity())) {
            emit userMessage("No point under cursor");
            return;
        }
    }

    if (currentPoseSet) {

        Vector3f clickedPoint;
        clickedPoint[0] = zBuffer3D(0, clickedPos.x(), clickedPos.y());
        clickedPoint[1] = zBuffer3D(1, clickedPos.x(), clickedPos.y());
        clickedPoint[2] = zBuffer3D(2, clickedPos.x(), clickedPos.y());
        std::cerr << clickedPoint << std::endl;

        LandmarkList candidates = frameViewer->frame().sequence().geometry().nearestNeighbourSearch(clickedPoint, 3);
        Landmark bestMatch = candidates.front();
        double bestCost = (bestMatch.pos() - clickedPoint).magnitude();
        for (Landmark worldPoint : candidates) {
            const double currentCost = (worldPoint.pos() - clickedPoint).magnitude();
            if (currentCost < bestCost) {
                bestCost = currentCost;
                bestMatch = worldPoint;
            }
        }

        /* if distance of found point and requested is larger than 0.1m, reject it */
        if (bestCost > 0.1) {
            qDebug() << "Fatal: Point not really found in the world. Choose a better one, kthxbye";
            emit userMessage("No matching point found in cloud.");
            return;
        }
        else {
            FeatureItem * newFeatureItem = new FeatureItem(bestMatch);
            scene()->addItem(newFeatureItem);
            newFeatureItem->setPos(clickedPos + QPointF(0.5,0.5));
        }
    }
}

void PartViewer::mousePressEvent(QMouseEvent *event)
{
    emit userMessage("");

    /* if no ctrl-key is pressed, than handle mouse input as
	 * usual */
	if(!(event->modifiers() & Qt::ControlModifier)) {

        /** @todo check here if we are in range annotate mode.
         *  if this is the case, then handle a click like so:
         *
         * If the click happend on the depth map image, (use QGraphicsView/PartViewe::itemAt() to
         * check this), on a valid depthmap entry (eg. no background) then add a new FeatureItem (on the heap)
         * into the scene.
         * Register it in the list m_features as well.
         *
         * Position the item (using QGraphicsItem::setPos()) onto the same position as the pixel.
         * Finally select the FeatureItem using it's own setSelected() (or so...)
         * method.
         *
         * A FeatureItem requieres an landmark instance. You have to somehow get in from
         * the spaghetti code.
         *
         * You should be done here. If there is a selected FeatureItem in the part viewer,
         * then the application logic will link the selected items.
         *
         * In any case, remove all FeatureItems that have been added before.
         */
        if (Qt::ShiftModifier & event->modifiers()) {
            if (1 == currentMode) {
                rangeAnnotateClickEvent(event);
            }
        }


		QGraphicsView::mousePressEvent(event);
		return;
	}

	/* else we are in selection mode */
	event->accept();

	/* if no current bulk-selection item is available,
	 * create a new one */
	if(!m_selection) {
		m_selection = new BulkSelectionItem();
		scene()->addItem(m_selection);
	}

	/* finally activate bulk selection mode */
	m_selection->setActiveTracking();
	m_selection->addPoint(mapToScene(event->pos()));



}

void PartViewer::mouseMoveEvent(QMouseEvent *event)
{
	/* if Ctrl-Key is pressed.. */
	if((event->modifiers() & Qt::ControlModifier)) {

		if(m_selection) {
			/* handle the bulk selection */
			m_selection->addPoint(mapToScene(event->pos()));
		}

		event->accept();
	} else {
		/* process normal events for dragging of items and panning the scene */
		QGraphicsView::mouseMoveEvent(event);
	}
}

void PartViewer::mouseReleaseEvent(QMouseEvent *event)
{



	/* handle mouse release events independant of modifier state, such that
	 * dragging can stop, even if ctrl-key is pressed */
	QGraphicsView::mouseReleaseEvent(event);

	if(event->button() == Qt::MiddleButton) {
		event->accept();

		PartItem * pI = 0;
		QList<QGraphicsItem*> items = this->items(event->pos());

		foreach(QGraphicsItem * i, items) {
			pI = qgraphicsitem_cast<PartItem*>(i);
			if(pI)
				break;
		}

		if(!pI)
			return;
	
		QTransform t ; t.scale(-1.0,1.0) ;
		pI->setTransform(pI->transform() * t) ;
	}
	
	/* if no control modifier is set, do nothing */
	if(!(event->modifiers() & Qt::ControlModifier))
		return;


	/* set bulk selection item to passive mode */
	if(m_selection) {
		m_selection->setPassiveTracking();
		m_selection->addPoint(mapToScene(event->pos()));
	}

}

void PartViewer::enterEvent(QEvent *event)
{

	/* make sure that everything is reset when mouse
	 * enters the widget */
	this->setCursor(QCursor());
	this->setDragMode(ScrollHandDrag);

	/* especially set the focus to the current widget */
	this->setFocus();

	event->accept();
}

void PartViewer::setFrameViewer(FrameViewer * _frameViewer) {
    this->frameViewer = _frameViewer;
}

void PartViewer::sampleFrame()
{

    Frame f= frameViewer->frame();

    if(!projection) {
        qDebug() << "PartViewer::sampleFrame(): No projection instance available. ";
        return;
    }

    try {
        GroundtruthSource gt(projection);
        gt.enablePoseEstimation(true);
        MapExporterSink me;
        me.setFrames(f.sequence().frames());

        FrameList frames = f.sequence().frames();

        me.setup(gt.provides());

        auto fit = std::find(frames.cbegin(), frames.cend(), f);

        if(fit == frames.cend())
            throw std::runtime_error("Did not find provided frame in frame list");

        int fn = fit-frames.cbegin();

        ExporterSource::SourceMap map = gt.sources(f);
        me.push(map, fn);


    } catch (std::exception & e) {
        qDebug() << "Sampling frames failed: " << e.what();
        emit userMessage("Sampling failed...");
    }

    emit userMessage("Sampling finished...");

}

/**
 * @brief setTransparencyValue Set the alpha channel value for a whole image.
 * @param[inout] img The image.
 * @param[in] value The new alpha value.
 */
void setAlphaValue(cv::Mat& img, const int value) {
    const int cols = img.cols;
    const int step = img.channels();
    const int rows = img.rows;
    const size_t SELECTED_CHANNEL_NUMBER = 3;
    for (int y = 0; y < rows; y++) {
        unsigned char* p_row = img.ptr(y) + SELECTED_CHANNEL_NUMBER; //gets pointer to the first byte to be changed in this row, SELECTED_CHANNEL_NUMBER is 3 for alpha
        unsigned char* row_end = p_row + cols*step;
        for(; p_row != row_end; p_row += step)
            *p_row = value;
    }
}

/**
 * @brief PartViewer::calculateRangeAnnotateForeground This
 */
void PartViewer::calculateRangeAnnotateForeground() {
    if (!changesSinceRA) {
        return;
    }
    changesSinceRA = false;
    //return;
    QTime myTimer, myCompleteTimer;
    myTimer.start();
    myCompleteTimer.start();

    // If the pose hasn't been set yet by setPose then we use the pose from the project for the projection.
    if (!currentPoseSet) {
        currentPose = frameViewer->frame().pose();
    }


    std::cerr << "Cloud shape: " << cloud.shape() << std::endl;

    vigra::MultiArray<2, int> idimg(vigra::Shape2(2560, 1080), -1);
    vigra::MultiArray<2, float> zBuffer(vigra::Shape2(2560, 1080), 0.0);

    std::cerr << "Triangles shape: " << triangles.shape() << std::endl;
    std::cerr << "Cloud shape: " << cloud.shape() << std::endl;

    vigra::MultiArray<2, float> Rt(4, 4);
    zBuffer3D = vigra::MultiArray<3, float> (vigra::Shape3(3, 2560, 1080), std::numeric_limits<float>::infinity());

    getProjector();
    QTime softwareProjectTimer;
    softwareProjectTimer.start();
    projection->setPointSize(2);
    projection->setPose(currentPose);
    projection->getDepth(zBuffer);
    projection->getPositions(zBuffer3D);
    projection->setPointSize(1);


    qDebug() << "Software projection takes " << softwareProjectTimer.elapsed();

    qDebug() << "zBuffer3D norm: " << zBuffer3D.norm();
    qDebug() << "inner norm(2) " << zBuffer3D.bindInner(2).norm();


    cv::Mat zBufferMat = cv::Mat(1080, 2560, CV_32FC1, zBuffer.data()).clone();
    zBufferMat.setTo(0.0, zBufferMat == std::numeric_limits<float>::infinity());


    cv::Mat coloredZBuffer;
    colorMap(zBufferMat, coloredZBuffer);
    cvtColor(coloredZBuffer, coloredZBuffer, CV_BGR2RGBA);
    setAlphaValue(coloredZBuffer, 127);
    cv::Scalar whiteTransparent(255,255,255,0);
    rangeAnnotateForeground = cv::Mat(coloredZBuffer.rows, coloredZBuffer.cols, CV_8UC4, cv::Scalar(255,255,255,0));
    coloredZBuffer.copyTo(rangeAnnotateForeground, zBufferMat);


    //cv::namedWindow("colormap");
    //cv::imshow("colormap", rangeAnnotateForeground);




    //cv::waitKey();
    qDebug() << "zBuffer norm: " << zBuffer.norm();
    qDebug() << "Setting up stuff took " << myTimer.elapsed() << "ms";
//    myTimer.restart();
//    const int zBufferSize = zBuffer.size();
//    for (int ii = 0; ii < zBufferSize; ++ii)
//    {
//        zBuffer[ii] = zBuffer[ii] == 0 ? std::numeric_limits<double>::max() : zBuffer[ii];
//    }
//    qDebug() << "Checking the zBuffer took " << myTimer.elapsed() << "ms";
    std::cerr << "Intrinsics: " << intrinsics << std::endl;
    std::cerr << "currentPose: " << currentPose << std::endl;

    /*
    myTimer.restart();
    RunningStats stat_x, stat_y, stat_hit_x, stat_hit_y;
    for (int ii = 0; ii < cloud.shape(1); ++ii)
    {
        Vector3d    wp(cloud(0, ii), cloud(1, ii), cloud(2, ii));
        double      res[3];
        ReprojectionError::reproject(wp.data(), intrinsics.data(), currentPose.data(), res);
        int px = res[0], py = res[1];
        stat_x.push(px);
        stat_y.push(py);
        if (px < 0 || px >= 2560 || py < 0 || py>= 1080|| res[2] <=0)
            continue;
        stat_hit_x.push(px);
        stat_hit_y.push(py);
        if (zBuffer(px, py)> res[2])
        {
            idimg(px, py) = ii;
            zBuffer(px, py) = res[2];
        }
    }
    qDebug() << "Calculating ReprojectionError took " << myTimer.elapsed() << "ms";
    std::cerr << "px was " << stat_x.print() << std::endl;
    std::cerr << "py was " << stat_y.print() << std::endl;
    std::cerr << "hit px was " << stat_hit_x.print() << std::endl;
    std::cerr << "hit py was " << stat_hit_y.print() << std::endl;


    qDebug() << "zBuffer norm: " << zBuffer.norm();
    // */
    //zBuffer /= 250;
    //cv::namedWindow("lalalal", CV_WINDOW_NORMAL);
    //cv::imshow("lalalal", cv::Mat(1080, 2560, CV_64FC1, zBuffer.data()));
    //cv::waitKey();
    /*
    std::vector<int> cloudids;
    for (int ii = 0; ii < idimg.size(); ++ii)
    {
        if (idimg[ii] >= 0)
            cloudids.push_back(idimg[ii]);
    }
    vigra::MultiArray<2, float> cloud_red;
    cloud_red.reshape(vigra::Shape2(3, cloudids.size()));
    int as = 0;
    for (auto c : cloudids)
    {
        vigra::columnVector(cloud_red, as) = vigra::columnVector(cloud, c);
        ++as;
    }
    qDebug() << "zBuffer norm: " << zBuffer.norm();
    */
    qDebug() << "Scene rect: " << scene()->sceneRect();
    qDebug() << "calculateRangeAnnotateForeground took " << myCompleteTimer.elapsed() << "ms";
}


void PartViewer::getProjector() {

    getCloud();
    if (NULL != projection) {
        return;
    }
    // Get the intrinsic camera parameters from the current frame.
    intrinsics = frameViewer->frame().intrinsics();
    try {
        projection = new OpenGLProjection(cloud, intrinsics);
    } catch (std::runtime_error & e) {
        std::cout << "GUI: Getting Instance of OpenGLProjection failed: " << e.what() << std::endl;
    }

    if(projection == nullptr) {
        projection = new SoftwareProjection(cloud, intrinsics);
    }

}

void PartViewer::updatePartContent()
{
    std::cerr << "Updating part content" << std::endl;
    try {


        intrinsics = frameViewer->frame().intrinsics();
        const PartList parts = m_project.parts();

        /* for each part, add a part item.
         * PartItems are responsible to setup
         * their Features and cloud-points. */
        foreach(Part p, parts) {
            //auto camerMatrix = hookers::toolbox::PoseEstimation
            //PartItem * item = new Part3DItem();

            for (Landmark lm : p.landmarks()) {
                auto landmarkPose = hookers::toolbox::PoseEstimation::reproject3d(lm.pos(), currentPose, Vector4d(intrinsics));
                const int t_x = (int) landmarkPose[0];
                const int t_y = (int) landmarkPose[1];
                if (
                        t_x > 0 && t_x < 2560
                        && t_y > 0 && t_y < 1080) {
                    if (landmarkPose[2] < zBuffer3D(2, t_x, t_y) + .1) {
                        FeatureItem * landmarkItem = new FeatureItem(lm);
                        landmarkItem->setPos(t_x + 0.5, t_y + 0.5);
                        landmarkItem->setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
                        landmarkItem->setScale(5);
                        scene()->addItem(landmarkItem);
                    }
                    else {
                    }
                }
                else {
                }
            }
        }
        std::cerr << "Intrinsics: " << intrinsics << std::endl;
    }
    catch(...) {
        std::cerr << "Exception in updatePartContent" << std::endl;
    }
}



void PartViewer::getTriangles(const char* filename) {
    if (gotTriangles) {
        return;
    }
    vigra::HDF5File trianglefile(filename, vigra::HDF5File::Open);
    try { // Let's see if we can open the triangles.h5 file and read the triangles.
        trianglefile.readAndResize("/triangles", triangles);
        gotTriangles = true;
    }
    catch (std::exception& e) { // If we can't read the triangles, we just give up.
        qDebug() << "Reading triangles.h5 failed:";
        qDebug() << e.what();
        qDebug() << "Aborting calculateRangeAnnotateForeground";
        return;
    }
    catch (...) { // This should never happen.
        qDebug() << "Reading triangles.h5 failed with a nonstandard exception";
        qDebug() << "Aborting calculateRangeAnnotateForeground";
        return;
    }
}

void PartViewer::getCloud() {
    if (!gotCloud) {
        if(frameViewer->frame().sequence().geometry().isValid()) {
            frameViewer->frame().sequence().geometry().getPointCloud(cloud);
            gotCloud = true;
        } else {
            std::cout << "No valid geometry" << std::endl;
        }
    }
}
