#include <memory>

#include <vector>

#include <sstream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <array>

#include <tclap/CmdLine.h>
#include <tclap/ZshCompletionOutput.h>

#include <vigra/multi_array.hxx>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>


using namespace hookers;
using namespace hookers::interface;

typedef cv::Mat LookupTable;

class WrongCommandException : public std::runtime_error
{
public:
    WrongCommandException(const std::string & module,
                          const std::string & command)
        : std::runtime_error("Command '" + command + "' not supported for module '" + module + "'")
    {}
};

class WrongCommandArgumentException : public std::runtime_error
{
public:
    WrongCommandArgumentException(const std::string & command)
        : std::runtime_error("Arguments provided for command '" + command + "' are not valid.")
    {}
    WrongCommandArgumentException(const std::string & command, const std::string & why)
        : std::runtime_error("Arguments provided for command '" + command + "' are not valid: " + why)
    {}

};



/**
 * @brief imports landmarks from a comma seperated value file.
 * @arg part the part to which landmarks should be added
 * @arg file opened file
 */
std::vector<Vector3f> importPartCsv(std::ifstream & file)
{
    std::vector<Vector3f> positions;

    for( std::string line; getline( file, line ); )
    {
        std::istringstream ss(line);

        Vector3f pos;
        ss >> pos[0];
        ss.ignore(1);
        ss >> pos[1];
        ss.ignore(1);
        ss >> pos[2];

        //std::cout << pos[0] << "," << pos[1] << "," << pos[2] << std::endl;

        positions.push_back(pos);
    }

    return positions;

}


void importPly(Vector3fArray & arr, std::ifstream & stream) {

        std::string line;
        std::getline(stream, line);

        if(line != "ply")
            throw std::runtime_error("PLY magic not found.");

        int VERTEX_COUNT = -1;
        std::vector<std::string> VERTEX_FORMAT;

        VERTEX_FORMAT.push_back("x");
        VERTEX_FORMAT.push_back("y");
        VERTEX_FORMAT.push_back("z");
//        VERTEX_FORMAT.push_back("nx");
//        VERTEX_FORMAT.push_back("ny");
//        VERTEX_FORMAT.push_back("nz");

        while(!stream.eof()) {
            std::getline(stream, line);

            if(line == "end_header")
                break;

            if(line.find("element vertex") != std::string::npos) {
                line.replace(0, 14, "");
                std::stringstream ss(line);

                int vertexCount = -1;
                ss >> vertexCount;

                if(vertexCount == -1)
                    throw std::runtime_error("vertex count couldn't be read.");

                VERTEX_COUNT = vertexCount;
            }
        }

        /* we are here, because the last header line has been read. now we have to
         * go on and read exactly the number of vertices */

        std::cout << "PLY: reading vertices: " << VERTEX_COUNT << std::endl;

        Vector3fArray::difference_type shape;
        shape[0] = VERTEX_COUNT;
        arr.reshape(shape);



        std::cout << "The following four lines show the two first and the two last read points:"
                  << std::endl << "---" << std::endl;

        float * entry = new float[VERTEX_FORMAT.size()];
        for(int i = 0; i < VERTEX_COUNT; i++) {
            stream.read((char*)entry, sizeof(float)*VERTEX_FORMAT.size());
            if(i < 2 || (i < VERTEX_COUNT && i > VERTEX_COUNT-3))
            std::cout << "Reading point: "
                      << entry[0] << ","
                      << entry[1] << ","
                      << entry[2] << std::endl;
            arr(i) = Vector3f(entry[0], entry[1], entry[2]);
        }
        delete[] entry;
        std::cout << "---" << std::endl;
}



int main(int argn, char ** argv) {



    //TCLAP::ZshCompletionOutput zshOutput;
    TCLAP::CmdLine cmd("hookers project browser", ' ', "0.1");
    //cmd.setOutput(&zshOutput);

    TCLAP::ValueArg<std::string>
        projectArg(std::string(),
                     "project",
                     "project name (eg. 20130101_0000)",
                     false,
                     std::string(),
                     "string",
                     cmd);

    TCLAP::ValueArg<std::string>
        plyArg(std::string(),
                     "ply",
                     "Url to a ply file. Must be in the format binary LE with properties x,y,z only.",
                     false,
                     std::string(),
                     "path",
                     cmd);

    TCLAP::ValueArg<std::string>
        csvArg(std::string(),
                     "csv",
                     "path to a csv file. ",
                     false,
                     std::string(),
                     "path",
                     cmd);
    TCLAP::ValueArg<std::string>
        geometryArg("g",
               "geometry",
               "name of the geometry object",
               false,
               std::string(),
               "string",
               cmd);

    TCLAP::ValueArg<double>
        pointToleranceArg(std::string(),
               "point-tolerance",
               "tolerance in m in which operations on points are tolerated",
               false,
               0.01,
               "float",
               cmd);




    TCLAP::SwitchArg updateArg(std::string(),
                               "update",
                                "specifies whether this command should update values.",
                                cmd, false);


    std::vector<std::string> allowedModules;
    allowedModules.push_back("project");
    allowedModules.push_back("sequence");
    allowedModules.push_back("part");
    allowedModules.push_back("geometry");

    TCLAP::ValuesConstraint<std::string> allowedModulesContraint(allowedModules);

    TCLAP::UnlabeledValueArg<std::string>
            moduleArg("object",
                      "type of object",
                      true,
                      "sequence",
                      &allowedModulesContraint,
                      cmd);

    TCLAP::UnlabeledValueArg<std::string>
            commandArg("command",
                      "the action that should be taken for selected object type. Use 'help'' for further reference.",
                      true,
                      "list",
                      "list|add|remove|...|help",
                      cmd);

    TCLAP::UnlabeledMultiArg<std::string>
            valueArg("values",
                     "arguments to command, like sequence name etc... Use 'help' for further reference.",
                     false,
                     "string",
                     cmd);


    try {
        cmd.parse(argn, argv);

    } catch (TCLAP::ArgException &e)  {
        std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
        return -1;
    }


    try {


        if(moduleArg.getValue() == "project") {
            if(commandArg.getValue() == "add") {

                std::vector<std::string> args = valueArg.getValue();
                if(args.size() != 1)
                    throw WrongCommandArgumentException(commandArg.getValue(),
                                                        "too many arguments given.");

                /* create the project */
                Project::create(args[0]);

            } else {
                throw WrongCommandException(moduleArg.getValue(), commandArg.getValue());
            }
        }

        if(moduleArg.getValue() == "sequence") {

            Project project = Project::open(projectArg.getValue());
            if(!project.isValid()) {
                std::cout << "[ERROR] Project could not be opened." << std::endl;
                return -1;
            }

            if(commandArg.getValue() == "list") {
                SequenceList sequences = project.sequences();
                std::cout << "Sequences registered with project: " << std::endl
                          << "----------------------------------" << std::endl;
                for(auto s: sequences) {
                    std::cout << s.name() << "\t\t"<< s.id().toString() << std::endl;
                }
                std::cout << std::endl;
            } else if(commandArg.getValue() == "add") {
                std::vector<std::string> args = valueArg.getValue();
                if(args.size() != 1)
                    throw WrongCommandArgumentException(commandArg.getValue(),
                                                        "too many arguments given.");

                /* add sequence to project */
                project.addSequence(valueArg.getValue()[0]);

            } else {
                throw WrongCommandException(moduleArg.getValue(), commandArg.getValue());
            }
        }

        if(moduleArg.getValue() == "geometry") {
            Project project = Project::open(projectArg.getValue());

            if(!project.isValid()) {
                throw std::runtime_error("Could not open project '" + projectArg.getValue() + "'");
            }

            if(commandArg.getValue() == "add") {
                std::vector<std::string> args = valueArg.getValue();
                if(args.size() != 1) {
                    throw WrongCommandArgumentException(commandArg.getValue(),
                                                        "too many arguments given.");
                }
                project.addGeometry(args.front());
            } else if(commandArg.getValue() == "import" || commandArg.getValue() == "delete-points") {

                if(valueArg.getValue().size() != 1) {
                    throw WrongCommandArgumentException("geometry import", "You must give the geometry name.");
                }

                if(!csvArg.isSet() && !plyArg.isSet())
                    throw WrongCommandArgumentException("geometry import", "You must supply either a csv (--csv) or ply (--ply) file.");

                if(csvArg.isSet() && plyArg.isSet())
                    throw WrongCommandArgumentException("geometry import", "you must set --ply or --csv exclusively.");

                Geometry geometry = project.geometries().byLabel(valueArg.getValue()[0]);

                if(!geometry.isValid()) {
                    throw std::runtime_error("Could not get geometry '" + valueArg.getValue()[0] + "'.");
                }

                std::ifstream stream;
                Vector3fArray pos;

                if(plyArg.isSet()) {
                    stream.open(plyArg.getValue(), std::ifstream::binary);
                }

                importPly(pos, stream);

                std::cout << "opened ply file and read "
                          << pos.size() << " entries." << std::endl;

                if(commandArg.getValue() == "import") {
                    geometry.addLandmarks(pos);
                }

                if(commandArg.getValue() == "delete-points") {

                    /* go through all points we want to remove */
                    std::vector<Vector3f> targets;
                    targets.resize(pos.size());

                    for(auto point: pos) {
                        targets.push_back(point);
                    }

                    std::vector<LandmarkList> llist = geometry.nearestNeighbourSearches(targets);
                    LandmarkList filtered;


                    for(int i = 0; i < llist.size(); i++) {

                        auto & ll = llist[i];
                        /* if list is empty, there is nothing todo */
                        if(ll.empty()) {
                            continue;
                        }

                        Landmark l = ll.front();
                        if(!l.isValid()) {
                            continue;
                        }

                        /* Check if the point doesn't deviate more than the provided point tolerance.
                         * In this case we can consider the KNN to be certainly wrong. */
                        if(vigra::abs(targets[i]-l.pos()).maximum() > pointToleranceArg.getValue()) {
                            continue;
                        }

                        filtered.push_back(l);
                    }

                    std::cout << "starting to remove landmarks" << std::endl;
                    geometry.removeLandmarks(filtered);
                }

            } else if(commandArg.getValue() == "list") {
                GeometryList geometries = project.geometries();
                std::cout << "Geometries registered with project:" << std::endl
                          << "-----------------------------------" << std::endl;
                for(auto g: geometries) {
                    std::cout << g.name() << "\t\t"<< std::endl;
                }
            } else {
                throw WrongCommandException(moduleArg.getValue(), commandArg.getValue());
            }


        }

        if(moduleArg.getValue() == "part") {

            Project project = Project::open(projectArg.getValue());

            Geometry geometry = project.geometries().byLabel(geometryArg.getValue());
            if(!geometry.isValid()) {
                throw std::runtime_error("Could not get geometry '" + valueArg.getValue()[0] + "'.");
            }

            if(!project.isValid()) {
                throw std::runtime_error("Could not open project '" + projectArg.getValue() + "'");
            }


            if(commandArg.getValue() == "list") {
                PartList parts = geometry.parts();
                std::cout << "Parts registered with project: " << std::endl
                          << "------------------------------" << std::endl;
                for(Part p: parts) {
                    std::cout << p.name() << "\t\t"<< p.landmarks().size() << std::endl;
                }
            } else if(commandArg.getValue() == "add") {
                if(valueArg.getValue().size() != 1) {
                    throw WrongCommandArgumentException("part add", "must exactly provide only the part name as argument");
                }

                geometry.addPart(valueArg.getValue()[0]);

            } else if(commandArg.getValue() == "import") {

                if(valueArg.getValue().size() != 1) {
                    throw WrongCommandArgumentException("part import", "You must give the part name.");
                }

                if(!csvArg.isSet() && !plyArg.isSet())
                    throw WrongCommandArgumentException("part import", "You must supply either a csv (--csv) or ply (--ply) file.");

                if(csvArg.isSet() && plyArg.isSet())
                    throw WrongCommandArgumentException("part import", "you must set --ply or --csv exclusively.");

                Part part = geometry.parts().byLabel(valueArg.getValue()[0]);

                if(!part.isValid()) {
                    throw std::runtime_error("Could not get a ref to part '" + valueArg.getValue()[0] + "'.");
                }

                if(!part.landmarks().empty() > 0 && !updateArg.isSet()) {
                    throw std::runtime_error("Part '" + valueArg.getValue()[0] + "' already has landmarks. You might set --update switch in order to update landmarks.");
                }

                if(!part.landmarks().empty()) {
                    throw std::runtime_error("Updating parts is currently not supported.");
                }


                std::vector<Vector3f> positions;

                if(csvArg.isSet()) {
                    std::ifstream input;
                    input.open(csvArg.getValue());

                    if(!input.is_open()) {
                        throw std::runtime_error("Couldnt open csv input file at " + csvArg.getValue());
                    }

                    positions = importPartCsv(input);
                }

                if(positions.empty()) {
                    throw std::runtime_error("Could not read any landmarks.");
                }

                std::vector<LandmarkList> knns = geometry.nearestNeighbourSearches(positions, 1);
                for(auto knnList: knns ) {
                    for(auto knn: knnList) {
                        part.addLandmark(knn);
                    }
                }

            } else {
                throw WrongCommandException(moduleArg.getValue(), commandArg.getValue());
            }
        }



    } catch (std::runtime_error & e) {
        std::cout << "[ERROR] : " << e.what() << std::endl;
    }
}
