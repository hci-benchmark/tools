#include <tclap/CmdLine.h>

#include <bitset>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>

#include <hookers/toolbox/Annotation/Annotator.h>
#include <hookers/toolbox/PoseEstimation/PoseEstimation.h>

#include "tracker.h"

using namespace hookers;
using namespace hookers::interface;
using namespace hookers::toolbox;

int main(int argc, char ** argv) {

    TCLAP::CmdLine cmd("boschgt scene registration", ' ', "0.1");

    TCLAP::ValueArg<std::string>
        sequenceNameArg("s", "sequence",
                        "sequence to analyze (eg. 20130101_0000)",
                        true,
                        std::string(),
                        "string",
                        cmd);

    TCLAP::ValueArg<std::string>
        referenceNameArg(std::string(),
                     "reference",
                     "reference sequence name (eg. 20130101_0000)",
                     false,
                     std::string(),
                     "string",
                     cmd);


    TCLAP::ValueArg<int>
        startArg(std::string(), "start",
                        "starting frame. default ist first frame in sequence",
                        false,
                        0,
                        "int",
                        cmd);

    TCLAP::ValueArg<std::string>
        viewsArg(std::string(),
                     "views",
                     "the views to track.",
                     false,
                     "base",
                     "base|stereo|all",
                     cmd);


    TCLAP::ValueArg<int>
        endArg(std::string(), "end",
                        "ending frame. Default is last frame in sequence.",
                        false,
                        -1,
                        "int",
                        cmd);

    TCLAP::UnlabeledValueArg<std::string> moduleArg(
                "module",
                "the module to use",
                true,
                std::string(),
                "track|annotate|register",
                cmd
                );

    TCLAP::SwitchArg dryRunArg(std::string(),
                                            "dry-run",
                                            "does only dry run on sequence",
                                            cmd);


    TCLAP::SwitchArg trackerEqualizeHistArg(std::string(),
                                            "equalize-hist",
                                            "enables histogram equalization for tracking process",
                                            cmd);


    try {
        cmd.parse(argc, argv);
    } catch (std::exception & e) {
        std::cerr << "Command line parser: " << e.what();

    }

    enum ModuleFlags {
        NoModule = 0,
        TrackerModule = 1,
        AnnotationModule = 2,
        RegistrationModule = 4
    };

    enum ViewsFlags {
        Base = 1,
        Stereo = 2,
        Both = 4
    };

    int modules = ModuleFlags::NoModule;
    if(moduleArg.getValue() == "track")
        modules |= TrackerModule;
    if(moduleArg.getValue() == "register")
        modules |= RegistrationModule;
    if(moduleArg.getValue() == "annotate")
        modules |= AnnotationModule;


    int views = ViewsFlags::Base;
    if(viewsArg.getValue() == "stereo") {
        views = ViewsFlags::Stereo;
    }
    if(viewsArg.getValue() == "all") {
        views = ViewsFlags::Base | ViewsFlags::Stereo;
    }



    std::cout << "Lalalala" << std::bitset<32>(modules) << std::endl
              << TrackerModule << "," << AnnotationModule << "," << RegistrationModule << std::endl;




    try {
        Project project = Project::open();
        if(!project.isValid()) {
            throw std::runtime_error("Could not open project.");
        }


        Sequence sequence = project.sequences().byLabel(sequenceNameArg.getValue());
        if(!sequence.isValid()) {
            throw std::runtime_error("Could not open sequence ("+sequenceNameArg.getValue()+").");
        }

        FrameList frames = sequence.frames();


        const int startFrame = startArg.getValue();
        {
            if(startFrame < 0 || startFrame >= (int)frames.size()) {
                std::stringstream ss;
                ss << "Starting frame is not in range [0, " << frames.size() << "]";
                throw std::runtime_error(ss.str());
            }
        }

        const int endFrame = endArg.getValue();
        {
            if(endFrame == -1)
                const_cast<int&>(endFrame) = frames.size()-1;
            if(endFrame < startFrame || endFrame >= (int)frames.size()) {
                std::stringstream ss;
                ss << "End frame is not in range [" << startFrame << ", " << frames.size()-1 << "]";
                throw std::runtime_error(ss.str());
            }
        }


        std::cout << "[STATUS] Registering sequence for [" << startFrame << ", " << endFrame << "]"
                  << std::endl;


        if(modules & ModuleFlags::TrackerModule) {

            std::cout << "[STATUS] Started tracking..." << std::endl;

            std::vector<FeatureTracker> trackers;

            if(views & ViewsFlags::Base) {
                trackers.emplace_back(FeatureTracker(Frame::View::Base));
            }

            if(views & ViewsFlags::Stereo) {
                trackers.emplace_back(FeatureTracker(Frame::View::Stereo));
            }

            for(auto & tracker: trackers) {
                tracker.setHistogramEqualization(trackerEqualizeHistArg.getValue());
                tracker.setDryRun(dryRunArg.getValue());
            }
            FrameList frames = sequence.frames();

            /* endFrame is inclusive */
            for(int i = startArg.getValue(); i <= endFrame; i++) {
                std::cout << "Tracking for frame:" << i << std::endl;
                Frame f = frames[i];
                for(auto & tracker: trackers) {
                    tracker.track(f);
                }
            }
        }

        if(modules & ModuleFlags::AnnotationModule) {
            std::cout << "[STATUS] Started annotation..." << std::endl;
            AutoAnnotator::auto_annotate(argc, argv);
        }

        if(modules & ModuleFlags::RegistrationModule) {

            std::cout << "[STATUS] Started registration..." << std::endl;

            /* endFrame is inclusive */
            for(int i = startArg.getValue(); i <= endFrame; i++) {
                Frame f = frames[i];
                TrackList annotTracks = f.tracks().annotated();

                try {

                    LandmarkList annotLandmarks = annotTracks.landmarks();

                    /* check if we have enough points to do the solvePNP stuff */
                    if(annotLandmarks.size() < 5 || annotTracks.size() < 5) {
                        throw std::runtime_error("Found no not enough correspondencies.");
                    }

                    Vector2fArray trackPositions;
                    Vector3fArray landmarkPositions;

                    annotTracks.getPositionsInFrame(trackPositions, f);
                    annotLandmarks.getPositions(landmarkPositions);



                    Vector6d pose;
                    try {
                        pose = hookers::toolbox::PoseEstimation::solvePnPRansac(landmarkPositions, trackPositions,
                                                                        f.intrinsics(), 10.0, 1);
                    } catch (std::exception & e) {
                        std::stringstream err;
                        err << "[INFO] Error at solvePnP for frame: " << e.what();
                        throw;
                    }

                    std::cout << " --- camera Pose:" << pose << std::endl;

                    if(dryRunArg.getValue())
                        continue;

                    f.setPose(pose);


                } catch (std::runtime_error & e) {
                    std::cout << "[WARNING] Could not find camera pose: " << e.what() << std::endl;
                    continue;
                }



            }

        }

    } catch(std::runtime_error & e) {
        std::cerr << "[ERROR] " << e.what() << std::endl;
        return -1;
    }


}
