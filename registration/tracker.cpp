#include <thread>
#include <mutex>

#include <vigra/timing.hxx>

#include "tracker.h"



#include <opencv2/video/video.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/flann/flann.hpp>
#include <opencv2/video/tracking.hpp>

using namespace hookers;
using namespace hookers::interface;

template<typename K,typename V>
AbstractObjectList<K> map_keys(const std::unordered_map<K,V> & map) {
    AbstractObjectList<K> objs;
    for(auto it = map.begin(); it != map.end(); it++)
        objs.push_back((*it).first);
    return objs;
}

template<typename K,typename V>
AbstractObjectList<V> map_values(const std::unordered_map<K,V> & map) {
    AbstractObjectList<V> objs;
    for(auto it = map.begin(); it != map.end(); it++)
        objs.push_back((*it).second);
    return objs;
}



FeatureTracker::FeatureTracker(const Frame::View & view)
    : m_id(1),
      m_patchSize(21, 21),
      m_searchWindowSize(31,31),
      m_histEqualize(false),
      m_dryRun(false),
      m_view(view)
{

}


FeatureTrackList FeatureTracker::trackFrame(Frame &f) {


        USETICTOC;
        TIC;

        /* get frames in scene and start tracking */
        Image vigraView;
        f.getView(vigraView,m_view);

        if(!vigraView.hasData())
            throw std::runtime_error("FeatureTracker::track(): No Frame data available");

        viewRaw = cv::Mat(vigraView.height(), vigraView.width(), CV_8UC1, vigraView.data());

        const cv::Size IMAGE_SIZE(vigraView.width(), vigraView.height());

        const cv::Size CORRELATION_PATCH_SIZE = m_patchSize;
        const cv::Size CORRELATION_SEARCH_WINDOW_SIZE = m_searchWindowSize;


        const cv::Rect ROI_TRACK_SEARCH(CORRELATION_PATCH_SIZE.width,
                                        CORRELATION_PATCH_SIZE.height,
                                        IMAGE_SIZE.width-2*CORRELATION_SEARCH_WINDOW_SIZE.width,
                                        IMAGE_SIZE.height-2*CORRELATION_SEARCH_WINDOW_SIZE.height);
        const cv::Rect ROI_FEATURE_SEARCH = ROI_TRACK_SEARCH;

        const int MAX_FEATURE_COUNT = 5000;

        const int MIN_FRAME_COUNT = 15;

        /* number of frames over that the flow should be averaged */
        const int FLOW_N = 10;

        /* sigma of search window for track points */
        const double CONFIDENCE_SIGMA = 2.0;

        /* confidence area for flow */
        const double CONFIDENCE_FLOW_SIGMA = 3.0;

        /* total confidence should not be less than 50% */
        const double MIN_CONFIDENCE = 0.5;

        /* minimal visual confidence. if the value drops below this value,
         * then the track will immidiatly removed */
        const double MIN_VISUAL_CONFIDENCE = 0.75;


        /* this map counts for each pixel how many features have been found */
        cv::Mat featureCount(IMAGE_SIZE, CV_64FC1);
        featureCount = 0.0;

        cv::Mat trackCount(IMAGE_SIZE, CV_64FC1);
        trackCount = 0.0;

        /* maskes positions where we don't want to search for feature points */
        cv::Mat featureSearchMask(IMAGE_SIZE, CV_8UC1);

        /* We are keeping track of regions where a track is existing */
        if(trackMask.size() != IMAGE_SIZE)
            trackMask = cv::Mat(IMAGE_SIZE, CV_8UC1);


        if(m_histEqualize) {
            cv::equalizeHist(viewRaw, viewRaw);
            cv::GaussianBlur(viewRaw, view, cv::Size(3,3), 1.0);
        } else {
            viewRaw.copyTo(view);
        }





        /* reset feature mask */
        featureSearchMask = 0.0;
        cv::Mat(featureSearchMask, ROI_FEATURE_SEARCH).setTo(255);



        /* this set will be used by following block matcher
         * to mark FeatureTracks that should be deleted.
         * We need this intermediate thing because we use threads to process
         * the FeatureTracks.
         */
        FeatureTrackSet disposeableTracks;
        std::mutex disposeableTrackLock;


        cv::Mat workingView = view;



//        /* first only search features that have been assigned to tracks */
        std::vector< cv::Point2f > trackCorners;
//        if(m_featureTracks.size()) {
//            cv::goodFeaturesToTrack(workingView, trackCorners,
//                                    std::min(m_featureTracks.size(), size_t(MAX_FEATURE_COUNT)),
//                                    0.002,
//                                    3*CONFIDENCE_SIGMA,
//                                    featureSearchMask & trackMask , 3, false);
//        }




        /* Find feature that are trackable.
         * The selected parameters are pretty greedy */
        std::vector< cv::Point2f > corners;
        if(trackCorners.size() < MAX_FEATURE_COUNT)
            cv::goodFeaturesToTrack(workingView, corners,
                                    std::max(MAX_FEATURE_COUNT-trackCorners.size(), size_t(0)),
                                    0.002,
                                    3*CONFIDENCE_SIGMA,
                                    featureSearchMask /*& (~trackMask)*/, 3, false);

        trackMask.setTo(0);

        /* merge both lists */
        corners.insert(corners.end(), trackCorners.begin(), trackCorners.end());


        /* counts the number of features on a given point */
        featureCount = 0.0;
        for(auto & p : corners) {
            featureCount.at<double>(p) += 1.0;
        }


        std::vector<cv::Point2f> trackPoints;
        /* set up track point list */
        for(auto & ft: m_featureTracks) {
            trackPoints.push_back(ft.center());
        }


        const float globalFeatureRadius = std::sqrt(CORRELATION_SEARCH_WINDOW_SIZE.area())/2.0;

//        cv::BFMatcher matcher;
        cv::FlannBasedMatcher matcher;
        const std::vector<cv::Point2f> & selectionPool = corners;
        {
            std::vector<cv::Mat> trainSet;
            trainSet.push_back(cv::Mat(selectionPool).reshape(1));
            matcher.add(trainSet);
            matcher.train();
        }


        cv::Mat kernelXY;
        {

            cv::Mat kernelX = cv::getGaussianKernel(CORRELATION_PATCH_SIZE.width, CONFIDENCE_SIGMA);
            cv::Mat kernelY = cv::getGaussianKernel(CORRELATION_PATCH_SIZE.height, CONFIDENCE_SIGMA);
            kernelXY = kernelX * kernelY.t();

            /* find maximum */
            double min, max;
            cv::minMaxLoc(kernelXY, &min, &max);

            /* such that the center of the gaussian corresponds to 1.0 */
            if(max > 0.0)
                kernelXY /= max;
        }

        /* count the features in track patch, weighted by a 2-sigma gaussian */
        static const cv::Mat countMask = kernelXY;

        auto matcherFunction = [&](int offset, int inc) {



            /* the following instances are held in memory to avoid reallocation all the time */

            /* keep it outside of loop to avoid memory reallocation */
            std::vector<cv::Point2f> potentialPoints;

            /* feature search centrum */
            cv::Mat_<float> radiusSearchPosMat(1, 2, CV_32FC1);


            cv::Mat matchTemplateOutputFeature;
            cv::Mat matchTemplateOutputEstimation;


            for(size_t i = offset;  i < m_featureTracks.size(); i += inc) {

                FeatureTrack & t = m_featureTracks[i];

                try {


                    cv::Point2f radiusSearchPos = t.center();
                    double radiusSearchRadius = globalFeatureRadius;

                    if(t.frames >= FLOW_N) {
                        radiusSearchPos += t.flow;
                        radiusSearchRadius = CONFIDENCE_FLOW_SIGMA/t.features*2.0;
                    }

                    std::vector<std::vector<cv::DMatch> > matches;
                    {
                        radiusSearchPosMat(0) = radiusSearchPos.x;
                        radiusSearchPosMat(1) = radiusSearchPos.y;
                        matcher.radiusMatch(radiusSearchPosMat, matches, radiusSearchRadius);
                    }


                    const std::vector<cv::DMatch> & nns = matches[0];

                    /* get all possible points from neighbour search */
                    potentialPoints.clear();
                    for(size_t j = 0; j < nns.size(); j++) {
                        const cv::DMatch & m = nns[j];
                        potentialPoints.push_back(selectionPool[m.trainIdx]);
                    }



                    /* calculate the position estimate from template matching */
                    cv::Point2f templatePos;
                    {

                        /* setup the search window that is bigger than the template size, of course */
                        cv::Rect searchWindow = cv::Rect()+CORRELATION_SEARCH_WINDOW_SIZE;
                        searchWindow -= cv::Point(searchWindow.width/2, searchWindow.height/2);
                        searchWindow += t.viewportCenter();

                        /* calculate a correlation map between search window and template */
                        cv::matchTemplate(cv::Mat(view, searchWindow), t.templ,
                                          matchTemplateOutputEstimation, CV_TM_SQDIFF);

                        double norm = cv::norm(t.templ);
                        cv::sqrt(matchTemplateOutputEstimation, matchTemplateOutputEstimation);
                        if(norm > 0.0) matchTemplateOutputEstimation /= norm;

                        /* get the maximum in correlation map */
                        double min, max;
                        cv::Point minLoc, maxLoc;
                        cv::minMaxLoc(matchTemplateOutputEstimation, &min, &max, &minLoc, &maxLoc);

                        /* move found maximum such that it is relative to viewport center */
                        cv::Point loc = minLoc;
                        cv::Size mtoSize = matchTemplateOutputEstimation.size();
                        loc -= cv::Point(mtoSize.width/2, mtoSize.height/2);
                        templatePos = t.viewportCenter()+loc;
                        potentialPoints.push_back(templatePos);
                    }

                    /* stores confidence measures for each point */
                    std::vector<double> visualConfidences(potentialPoints.size());
                    std::vector<double> flowConfidences(potentialPoints.size());

                    for(size_t i = 0; i < potentialPoints.size(); i++) {
                        cv::Rect ROI_RECT = cv::Rect() + CORRELATION_PATCH_SIZE;

                        /* check if the template lies out of the the whole image */
                        if(!ROI_TRACK_SEARCH.contains(potentialPoints[i]))
                            continue;

                        /* position the ROI such that the center lies on the point */
                        ROI_RECT -= cv::Point(CORRELATION_PATCH_SIZE.width/2, CORRELATION_PATCH_SIZE.height/2);
                        ROI_RECT += cv::Point(potentialPoints[i].x, potentialPoints[i].y);


                        /* create a view of the whole data */
                        cv::Mat ROI(view, ROI_RECT);
//                        confidences[i] = std::max(1.0-cv::norm(ROI, t.templ, CV_RELATIVE_L2), 0.0);


                        /* calculate the probability for beeing the correct point
                         * with respect to the mean flow of the given track */
                        cv::Point2f flow = potentialPoints[i]-t.center();
                        cv::Point2f deltaFlow = flow-t.flow;
                        double sigma1 = CONFIDENCE_FLOW_SIGMA/t.features;
                        double sigma2 = CONFIDENCE_FLOW_SIGMA/t.features;
                        const double inner =   std::pow(deltaFlow.x/sigma1, 2.0)
                                             + std::pow(deltaFlow.y/sigma2, 2.0);
                        double flowConfidence = (t.frames>=FLOW_N)?std::exp(-1.0/2.0*inner):1.0;




                        /* setup the search window that is bigger than the template size, of course */
                        cv::Rect searchWindow = cv::Rect() + CORRELATION_PATCH_SIZE + cv::Size(2,2);
                        //cv::Rect searchWindow = cv::Rect() + CORRELATION_PATCH_SIZE;
                        searchWindow -= cv::Point(searchWindow.width/2, searchWindow.height/2);
                        searchWindow += t.viewportCenter();
                        cv::Mat ROI_PATCH(view, searchWindow);


                        /* calculate a correlation map between search window and template */
                        cv::matchTemplate(ROI_PATCH, t.templ, matchTemplateOutputFeature, CV_TM_SQDIFF);
                        cv::sqrt(matchTemplateOutputFeature, matchTemplateOutputFeature);
                        double norm = cv::norm(t.templ);
                        if(norm > 0.0) matchTemplateOutputFeature /= norm;

                        double min, max;
                        cv::minMaxLoc(matchTemplateOutputFeature, &min, &max);

                        visualConfidences[i] = std::max(1.0-min, 0.0);
                        flowConfidences[i] = visualConfidences[i] * flowConfidence;


//                        std::cout << "sigma: " << deltaFlow << ", " << sigma1 << "," << sigma2 << ", "
//                                  << flowConfidence  << ","
//                                  << confidences[i]
//                                  << std::endl;

                    }


                    /* if there are no viable points then this track can be deleted */
                    if(potentialPoints.empty()) {
                        throw false;
                    }


                    auto biggest = std::max_element(flowConfidences.begin(), flowConfidences.end());
                    size_t biggestPos = std::distance(flowConfidences.begin(), biggest);
//                    auto smallest = std::min_element(flowConfidences.begin(), flowConfidences.end());



                    double newVisualConfid = visualConfidences[biggestPos];
                    cv::Point2f newPos = potentialPoints[biggestPos];


                    if(newVisualConfid < MIN_VISUAL_CONFIDENCE)
                        throw false;

                    if(!ROI_TRACK_SEARCH.contains(newPos)
                       || t.confidence() < MIN_CONFIDENCE
                      )
                    {
                        throw false;
                    }

                    cv::Rect ROI_RECT = cv::Rect() + CORRELATION_PATCH_SIZE;

                    /* position the ROI such that the center lies on the point */
                    ROI_RECT -= cv::Point(CORRELATION_PATCH_SIZE.width/2, CORRELATION_PATCH_SIZE.height/2);
                    ROI_RECT += cv::Point(newPos.x, newPos.y);

                    /* create a view of the whole data */
                    cv::Mat ROI(view, ROI_RECT);


                    t.frames += 1;

                    cv::Point2f deltaPos = newPos-t.center();
                    t.flow = t.flow*(1.0-1.0/FLOW_N) + deltaPos*(1.0/FLOW_N);

                    t.setViewportCenter(newPos);


                    double confidFeature;
                    confidFeature = sum(cv::Mat(featureCount, t.viewport).mul(countMask))[0];
                    confidFeature = std::exp(-1.0/2.0*std::pow((confidFeature-1.0)/0.5, 2.0));

                    /* count the number of tracks in given point. the first track at this position
                     * will always see 1. Any newer track will loose against this, as it sees more than
                     * one track. This leads to a high downvote in confidence */
                    trackCount.at<double>(newPos)++;
                    double confidTrackAmbiguity;
                    confidTrackAmbiguity = sum(cv::Mat(trackCount, t.viewport).mul(countMask))[0];
                    confidTrackAmbiguity = std::exp(-1.0/2.0*std::pow((confidTrackAmbiguity-1.0)/0.5, 2.0));


                    double allConfid = *biggest * confidFeature * confidTrackAmbiguity;

                    double frameFrac = 1.0/MIN_FRAME_COUNT;
                    t.features = t.features*(1.0-frameFrac) + allConfid*frameFrac;


                    /* only update point when a real feature has been found.
                     * this enforces a feature point to be very near the found center as well as a good match
                     * regarding the former template */
                    if(newVisualConfid * confidFeature > 0.9) {
                        ROI.copyTo(t.templ);
                    }



                } catch (...) {
                    std::lock_guard<std::mutex> lck(disposeableTrackLock);
                    disposeableTracks.insert(t);
                }
            }
        };



        /* run the blockmatching concurrent */
        std::vector<std::thread> threads;
        int num_threads = std::thread::hardware_concurrency();
        if(num_threads < 2) num_threads = 2;
        for(int i = 0; i < num_threads; i++) {
            threads.push_back(std::thread(matcherFunction, i, num_threads));
        }
        /* wait for them */
        for(size_t i = 0; i < threads.size(); i++) threads[i].join();


        /* reset track mask */
        trackMask.setTo(0);

        for(const auto t : m_featureTracks) {

            const cv::Size TRACK_MASK_SIZE(11, 11);

            const cv::Size & patchSize = TRACK_MASK_SIZE;
            cv::Rect ROI_RECT = cv::Rect() + patchSize;
            /* position the ROI such that the center lies on the point */
            ROI_RECT -= cv::Point(patchSize.width/2, patchSize.height/2);
            ROI_RECT += cv::Point(t.center().x, t.center().y);

            cv::Mat(trackMask, ROI_RECT).setTo(255);

        }

        /* go through each found corner and create a new track from it */
        for(cv::Point2f & p : corners) {

            /* check that there is no track around this position */
            if(trackMask.at<unsigned char>(p))
                continue;

            cv::Rect ROI_RECT = cv::Rect() + CORRELATION_PATCH_SIZE;

            /* position the ROI such that the center lies on the point */
            ROI_RECT -= cv::Point(CORRELATION_PATCH_SIZE.width/2, CORRELATION_PATCH_SIZE.height/2);
            ROI_RECT += cv::Point(p.x, p.y);

            /* create a view of the whole data */
            cv::Mat ROI(view, ROI_RECT);

            FeatureTrack t(p, ROI_RECT, m_id++);
            t.features = MIN_CONFIDENCE;
            t.flowSigma = cv::Point2f(200.0, 200.0);
            ROI.copyTo(t.templ);
            m_featureTracks.push_back(t);
        }

        /* remove disposable tracks */
        for(auto it = m_featureTracks.begin(); it != m_featureTracks.end();) {
            if(disposeableTracks.contains(*it)) {
                //std::cout << "removing track at: " << t.center() << t.id << std::endl;
                it = m_featureTracks.erase(it);
            } else {
                it++;
            }
        }


//            static double thresGradient = 0.1;
//            cv::Sobel(view, edgesX, CV_32F, 1, 0);
//            edgesX = abs(edgesX)/255.0;
//            edgesX.setTo(1.0, edgesX>thresGradient);
//            cv::GaussianBlur(edgesX, blurredEdgesX, cv::Size(101, 101), 20);

//            cv::Sobel(view, edgesY, CV_32F, 0, 1);
//            edgesY = abs(edgesY)/255.0;
//            edgesY.setTo(1.0, edgesY>thresGradient);
//            cv::GaussianBlur(edgesY, blurredEdgesY, cv::Size(101, 101), 20);

//            edges = cv::Mat(view.size(), CV_32FC1);
//            edges = 0.0;
//            edges = blurredEdgesX.mul(blurredEdgesY);

//            cv::GaussianBlur(edges, blurredEdges, cv::Size(101, 101), 50);
//            blurredEdges = edges.mul(blurredEdges);

//            featureMask.setTo(0,(blurredEdges > 0.05));


        FeatureTrackList result;
        for(const FeatureTrack & t : m_featureTracks) {


            /* if the minimum number of frames has not been reached,
             * omit this track */
            if(t.frames <= MIN_FRAME_COUNT)
                continue;

            result.push_back(t);
        }




        /* create color image into that we can draw */
        cv::Mat paintSurface;
        cv::Mat maskColor;
        cv::cvtColor(view, paintSurface , CV_GRAY2RGB);
        cv::cvtColor(featureSearchMask, maskColor, CV_GRAY2RGB);



        for(FeatureTrack & ft: result) {
            double scaledConfid = (ft.confidence()-MIN_CONFIDENCE)/(1.0-MIN_CONFIDENCE);
            cv::circle(paintSurface , ft.center() , 2, cv::Scalar(0, scaledConfid*255, 255*(1-scaledConfid)));
        }
//        for(cv::Point2f & c: corners ) {
//            cv::circle(paintSurface , c , 2, cv::Scalar(0, 0, 255, 125));
//        }


        std::stringstream title;
        title << "boschgt-tracker for view:" << int(m_view);

        cv::namedWindow(title.str(), CV_NORMAL);
        cv::imshow(title.str() , (paintSurface & maskColor));
//        cv::imshow("boschgt-tracker", (paintSurface ));

        //std::cout << "flow: " << denseFlow << std::endl;

        cv::waitKey(3);
        view.copyTo(lastView);

        TOC;

        return result;



}

TrackList FeatureTracker::track(Frame &f)
{
    FeatureTrackList needleTracks = this->trackFrame(f);



    if(!m_dryRun) {
        for(auto & ft : needleTracks) {

            /* track has not been created */
            if(!m_tracks.count(ft)) {
                m_tracks[ft] = f.sequence().addTrack();
            }
            Vector2d pos(ft.center().x, ft.center().y);
            f.linkTrack(m_tracks[ft], pos, m_view);

        }
    }

    FeatureTrackSet deletedTracks = FeatureTrackSet::fromList(map_keys(m_tracks))-FeatureTrackSet::fromList(needleTracks);
    for(auto & ft: deletedTracks) {
        m_tracks.erase(ft);
    }

    return map_values(m_tracks);
}



