#ifndef BOSCHGT_AUTOANNOTATE_TRACKER_H
#define BOSCHGT_AUTOANNOTATE_TRACKER_H

#include <unordered_map>
#include <tclap/CmdLine.h>

#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/Track.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>


struct FeatureTrack {


    int id;

    /** stores last patch */
    cv::Mat templ;

    /** stores last position of templ in frame */
    cv::Rect viewport;

    /** counts how many frames this track has lived so far */
    int frames;

    /** stores the difference of initial viewport center and feature position */
    cv::Point2f delta;

    cv::Point2f flow;
    cv::Point2f flowSquaredSum;
    cv::Point2f flowSigma;

    /** counts how many features this frame has seen */
    double features;

    std::unordered_map<hookers::interface::Frame, hookers::Vector2d> framePos;


    FeatureTrack(const cv::Point2f & center, const cv::Rect & viewport, int id)
        : id(id), viewport(viewport), frames(0), flow(0.0,0.0), features(0)
    {
        /* calculate the difference of the center of viewport from the feature position */
        delta = center-cv::Point2f(viewportCenter().x, viewportCenter().y);
    }


    void setViewportCenter(cv::Point p) {
        int cx = viewport.width/2;
        int cy = viewport.height/2;

        cv::Point dP = cv::Point(cx, cy);

        /* move viewport to new center position */
        viewport +=  (p-dP)-viewport.tl();
    }

    inline cv::Point viewportCenter() const {
        return viewport.tl()+cv::Point(viewport.width/2, viewport.height/2);
    }

    inline cv::Point2f center() const {
        return cv::Point2f(viewportCenter().x,viewportCenter().y)+delta;
    }

    /**
     *  confidence about constancy throughout all tracked frames
     */
    inline double confidence() const {
        return features/*/double(frames)*/;
    }

    bool operator==(const FeatureTrack & other) const {
        if(id == 0)
            return false;
        return id == other.id;
    }

    /**
     * call this function in order to clear the frame position cache
     * and update the track.
     * If the provided track is invalid, just clear the cache */
    void updateTrack(hookers::interface::Track & t) {
        /* if a valid track is provided, we have to update all recent positions */
        if(t.isValid()) {
            for(auto it = framePos.begin(); it != framePos.end(); it++) {
                hookers::interface::Frame  f = (*it).first;
                hookers::Vector2d & p = (*it).second;
                f.linkTrack(t, p);
            }

        }
        framePos.clear();
    }
};

namespace std {
  template <> struct hash<FeatureTrack>
  {
    size_t operator()(const FeatureTrack & x) const
    {
        return x.id;
    }
  };
}





typedef hookers::interface::AbstractObjectSet<FeatureTrack> FeatureTrackSet;
typedef hookers::interface::AbstractObjectList<FeatureTrack> FeatureTrackList;




/**
 * @brief The FeatureTracker class
 *
 * This class tracks features that once were identified as good features to track.
 * The tracking is realized by template based matching.
 * However, in order to measure the quality of a track, it will be counted how often
 * a feature point from an edge detector lies inside the search window.
 *
 * If this measure drops below a certain threshold, the track will be dropped again.
 *
 * The most important function is track() which can be called iteratively for each frame. See
 * at track() for further reference.
 *
 * Basic usage is like this:
 *
 * @code
 * FeatureTracker tracker;
 * FrameList frames;
 *
 * for(auto & f: frames) {
 *   TrackList currentTracks = tracker.track(f);
 *   // do something with the tracks here
 * }
 * @endcode
 *
 * Note, that the frame list doesn't neccesarily need to contain only frames from one sequence.
 * It could also be used to find corresponding points in a stereo view, etc.
 *
 *
 */
class FeatureTracker {

    unsigned m_id;
    std::vector<FeatureTrack> m_featureTracks;
    std::unordered_map<FeatureTrack, hookers::interface::Track> m_tracks;

    cv::Size m_patchSize;
    cv::Size m_searchWindowSize;


    cv::Mat viewRaw, view, blurred, edges, blurredEdges;
    cv::Mat edgesX, edgesY;
    cv::Mat blurredEdgesX, blurredEdgesY;

    cv::Mat lastView;

    cv::Mat trackMask;

    bool m_histEqualize;
    bool m_dryRun;
    hookers::interface::Frame::View m_view;



public:
    FeatureTracker(const hookers::interface::Frame::View &view = hookers::interface::Frame::View::Base);

    /**
     * @brief lightweight tracker function that only tracks temporary objects
     * @see track() for a complete tracker functionality
     *
     *
     */
    FeatureTrackList trackFrame(hookers::interface::Frame & f);

    /** tracks a frame and handles track registration into sequence and frames */
    hookers::interface::TrackList track(hookers::interface::Frame & f);

    void setHistogramEqualization(bool on)
    {
        m_histEqualize = on;
    }

    bool histogramEqualization() const
    {
        return m_histEqualize;
    }

    void setDryRun(bool on)
    {
        m_dryRun = on;
    }

    bool dryRun() const
    {
        return m_dryRun;
    }


};


#endif // BOSCHGT_AUTOANNOTATE_TRACKER_H

