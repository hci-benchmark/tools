#include <iostream>
#include <string>
#include <sstream>

#include <tclap/CmdLine.h>

#include <hookers/interface/Project.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/NumericTypes.h>


class IntrinsicsImport{
public:
	IntrinsicsImport(std::vector<std::string> & args);
};

IntrinsicsImport::IntrinsicsImport(std::vector<std::string> & args)
{
	TCLAP::CmdLine cmd("boschgt raw sequence importer", ' ', "0.1");

	TCLAP::ValueArg<std::string>
		projectArg(std::string(),
		"project",
		"project name (eg. 20130101_0000)",
		false,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		sequenceArg(std::string(),
		"sequence",
		"sequence name (eg. 0_0000)",
		true,
		std::string(),
		"string",
		cmd);

	TCLAP::MultiArg<std::string> 
		viewArg(
		std::string(), "view",
		"instrinsics will be set for this view : -Base -Stereo",
		false,
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		sourceArg(std::string(),"source",
		"source of intrinsics values:\n -Base -Stereo -Fx:Fy:Cx:Cy",
		true,
		std::string(),
		"string",
		cmd);

	try {
		cmd.parse(args);

	}
	catch (TCLAP::ArgException &e)  {
		std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
		return;
	}

	Project project = Project::open(projectArg.getValue());
	if (!project.isValid()) {
		throw std::runtime_error("[ERROR] Project could not be opened.");
	}

	SequenceList sequences = project.sequences();
	Sequence sequence = sequences.byLabel(sequenceArg.getValue());
	if (!sequence.isValid()) {
		throw std::runtime_error("Sequence '" + sequenceArg.getValue() + "' could not be found in project.");
	}

	FrameList frames = sequence.frames();
	if (frames.size() == 0)
	{
		throw std::runtime_error("Sequence '" + sequenceArg.getValue() + "' Does not have frame");
	}

	Vector4d intrinsics;

	if (sourceArg.getValue() == "Base")
		intrinsics = frames[0].intrinsics(FrameData::Base);
	else if (sourceArg.getValue() == "Stereo")
		intrinsics = frames[0].intrinsics(FrameData::Stereo);
	else
	{
		std::istringstream ss(sourceArg.getValue());
		std::string intVal;

		for (int i = 0; i < 4;i++){
			std::getline(ss, intVal, ':');
			intrinsics[i]=std::stod(intVal);
		}

	}
	if (!viewArg.getValue().empty()){
		std::vector<std::string> views = viewArg.getValue();

		if (std::count(views.cbegin(), views.cend(), "Base"))
		{
                        for (unsigned int i = 0; i < frames.size(); i++)
				frames[i].setIntrinsics(intrinsics, FrameData::Base);
		}
		if (std::count(views.cbegin(), views.cend(), "Stereo"))
		{
                        for (unsigned int i = 0; i < frames.size(); i++)
				frames[i].setIntrinsics(intrinsics, FrameData::Stereo);
		}
	}
	else{
                for (unsigned int i = 0; i < frames.size(); i++){
			frames[i].setIntrinsics(intrinsics, FrameData::Base);
			frames[i].setIntrinsics(intrinsics, FrameData::Stereo);

		}
	}

}
