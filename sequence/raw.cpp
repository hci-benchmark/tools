#include "raw.h"
#include <vigra/timing.hxx>
#include <thread>


using namespace hookers;
using namespace hookers::interface;

void RawImporter::readLUT(std::ifstream & f, LookupTable & lut)
{

	assert(lut.type() == CV_32FC2);

	if (!f.is_open())
		return;

	f.seekg(0);


	int pixelCount = lut.cols*lut.rows;

	/* counts any valid rows that we have processed so far */
	int nrows = 0;

	/* counts the pixel rows we have processed so far */
	int prows = 0;

	float * lutPtr = lut.ptr<float>();

	while (!f.eof() && prows < pixelCount) {
		std::string line;
		std::getline(f, line);

		if (line.front() == '#')
			continue;

		/* check if the line is empty */
		if (line.empty())
			continue;

		/* counts how many valid lines we have processed so far */
		nrows++;

		/* skip the header */
		if (nrows <= 2) {
			std::cout << "Header: " << line << std::endl;
			continue;
		}


		prows++;

		float dx, dy;
		std::stringstream lineStream(line);
		lineStream >> dx >> dy;

		*(lutPtr++) = dx;
		*(lutPtr++) = dy;

	}

	if (prows != lut.rows*lut.cols) {
		std::stringstream stream;
		stream << "Loading LUT for " << lut.cols << "x" << lut.rows << ", but read " << prows << " rows only.";
		throw std::runtime_error(stream.str());
	}
}


RawImporter::RawImporter(std::vector<std::string> & args, bool meta)
{

	TCLAP::CmdLine cmd("boschgt raw sequence importer", ' ', "0.1");

	TCLAP::ValueArg<std::string>
		sequenceNameArg(std::string(),
		"sequence",
		"sequence name (eg. 20130101_0000)",
        false,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		projectNameArg(std::string(),
		"project",
		"name of the project.",
		false,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		foldeerPathArg("D",
		"datapath",
		"path of the folder contains raw data",
		true,
		std::string(),
		"string",
		cmd);

    TCLAP::SwitchArg updateSwitch("U", "update", "update the sequence if it already exists", cmd, false);

/*	TCLAP::ValueArg<std::string>
		inputLeftArg(std::string(),
		"left-frames",
		"left raw frames file",
		true,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		inputRightArg(std::string(),
		"right-frames",
		"right raw frames file",
		true,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		inputLeftLutArg(std::string(),
		"left-lut",
		"left calibration lookup table",
		false,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		inputRightLutArg(std::string(),
		"right-lut",
		"right calibration lookup table",
		false,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<int>
		inputNumberOfFrames(std::string(),
		"frames",
		"number of frames to import",
		false,
		0,
		"integer",
		cmd);

	TCLAP::ValueArg<std::string>
		infotextArg(std::string(),
		"info",
		"address of thetext file contains sequence information",
		false,
		std::string(),
		"string",
		cmd);


	TCLAP::ValueArg<std::string>
		calibtextArg(std::string(),
		"calib",
		"address of the text file contains calibration information",
		false,
		std::string(),
		"string",
		cmd);

	TCLAP::ValueArg<std::string>
		dstPath(std::string(),
		"dst",
		"address for saving videos",
		false,
		std::string(),
		"string",
		cmd);*/

	try {
		cmd.parse(args);

	}
	catch (TCLAP::ArgException &e)  {
		std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
		return;
	}





	try {
		std::string folderPath = foldeerPathArg.getValue();
        std::cout << "[INFO] Importing information of the sequence..." << std::endl;


        RawReader rawreader(folderPath);


        RawReader::SequenceInfo sequenceInfo = rawreader.sequenceInfo();

        std::cout << "path is:" << sequenceInfo.calibrationPath << std::endl;

		/* open raw frame files and check if they could be opened and that their file sizes match */
		std::ifstream leftFile;
        int leftFileSize = 0;
		leftFile.open(folderPath+"/"+sequenceInfo.leftRawFName, std::ios_base::binary);
		leftFile.seekg(0, std::ios_base::end); leftFileSize = leftFile.tellg();
		leftFile.seekg(0);


		std::ifstream rightFile;
        int rightFileSize = 0;
		rightFile.open(folderPath + "/" + sequenceInfo.rightRawFName, std::ios_base::binary);
		rightFile.seekg(0, std::ios_base::end); rightFileSize = rightFile.tellg();
		rightFile.seekg(0);


		if (!leftFile.is_open() || !rightFile.is_open())
			throw std::runtime_error("Could not open raw input files.");


        Project project = Project::open(projectNameArg.getValue());
		if (!project.isValid()) {
			throw std::runtime_error("Could not open Project with name '" + projectNameArg.getValue() + "'.");
		}



        std::string seqName = sequenceInfo.name;
        if(seqName.empty()) {
                seqName = sequenceNameArg.getValue();
        }

        if(seqName.empty()) {
            throw std::runtime_error("No sequence name is specified or could be derived from raw files.");
        }

        Sequence sequence = project.sequences().byLabel(seqName);

        if(meta) {

            if(sequence.isValid()) {
                std::stringstream err;
                err << "Cannot import sequence " << seqName << ", it already exists.";
                throw std::runtime_error(err.str());
            }

            sequence = project.addSequence(seqName);

            if(!sequence.isValid()) {
                throw std::runtime_error("Creating sequence " + seqName + " failed.");
            }

            sequence.setTime(sequenceInfo.sequenceTime);
            sequence.save();
            project.save();
            return;
        }

		if (!sequence.isValid())
			throw std::runtime_error("Sequence " + sequenceNameArg.getValue() + " could not be opened. Check if it exists.");

		FrameList frames = sequence.frames();
        if (!frames.empty() && !updateSwitch.getValue())
            throw std::runtime_error("Sequence " + seqName + " has already frames. Not updating.");


        if (updateSwitch.getValue() && ((int)frames.size() != sequenceInfo.numOfFrames))
            throw std::runtime_error("Update of " + seqName + " failed: Number of frames don't match");

		
		bool option_rectify = false;

		cv::Mat unrectifiedLeft(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_8UC1);
		cv::Mat unrectifiedRight(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_8UC1);


		/* create buffers for left and right view */
		cv::Mat leftImage(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_8UC1);
		cv::Mat rightImage(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_8UC1);

		cv::Mat leftImageFinal(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_8UC1);
		cv::Mat rightImageFinal(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_8UC1);


		/* check for rectifiying maps and prepare lookup tables */
		LookupTable leftLut(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_32FC2);
		LookupTable rightLut(sequenceInfo.frameHeight, sequenceInfo.frameWidth, CV_32FC2);
		leftLut = 0.0;
		rightLut = 0.0;

		std::string pathLUTLeft;
		std::string pathLUTRight;
		/*if (inputLeftLutArg.isSet() && inputRightLutArg.isSet()) {
			pathLUTLeft = inputLeftLutArg.getValue();
			pathLUTRight = inputRightLutArg.getValue();
		}*/
		{
            std::string pathToLUT = sequenceInfo.calibrationPath;
			pathLUTLeft = pathToLUT + "_r2d_c0.lut";
			pathLUTRight = pathToLUT + "_r2d_c1.lut";
		}

		std::ifstream fLeftLut;
		std::ifstream fRightLut;
		fLeftLut.open(pathLUTLeft, std::ios_base::binary);
		fRightLut.open(pathLUTRight, std::ios_base::binary);

		if (!fLeftLut.is_open() || !fRightLut.is_open()) {
			throw std::runtime_error("Could not open LUT files.");
		}
		if (fLeftLut.is_open() /*&& fRightLut.is_open()*/) {
			std::cout << "[STATUS] Reading in Lookup Tables..." << std::endl;
			readLUT(fLeftLut, leftLut);
			readLUT(fRightLut, rightLut);


			/* cv::remap needs absolute mapping positions. */
			for (int i = 0; i < sequenceInfo.frameHeight; i++) {
				for (int j = 0; j < sequenceInfo.frameWidth; j++) {
					leftLut.at<cv::Vec2f>(i, j)[0] += j;
					leftLut.at<cv::Vec2f>(i, j)[1] += i;
					rightLut.at<cv::Vec2f>(i, j)[0] += j;
					rightLut.at<cv::Vec2f>(i, j)[1] += i;
				}
			}
			std::vector<cv::Mat> lLayers, rLayers;
			cv::split(leftLut, lLayers);
			option_rectify = true;
		}

	
		/* we need this number for status outputs */
		int percent = sequenceInfo.numOfFrames / 100.0;
		/* walk through frames and add them to sequence */


        /* used to parallize fetching raw data and compressing */
        auto readFetchData = [&] {
            leftFile.read(reinterpret_cast<char*>(leftImage.ptr()), (sequenceInfo.frameHeight*sequenceInfo.frameWidth));
            rightFile.read(reinterpret_cast<char*>(rightImage.ptr()), (sequenceInfo.frameHeight*sequenceInfo.frameWidth));
        };

        /* call it once before the loop, such that data is available */
        readFetchData();

        Frame frame;
		for (int f = 0; f <sequenceInfo.numOfFrames; f++) {

			if (!(f % (percent * 5)))
				std::cout << f / percent << "% " << std::flush;


			ImageShape shape;
			shape[0] = sequenceInfo.frameWidth;
			shape[1] = sequenceInfo.frameHeight;

            if(frames.empty()) {
                frame = sequence.addFrame();
            } else {
                frame = frames[f];
            }

            if (option_rectify) {
                    cv::remap(leftImage, leftImageFinal, leftLut, cv::Mat(), cv::INTER_LINEAR);
                    cv::remap(rightImage, rightImageFinal, rightLut, cv::Mat(), cv::INTER_LINEAR);
            }
            else {
                leftImage.copyTo(leftImageFinal);
                rightImage.copyTo(rightImageFinal);
            }

            ImageView leftImageView(shape, leftImageFinal.data);
            ImageView rightImageView(shape, rightImageFinal.data);

            std::thread execDataFetcher(readFetchData);

            frame.setView(leftImageView, Frame::View::Base);
			frame.setView(rightImageView, Frame::View::Stereo);

            frame.setTime(sequenceInfo.sequenceTime + double(f)/double(sequenceInfo.leftFPS));

            /* set the intrinsics on both views */
            for(auto v: {Frame::View::Base, Frame::View::Stereo}) {
                frame.setIntrinsics({ sequenceInfo.camParams.f[0],
                                    sequenceInfo.camParams.f[1],
                                    sequenceInfo.camParams.c[0],
                                    sequenceInfo.camParams.c[1] }, v);

                frame.setIntrinsicsUncertainty({sequenceInfo.camParams.df[0],
                                                sequenceInfo.camParams.df[1],
                                                sequenceInfo.camParams.dc[0],
                                                sequenceInfo.camParams.dc[1]}, v);
            }

            Vector6d vt(0);
            vt.subarray<0, 3>() = Vector3d({sequenceInfo.camParams.t[0],
                                            sequenceInfo.camParams.t[1],
                                            sequenceInfo.camParams.t[2]});
            frame.setViewTransformation(vt);

            Vector6d vtu(0);
            vtu.subarray<0,3>() = Vector3d({sequenceInfo.camParams.dt[0],
                                            sequenceInfo.camParams.dt[1],
                                            sequenceInfo.camParams.dt[2]});
            frame.setViewTransformationUncertainty(vtu);



            execDataFetcher.join();

		}

		std::cout << "[STATUS] Finished reading " << sequenceInfo.numOfFrames << " ..." << std::endl;


		/*  sequence.save();
		project.save();*/

	}
	catch (std::exception & e) {
        //std::cout << "[ERROR] " << e.what() << std::endl;
        throw;

	}



}

///*RawImporter::RawFrame RawImporter::nextFrame()
//{
///** @todo implement me */
///* see at RawImporter::RawImporter() on how to get the next frames from the raw files,
//* and implement it here.
//* Note that we are only fetching exactly one frame */


////}


