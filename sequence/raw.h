#ifndef BOSCHGT_PROJECT_TOOLS_SEQUENCE_RAWIMPORTER_H
#define BOSCHGT_PROJECT_TOOLS_SEQUENCE_RAWIMPORTER_H

#include <memory>

#include <vector>

#include <sstream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <tclap/CmdLine.h>

#include "rawreader.h"

#include <vigra/multi_array.hxx>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include <hookers/interface/Project.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Frame.h>


class RawImporter {

public:
    typedef cv::Mat LookupTable;

    RawImporter(std::vector<std::string> & args, bool meta = false);

    /**
     * @brief readLUT
     * @param f
     * @param lut openCV 2d mat type with CV_32FC2 format
     */
    void readLUT(std::ifstream & f, LookupTable & lut);
};



#endif



