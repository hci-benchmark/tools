#include <regex>


# include "rawreader.h"

/**
 * @brief use other functions to read sequence info and camra parameters
 * @param path path to the sequence folder that contains info.txt and calib.txt
 */
void RawReader::readInfo(const std::string & path){

	this->infoFile.open(path + "/info.txt", std::ios_base::binary);

    if (!infoFile.is_open()){
		throw std::runtime_error("[ERROR] Could not open Information files. There should be info.txt file in your given path");
	}

    /* read infos from info.txt */
    SequenceInfo sequenceInfo;



    /* check if we can guess the sequence name from the path... */
    {
        std::regex rgx("^((?:.*[\\\\/])*)(.*)$");
        std::smatch match;
        if (std::regex_search(path, match, rgx)) {
            m_info.name = match[2];
        }
    }

	readSequnceTime(sequenceInfo);
	readRawFileName(sequenceInfo);
	readFrameFPS(sequenceInfo);
	readFrameWidth(sequenceInfo);
	readFrameHeight(sequenceInfo);
	readNumOfFrames(sequenceInfo);


    /* read calibration */
    readClibrationPath(path + "/calib.txt");
    this->parameterFileName = m_info.calibrationPath.substr(0, (m_info.calibrationPath.find_last_of("/"))) + "/Tmcc.log";
    readCalibration(this->parameterFileName);

}

void RawReader::readNumOfFrames(SequenceInfo & sequenceInfo){
	/**@brief read frame number from ifnfo.txt
	*
	*/
	if (!this->infoFile.is_open()){
		throw std::runtime_error("[ERROR] Could not open Information files. There should be info.txt file in your given path");
	}
	

	std::string line;
	while (getline(infoFile, line)) {
		std::istringstream iss(line);
		std::string s;
		int i;
		iss >> s >> i;
		if (s == "NumOfImages:"){
            m_info.numOfFrames = i;
			break;
		}
	}

}

void RawReader::readFrameFPS(SequenceInfo & sequenceInfo){
	/**@brief read FPS from ifnfo.txt
	*here fps value for left and right frames provided but it is same
	*/

	std::string line;
	while (getline(infoFile, line)) {
		std::istringstream iss(line);
		std::string s1, s2, s3, s4;
		double d;
		iss >> s1 >>s2 >> s4 >> d >> s3;
		if (s1 == "FPS" && s2 == "left"){
            m_info.leftFPS = d;
		}
		if (s1 == "FPS" && s2 == "right:"){
			iss >> s1 >> s2 >> d >> s3;
            m_info.rightFPS = d;
			break;
		}
	}

}

void RawReader::readSequnceTime(SequenceInfo & sequenceInfo){
	/**@brief read sequence time from ifnfo.txt 
	* final result save as a unix time
	*/

	struct tm timeinfo = { 0 };

	std::string line;
	int lineNum = 0;
	while (getline(this->infoFile, line)) {
		lineNum++;
		if (lineNum == 2){
			std::istringstream iss(line);
			iss >> std::get_time(& timeinfo, " %d.%m.%Y %H:%M:%S");
            m_info.sequenceTime = mktime(& timeinfo);
			break;
		}
	}
}


void RawReader::readRawFileName(SequenceInfo & sequenceInfo){
	/**@brief read raw data file name from ifnfo.txt
	*
	*/

	std::string line;

	while (getline(this->infoFile, line)){
		std::istringstream iss(line);
		std::string s1, s2, s3, s4;
		iss >> s1 >> s2 >> s3 >> s4;
		if (s1 == "Filename" && s2 == "left"){
            m_info.leftRawFName = s4;
		}
		if (s1 == "Filename" && s2 == "right:"){
			iss >> s1 >> s2 >> s3;
            m_info.rightRawFName = s3;
			break;
		}
		
	}

}


void RawReader::readFrameHeight(RawReader::SequenceInfo & sequenceInfo){
	/**@brief read Frames Height from ifnfo.txt
	*
	*/


	std::string line;
	while (getline(infoFile, line)) {
		std::istringstream iss(line);
		std::string s;
		int i;
		iss >> s >> i;
		if (s == "Ny:"){
            m_info.frameHeight = i;
			break;
		}
	}
}

void RawReader::readFrameWidth(RawReader::SequenceInfo & sequenceInfo){
	/**@brief read Frames width from ifnfo.txt
	*
	*/


	std::string line;
	while (getline(infoFile, line)) {
		std::istringstream iss(line);
		std::string s;
		int i;
		iss >> s >> i;
		if (s == "Nx:"){
            m_info.frameWidth = i;
			break;
		}
	}
}

void RawReader::readCalibration(const std::string &filename)
{


	std::ifstream fin(filename.c_str());

    if(!fin.is_open()) {
        std::stringstream err;
        err << "Could not open calibration path file: " << filename;
        throw std::runtime_error(err.str());
    }

    Params & p = m_info.camParams;
	while (fin.is_open() && !fin.eof())
	{
		std::string line;
		std::getline(fin, line);
		if (line.find("Translation:") != std::string::npos)
			parseTranslation(fin, p.b, p.db, p.t, p.dt);
		else if (line.find("Estimated interior orientation parameters:") != std::string::npos)
			parseIntrinsicError(fin, p.df, p.dc);
		else if (line.find("Camera 0:Parameters") != std::string::npos)
			parseIntrinsic1(line, p.f, p.c);
		else if (line.find("please select the character of a parameter ('q'-quit)") != std::string::npos)
			parseIntrinsic2(fin, p.f, p.c);
	}
}

void RawReader::parseIntrinsic1(std::string line, double * f, double* c)
{
//#if LOG
//    std::cerr << "\tLOG: INTRINSIC FOUND (Single line Format)\n";
//#endif
	std::string fstr = line.substr(line.find("c=") + 2);
	fstr = fstr.substr(fstr.find_first_not_of(' '));
	fstr = fstr.substr(0, fstr.find_first_of(' '));
	f[0] = f[1] = std::atof(fstr.c_str());
	std::string xhstr = line.substr(line.find("xh=") + 3);
	std::string yhstr = line.substr(line.find("yh=") + 3);
	c[1] = std::atof(yhstr.c_str());
	c[0] = std::atof(xhstr.substr(0, xhstr.find_first_of(' ')).c_str());
}
void RawReader::parseIntrinsic2(std::ifstream & fin, double * f, double* c)
{
//#if LOG
//    std::cerr << "\tLOG: INTRINSIC FOUND (Multiline Format)\n";
//#endif
	std::string line;
	std::getline(fin, line); line = "";
	std::getline(fin, line);
	std::string fstr = line.substr(line.find("(a)") + 3);
	fstr = fstr.substr(fstr.find_first_not_of(' '));
	f[0] = f[1] = std::atof(fstr.substr(0, fstr.find_first_of('|')).c_str());
	std::getline(fin, line); line = "";
	std::getline(fin, line);
	std::string xstr = line.substr(line.find("(e)") + 3);
	xstr = xstr.substr(xstr.find_first_not_of(' '));
	c[0] = std::atof(xstr.substr(0, xstr.find_first_of('|')).c_str());
	line = "";
	std::getline(fin, line);
	std::string ystr = line.substr(line.find("(g)") + 3);
	ystr = ystr.substr(ystr.find_first_not_of(' '));
	c[1] = std::atof(ystr.substr(0, ystr.find_first_of('|')).c_str());
}

void RawReader::parseIntrinsicError(std::ifstream & fin, double * df, double* dc)
{
//#if LOG
//    std::cerr << "\tLOG: INTRINSIC ERR FOUND\n";
//#endif
	std::string line;
	{
		std::getline(fin, line); line = "";
		std::getline(fin, line);
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		double dc1 = std::atof(line.substr(0, line.find_first_of(' ')).c_str());
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		double dc2 = std::atof(line.c_str());
		df[0] = df[1] = (dc1 + dc2) / 2.0;
	}
	{
		std::getline(fin, line); line = "";
		std::getline(fin, line);
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		double dc1 = std::atof(line.substr(0, line.find_first_of(' ')).c_str());
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		double dc2 = std::atof(line.c_str());
		line = "";
		std::getline(fin, line);
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		double dc3 = std::atof(line.substr(0, line.find_first_of(' ')).c_str());
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		double dc4 = std::atof(line.c_str());
		dc[0] = dc[1] = (dc1 + dc2 + dc3 + dc4) / 4.0;
	}
}


void RawReader::parseTranslation(std::ifstream &fin, double &b, double & db,double *t, double *dt )
{
//#if LOG
//    std::cerr << "\tLOG: TRANSLATION FOUND\n";
//#endif
	/*double t[3] = { -1, -1, -1 };
	double dt[3] = { -1, -1, -1 };*/
	for (int ii = 0; ii < 3; ++ii)
	{
		std::string line;
		std::getline(fin, line);
		line = line.substr(line.find("=") + 1);
		line = line.substr(line.find_first_not_of(' '));
		t[ii] = std::atof(line.substr(0, line.find("+-")).c_str());
		line = line.substr(line.find("+-") + 2);
		line = line.substr(line.find_first_not_of(' '));
		dt[ii] = std::atof(line.c_str());
	}
	auto t00 = t[0] * t[0];
	auto t11 = t[1] * t[1];
	auto t22 = t[2] * t[2];
	auto dt00 = dt[0] * dt[0];
	auto dt11 = dt[1] * dt[1];
	auto dt22 = dt[2] * dt[2];

	b = std::sqrt(t00 + t11 + t22);
	auto bb = b*b;
	db = std::sqrt(dt00*t00 / bb + dt11*t11 / bb + dt22*t22 / bb);
}

/**
 * extract folder path of the LUT and camera parameters faile from calib.txt in sequence folder.
 */
void RawReader::readClibrationPath(const std::string & fileName){


	std::string clibPath;
	std::ifstream fCalib;
	fCalib.open(fileName, std::ios_base::binary);

    if(!fCalib.is_open()) {
        throw std::runtime_error("Could not open Calibration Path File at:" + fileName);
    }


	fCalib.seekg(0);
	int lineNum = 0;

	while (!fCalib.eof() && lineNum<23) {
        std::string line;
		std::getline(fCalib, line);

        if (!line.size()) {
			throw std::runtime_error(" calib.txt file is empty! \n");
        }

        if(line.front() == '\\' || line.front() == '/' || (line.find(':') != std::string::npos)) {
            throw std::runtime_error("Absolute pathes  in calib.txt are not supported.");
        }

        m_info.calibrationPath = line;
        return;
	}
}

RawReader::RawReader(const std::string &path)
{
    this->readInfo(path);
}

const RawReader::SequenceInfo & RawReader::sequenceInfo()
{
    return m_info;
}


