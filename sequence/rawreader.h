#ifndef BOSCHGT_PROJECT_TOOLS_SEQUENCE_RAWREADER_H
#define BOSCHGT_PROJECT_TOOLS_SEQUENCE_RAWREADER_H

#include <sstream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <time.h>
#include <locale>
#include <iomanip>
#include <string>
#include <vector>

#include <cmath>

#define LOG 1;

class RawReader
{


private:

public:
    struct Params
    {
        double f[2];
        double df[2];
        double c[2];
        double dc[2];
        double t[3];
        double dt[3];
        double b, db;
        Params() :b(-1), db(-1)
        {
            f[0] = f[1] = df[0] = df[1] = c[0] = c[1] = dc[0] = dc[1] = t[0] = t[1] = t[2] = dt[0] = dt[1] = dt[2] = - 1;
        }

        void print() const
        {
            std::cerr << "Parameters are: ";
            std::cerr << "baseline: " << b << "+-" << db << std::endl;
            std::cerr << "focallength: " << f[0] << " " << f[1] << "+-" << df[0] << " " << df[1] << std::endl;
            std::cerr << "principle point " << c[0] << " " << c[1] << "+-" << dc[0] << " " << dc[1] << std::endl;
            std::cerr << "Note: errors for new f and pp are unknown using error of calibration\n\n";
        }

    };

    struct SequenceInfo
    {/**@brief structure to save sequence information read from folders.
     *
     */
        std::string leftRawFName, rightRawFName;
        time_t sequenceTime;
        double rightFPS, leftFPS;
        int numOfFrames;
        std::string rawVideoName;
        int frameWidth, frameHeight;
        RawReader::Params camParams;
        std::string calibrationPath;
        std::string name;
    };


private:
    void readInfo(const std::string & path);
    void readCalibration(const std::string & fileName);
    void readClibrationPath(const std::string & fileName);


public:
    RawReader(const std::string & path);


    const SequenceInfo & sequenceInfo();

    std::string calibrationPath() const {
        return m_calibrationPath;
    }

    std::string sequencePath() const {
        return m_sequencePath;
    }

	
private:

	std::ifstream infoFile;

	
	void readSequnceTime(SequenceInfo & sequenceInfo);
	void readNumOfFrames(SequenceInfo & sequenceInfo);
	void readFrameFPS(SequenceInfo & sequenceInfo);
	void readRawFileName(SequenceInfo & sequenceInfo);
	void readFrameWidth(SequenceInfo & sequenceInfo);
	void readFrameHeight(SequenceInfo & sequenceInfo);
	

	void parseIntrinsic1(std::string line, double * f, double* c);
	void parseIntrinsic2(std::ifstream & fin, double * f, double* c);
	void parseIntrinsicError(std::ifstream & fin, double * df, double* dc);
	void parseTranslation(std::ifstream &fin, double &b, double & db, double *t, double *dt);

	std::string parameterFileName;

    std::string m_calibrationPath;
    std::string m_sequencePath;
    SequenceInfo m_info;

};






#endif
