#include "voodoo.h"
#include "raw.h"
#include "intrinsicsImport.h"
#include "trackImporter.h"

int main(int argn, char ** argv) {

    TCLAP::CmdLine cmd("sequence management", ' ', "0.1");

    TCLAP::UnlabeledValueArg<std::string> moduleArg(
                "module",
                "the module to use",
                true,
                std::string(),
                "list|import|export|frame|track",
                cmd
                );
    TCLAP::UnlabeledValueArg<std::string> cmdArg(
                "command",
                "command that should be executed on given module. Depends on the module.",
                true,
                std::string(),
                "cmd",
                cmd
                );

    TCLAP::UnlabeledMultiArg<std::string>
            argsArg("args",
                    "arguments to commands",
                    false,
                    "args1 ... argN",
                    cmd);


    try {
        cmd.parse(argn, argv);
    } catch (...) {
        return -1;
    }

    /* get all uncatched arguments and prepend the application name
     * so that we can pass this stripped list to the module's argument parser. */
    std::vector<std::string> args = argsArg.getValue();
    args.insert(args.begin(),
                cmd.getProgramName()+" "
                +moduleArg.getValue()+" "
                +cmdArg.getValue()
                );

    try {

        if(moduleArg.getValue() == "import") {


            if(cmdArg.getValue() == "raw") {
                RawImporter raw(args);
                return 0;
            } else if(cmdArg.getValue() == "meta") {
                RawImporter raw(args, true);
                return 0;
			}
			else if (cmdArg.getValue() == "voodoo") {
				Importer voodoo(args);
				return 0;
			}
			else if (cmdArg.getValue() == "frame"){
				IntrinsicsImport intrinsicsImport(args);
				return 0;
            }

            throw std::runtime_error("Module 'import' requires one of the commands: raw, voodoo");
        } else if (moduleArg.getValue() == "export") {

            throw std::runtime_error("Module 'export' is not implemented.");

        } else if (moduleArg.getValue() == "list") {

            throw std::runtime_error("Module 'list' is not implemented.");

        } else if(moduleArg.getValue() == "track") {
            if(cmdArg.getValue() == "import") {
                TrackImport import(args);
                return 0;
            }


            if(cmdArg.getValue() == "remove") {
                TrackRemover remover(args);
                return 0;
            }

        }

        throw std::runtime_error("Module '"+moduleArg.getValue()+"' not valid.");

    } catch (std::exception & e) {
        std::cerr << "[ERROR][SEQUENCE]: " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cerr << "[ERROR]:  Something really bad happend..." << std::endl;
        return 1;
    }

    return 0;
}
