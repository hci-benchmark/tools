#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>

#include <tclap/CmdLine.h>

#include <hookers/interface/NumericTypes.h>
#include <hookers/interface/Project.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/Track.h>

#include "../common/Arguments.h"


class TrackImport{
public:
    TrackImport(std::vector<std::string> args);

    std::string framePairPath(hookers::interface::Frame frame1,
                              hookers::interface::Frame frame2);
};


class TrackRemover {
public:
    TrackRemover(std::vector<std::string> args);

};

std::string TrackImport::framePairPath(hookers::interface::Frame frame1,
                                       hookers::interface::Frame frame2)
{
    std::stringstream name;

    name << "_tmp%46%46%47images%47"
         << frame1.sequence().name() << "_" << std::setfill('0') << std::setw(6) << frame1.id().object()
         << "%46png_%46%46%47images%47"
         << frame2.sequence().name() << "_" << std::setfill('0') << std::setw(6) << frame2.id().object()
         << "%46png.txt";

    return name.str();
}


TrackRemover::TrackRemover(std::vector<std::string> args)
{
    using namespace hookers::interface;

    TCLAP::CmdLine cmd("boschgt ait annotation importer", ' ', "0.1");


    TCLAP::ValueArg<std::string>
        projectArg(std::string(),
        "project",
        "project name (eg. 20130101_0000)",
        false,
        std::string(),
        "string",
        cmd);



    TCLAP::ValueArg<std::string>
        sequenceArg(std::string(),
        "sequence",
        "sequence name (eg. 0_0000)",
        true,
        std::string(),
        "string",
        cmd);

    TCLAP::ValueArg<std::string>
        framesArg(std::string(),
        "frames",
        "frame numbers",
        true,
        std::string(),
        "string",
        cmd);

    try {
        cmd.parse(args);

    }
    catch (TCLAP::ArgException &e)  {
        std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
        return;
    }


    Project project = Project::open(projectArg.getValue());
    if (!project.isValid()) {
        throw std::runtime_error("[ERROR] Project could not be opened.");
    }

    SequenceList sequences = project.sequences();
    Sequence sequence = sequences.byLabel(sequenceArg.getValue());
    if (!sequence.isValid()) {
        throw std::runtime_error("Sequence '" + sequenceArg.getValue() + "' could not be found in project.");
    }



    FrameList frames = sequence.frames();
    if (
            frames.size() == 0
       )
    {
        std::stringstream err;
        err << "Sequence '" << sequenceArg.getValue()
            << "' doesn not have requested frame " << framesArg.getValue();
        throw std::runtime_error(err.str());
    }

    FrameList removeFrames;
    auto frameIndices = framesFromArgument(framesArg.getValue());
    for(auto i: frameIndices) {
        removeFrames.push_back(frames[i]);
    }

    for(auto f: removeFrames) {
        auto tracks = f.tracks().tracksByCreationContext(Project::ManualContext);
        for(auto t: tracks) {
            f.unlinkTrack(t);
        }
    }


}

TrackImport::TrackImport(std::vector<std::string> args)
{



    using namespace hookers::interface;

    TCLAP::CmdLine cmd("boschgt ait annotation importer", ' ', "0.1");


	TCLAP::ValueArg<std::string>
		projectArg(std::string(),
		"project",
		"project name (eg. 20130101_0000)",
		false,
		std::string(),
		"string",
		cmd);



	TCLAP::ValueArg<std::string>
		sequenceArg(std::string(),
		"sequence",
		"sequence name (eg. 0_0000)",
		true,
		std::string(),
		"string",
		cmd);

    TCLAP::ValueArg<std::string>
        viewArg(std::string(),
        "view",
        "view, eg. base, stereo",
        false,
        std::string("base"),
        "string",
        cmd);

    TCLAP::ValueArg<std::string>
        framesArg(std::string(),
        "frames",
        "frame numbers",
        true,
        std::string(),
        "string",
        cmd);


    /* add the following arguments xored */
    TCLAP::ValueArg<std::string>
        aitAnnotFileArg(std::string(),
        "ait-annot-file",
        "load a single annotation for manual tracks",
        false,
        std::string(),
        "string");
    TCLAP::ValueArg<std::string>
        aitAnnotDirArg(std::string(),
        "ait-annot-dir",
        "load all annotation files from this directory for manual tracks",
        false,
        std::string(),
        "string");

    TCLAP::SwitchArg
        aitAnnotPurgeArg(std::string(),
        "ait-annot-purge",
        "Purge all existing manual tracks",
         cmd,
         false);

    cmd.xorAdd(aitAnnotDirArg,aitAnnotFileArg);

	try {
		cmd.parse(args);

	}
	catch (TCLAP::ArgException &e)  {
		std::cerr << "[ERROR] " << e.error() << " for arg " << e.argId() << std::endl;
		return;
	}

    int FLOW_STEP = 8;

	Project project = Project::open(projectArg.getValue());
	if (!project.isValid()) {
		throw std::runtime_error("[ERROR] Project could not be opened.");
	}

	SequenceList sequences = project.sequences();
	Sequence sequence = sequences.byLabel(sequenceArg.getValue());
	if (!sequence.isValid()) {
		throw std::runtime_error("Sequence '" + sequenceArg.getValue() + "' could not be found in project.");
	}



	FrameList frames = sequence.frames();
    if (
            frames.size() == 0
       )
	{
        std::stringstream err;
        err << "Sequence '" << sequenceArg.getValue()
            << "' doesn not have requested frame " << framesArg.getValue();
        throw std::runtime_error(err.str());
	}


    /* set project context */
    Project::setContext(Project::ManualContext);


    typedef std::pair<hookers::Vector2d, hookers::Vector2d> PosPair;

    struct PosTrackPair {
        PosPair pos;
        Track track;
    };

    FrameList importFrames;
    auto frameIndices = framesFromArgument(framesArg.getValue());
    for(auto i: frameIndices) {
        importFrames.push_back(frames[i]);
    }

    Frame f0 = importFrames[0];
    Frame f1 = importFrames[1];

    if(aitAnnotPurgeArg.isSet()) {
        for(int i = 0; i < importFrames.size(); i++) {
            Frame f = importFrames[i];
            TrackList tl = f.tracks().tracksByCreationContext(Project::ManualContext);
            for(Track &t: tl) {
                if(!t.hasLink())
                    f.unlinkTrack(t);
            }
        }
    }

    if(aitAnnotDirArg.isSet()) {
        /**
         * stores all frame-pair-wise correspondencies that have been read in from
         * annotation files */
        std::unordered_map<Frame, std::vector<PosPair>> readInTracks;

        /**
         * stores the mapping of target position of the current PosPair within the next frame. */
        std::unordered_map<Frame, std::vector<PosTrackPair>> trackMapping;

        /** @todo only for debug purpose */
        std::unordered_map<Track, int> trackOccouranceCount;

        for(int i = 0; i < importFrames.size()-1; i++) {

            Frame f0 = importFrames[i];
            Frame f1 = importFrames[i+1];

            auto fname = this->framePairPath(f0, f1);
            auto path = aitAnnotDirArg.getValue()  + "/" + fname;

            std::ifstream file;
            file.open(path,std::ios_base::in);

            if(!file.is_open()) {
                std::stringstream err;
                err << "ait annotation dir-file '" << path << "' could not be opened.";
                throw std::runtime_error(err.str());
            }

            TrackList tracksInFrame = f0.tracks().tracksByCreationContext(Project::ManualContext);

            std::string line;
            while(std::getline(file, line)) {

                auto countsemi = std::count(line.begin(), line.end(), ';');

                /* must have exactly 4 semicolons */
                if(countsemi != 4)
                    continue;

                std::istringstream ss(line);
                hookers::Vector2d p0;
                ss >> p0[0];
                ss.ignore(1);
                ss >> p0[1];
                ss.ignore(1);

                hookers::Vector2d p1;
                ss >> p1[0];
                ss.ignore(1);
                ss >> p1[1];
                ss.ignore(1);

                float err;
                ss >> err;

                /* if there were any conversation errors, we can discard this line */
                if(ss.fail())
                    continue;

                /* this track is flagged invalid in the text file */
                if(p0.minimum() < 0 || p1.minimum() < 0)
                    continue;

                std::sort(tracksInFrame.begin(), tracksInFrame.end(), [&](const Track &a, const Track &b){
                    return (a.posInFrame(f0)-p0).magnitude() < (b.posInFrame(f0)-p0).magnitude();
                });

                if((tracksInFrame.front().posInFrame(f0)-p0).magnitude() < 0.001) {
//                    if((tracksInFrame.front().posInFrame(f1)-p1).magnitude() < 0.001) {
                        std::cout << "Track has already been imported..." << p0 << tracksInFrame.front().posInFrame(f0)
                                  << tracksInFrame.front().creationContext()
                                  << std::endl;

                        trackMapping[f0].push_back({{p0, p1}, tracksInFrame.front()});
                        continue;
//                    }
                }

                /* okay. this one passed all points of failure,
                 * so insert it into the list of possible candidates. */
                readInTracks[f0].push_back(PosPair(p0, p1));
            }
        }



        auto trackMetric = [](const hookers::Vector2d & a, const hookers::Vector2d & b){
            return false;
        };


        for(int i = 0; i < importFrames.size(); i++) {

            Frame f = importFrames[i];

            std::cout << "doing import stuff for frame: " << i << ", " << f.id().object() << std::endl;

            /* check first if there are tracks
             * that could be used, instead of creating new ones.
             * So in this step we want to match tracks in the previous frame
             * that have close matches of correspondences to this frame. */
            if(!(f == importFrames.front())) {

                /* fetch the predecessor frame */
                Frame fp = importFrames[i-1];

                /* get the track mapping of the former frame */
                auto & tmap = trackMapping[fp];

                /* get the read feature point correspondencies for current frame */
                auto & fmap = readInTracks[f];


                std::cout << f.id().object() << ": not front frame, "
                          << tmap.size() << ", "
                          << fmap.size()
                          << std::endl;

                for(auto it = fmap.begin(); it != fmap.end();) {

                    auto &featurePair = *it;

                    /* find a feature point in the track map of the previous frame that
                         * has a close correspondence in current frame.
                         * we do this by sorting and taking the head entry of the list */
                    std::sort(tmap.begin(), tmap.end(),
                              [&featurePair](const PosTrackPair & a,const PosTrackPair & b){
                        return (featurePair.first-a.pos.second).magnitude() < (featurePair.first-b.pos.second).magnitude();
                    });

                    /* if there are any tracks and the head of the list is close enough, we can take it */
                    if(!tmap.empty() && (tmap.front().pos.second-featurePair.first).magnitude() < 1.0) {

                        /* register the track with the current frame */
                        Track & t = tmap.front().track;
                        f.linkTrack(t, featurePair.first);
                        trackMapping[f].push_back({featurePair, t});
                        trackOccouranceCount[t]++;

                        std::cout << "= " << f.id().object() << ": " << t.id().object() << ", "
                                  << featurePair.first
                                  << tmap.front().pos.second << ", "
                                  << std::endl;

                        it = fmap.erase(it);
                    } else {
                        /* leave it for the next step */
                        it++;
                    }
                }
            }

            /* okay, here we are left with tracks that could not be matched
             * to points in the previous frame */
            for(auto p: readInTracks[f]) {

                auto t = sequence.addTrack();

                trackMapping[f].push_back({p, t});
                trackOccouranceCount[t]++;
                f.linkTrack(t, p.first);

                std::cout << "+ " << f.id().object() << ": " << t.id().object() << ", " << p.first << std::endl;
            }
        }

        for(auto & pair: trackOccouranceCount) {
            std::cout << "Track: " << pair.first.id().object() << ", " << pair.second << std::endl;
        }
    }







    return;

    std::ifstream file;
    file.open(aitAnnotFileArg.getValue());

    if(!file.is_open()) {
        std::stringstream err;
        err << "ait annotation file '" << aitAnnotFileArg.getValue() << "' could not be opened.";
        throw std::runtime_error(err.str());
    }


    TrackList tracksInFrame = f0.tracks().tracksByCreationContext(Project::ManualContext);

    std::string line;
    while(std::getline(file, line)) {

        auto countsemi = std::count(line.begin(), line.end(), ';');


        /* must have exactly 4 semicolons */
        if(countsemi != 4)
            continue;

        std::istringstream ss(line);
        hookers::Vector2d p0;
        ss >> p0[0];
        ss.ignore(1);
        ss >> p0[1];
        ss.ignore(1);

        hookers::Vector2d p1;
        ss >> p1[0];
        ss.ignore(1);
        ss >> p1[1];
        ss.ignore(1);

        float err;
        ss >> err;

        if(ss.fail())
            continue;

        if(p0.minimum() < 0 || p1.minimum() < 0)
            continue;

        std::sort(tracksInFrame.begin(), tracksInFrame.end(), [&](const Track &a, const Track &b){
            return (a.posInFrame(f0)-p0).magnitude() < (b.posInFrame(f0)-p0).magnitude();
        });

        if((tracksInFrame.front().posInFrame(f0)-p0).magnitude() < 0.001) {
            if((tracksInFrame.front().posInFrame(f1)-p1).magnitude() < 0.001) {
                std::cout << "Track has already been imported..." << p0 << tracksInFrame.front().posInFrame(f0)
                          << tracksInFrame.front().creationContext()
                          << std::endl;
                continue;
            }
        }

        auto t = sequence.addTrack();
        f0.linkTrack(t, p0);
        f1.linkTrack(t, p1);

        std::cout << p0 << ", " << p1 << ", " << err << std::endl;

    }
}
