#include "voodoo.h"

using namespace hookers;
using namespace hookers::hdf5;
using namespace hookers::interface;

Importer::Importer(std::vector<std::string> &args)
{
    TCLAP::CmdLine cmd("voodoo track file importer", ' ', "0.1");

    TCLAP::ValueArg<std::string>
        VoodooFile(std::string(),
        "voodoo",
        "hdf5 file from voodootracker",
        true,
        std::string(),
        "complete path to the output of voodoo tracker",
        cmd);

    TCLAP::ValueArg<std::string>
        projectNameArg(std::string(),
        "project",
        "name of the project.",
        false,
        std::string(),
        "string",
        cmd);

    TCLAP::ValueArg<std::string>
        sequenceNameArg(std::string(),
        "sequence",
        "sequence name (eg. 20130101_0000)",
        true,
        std::string(),
        "string",
        cmd);

    cmd.parse(args);

    bool SeqOW = false; //SeqOWArg.getValue();
    bool PartOW = true; //PartOWArg.getValue();


    file.open(VoodooFile.getValue(), vigra::HDF5File::Open);

    std::string projectName = projectNameArg.getValue();
    project = Project::open(projectName);

    std::string sequenceName = sequenceNameArg.getValue();

    sequence = AddingSequence_CheckExistance(sequenceName, SeqOW, project);

    if (!sequence.isValid())
        throw std::runtime_error("Sequence could not be opened nor created.");

    /* get a list of tracks. If it is not empty, there are already tracks
     * present for the sequence and hence we don't want to import the data
     * automaticly */
    TrackList tracks = sequence.tracks();
    if (!tracks.empty())
        throw std::runtime_error("Requested to import tracks to a sequence that already has tracks.");

    /* adds frames if not imported yet */
    Imp_Frame();

    /* save the sequence in case frames were added */
    sequence.save();

    /* get the list of frames */
    frames = sequence.frames();
    if (frames.empty())
        throw std::runtime_error("No frames available to which we could import tracks.");


    std::cout << "[STATUS] Sequence has " << frames.size() << " frames !" << std::endl;



    Imp_Parts(project, file, PartOW);

    /* save the project, in case parts were added */
    project.save();

    parts = project.parts();


    /* If there are no parts at all, then there is something wrong */
    if (parts.empty())
        throw std::runtime_error("Parts are not imported.");


    std::cout << "[STATUS] Starting to import tracks." << std::endl;

    Imp_Tracks();

    std::cout << "[STATUS] Finished importing tracks." << std::endl;

}

Importer::~Importer()
{
    frameCache.clear();
    project.save();
    sequence.save();
}

void Importer::get_dataset_names(vigra::HDF5File & file,
                                 std::vector<std::string> & output,
                                 std::string subgroup)
{
	HDFGroup Group(file.getGroupHandle(subgroup));
	std::list<std::string> datasets;
	datasets = Group.entries();
	output.resize(datasets.size());
	std::copy(datasets.begin(), datasets.end(), output.begin());
}


std::string Importer::ReadAttr(vigra::HDF5Handle m_attr,
                               vigra::HDF5Handle T_handle,
                               std::string attName)
{
	/*
	*Read attributes of the track.
	*/

	/*
	* Get the datatype.
	*/
//	hid_t type = H5Aget_type(m_attr);
//	if (type < 0) {
//		H5Aclose(m_attr);
//	}
//	hid_t memtype = H5Tcopy(H5T_C_S1);

    char stringVal[1024];
	int status;

    //stringVal = new char[H5Tget_size(type)];
	status = H5LTget_attribute_string(T_handle, ".", attName.c_str(), stringVal);

	if (status < 0) {
		std::cout << "can not read the attribute! \n";
	}

//	std::cout << "the attribute is : " << stringVal << "\n";
	std::string attrVal(stringVal);
	return attrVal;
}

void Importer::LinkTrackLandmarks(PartList  & parts, std::string trackId, Track track, vigra::HDF5File & file)
{

    /* get the value of the link attribute */
    const std::string attVal = ReadAttr(file.getAttributeHandle(trackId, "link"), file.getDatasetHandle(trackId), "link");

    /* use regex to find the part name and landmark number */
    std::regex rgx("/parts/(.*)/features/([0-9]*)");
    std::smatch match;

    /* check if a match can be found */
    if (!std::regex_search(attVal.begin(), attVal.end(), match, rgx)
        || (match[1].length() <= 0 || match[2].length() <= 0) ) {
        std::cout << "[INFO] landmark path is not valid: " << attVal << std::endl;
        return;
    }

    /* read in part name and landmark number */
    std::string partName = match[1];
    std::string landmarkNumberString = match[2];
    std::stringstream landmarkNumberStream(landmarkNumberString);
    int landmarkNumber = -1;
    landmarkNumberStream >> landmarkNumber;

    /* substract number by one, because landmark indices are one-based in
     * voodoo tracker file */
    landmarkNumber -= 1;


    /* get a part with given name and check if it is valid */
    Part p = parts.byLabel(partName);
    if(!p.isValid()) {
        std::cout << "[INFO] Not linking, could not find Part with name: "
                  << partName << std::endl;
    }

    /* try to find a landmark with given id on given part */
    LandmarkList landmarks = p.landmarks();
    if(landmarkNumber < 0 || landmarkNumber >= (int)landmarks.size()) {
        std::cout << "[INFO] Not linking, could not find landmark on part "
                  << partName << " with number " << landmarkNumber << "/" << landmarks.size()
                  << std::endl;
    }



    /* link track to landmark if everything could be found */
    track.linkToLandmark(landmarks[landmarkNumber]);

    std::cout << "[INFO] Linking track " << trackId << " with landmark "
              << partName << "/" << landmarkNumber << std::endl;


}

Sequence Importer::AddingSequence_CheckExistance(std::string seqeunceName, bool SeqOW, Project & project)
{
	SequenceList sequences = project.sequences();
	Sequence dummysequence = sequences.byLabel(seqeunceName);

    if (dummysequence.isValid())
        return dummysequence;

    Sequence sequence = project.addSequence(seqeunceName);

    std::cout << "[STATUS] A new sequence with name \""
              << seqeunceName
              << "\" is added" << std::endl;

    return sequence;


}

void Importer::Imp_Frame()
{

    /*add empty frames*/
	FrameList frames = sequence.frames();

	std::vector<std::string> frames_Number;
	get_dataset_names(file, frames_Number, "/frames");

    /* check if there are already frames in the sequence. In this case
     * we augment the existing frames with tracks.
     * However, this requires that the number of frames in the sequence and
     * in the file to import are matching */
    if (!frames.empty()) {

        /* in the case the number is the same, we can return here */
        if(frames.size() == frames_Number.size())
            return;

        throw std::runtime_error("Number of frames in sequence and in old file don't match.");
    }

    USETICTOC

    /* keep reference to last frame in order to hold frame data cache in memory */
    Frame lastFrame;

    /* create as much frames as are in the voodoo hdf file */
    for (size_t ii = 0; ii < frames_Number.size(); ++ii) {
        TIC
        lastFrame = sequence.addFrame();
        TOC
        std::cout << "frame" << ii << "/" << frames_Number.size() << " is added..." << std::endl;
    }

}




herr_t Importer::iterate_tracks (
        hid_t loc_id,
        const char *name,
        const H5L_info_t *linfo,
        void *opdata)
{

    Importer * imp = (Importer*) opdata;

    /* add a new track to the sequence */
    imp->newTrack = imp->sequence.addTrack();

    /* get the frame links from the dataset of the old file */
    vigra::MultiArray<2, double> tmp;

    std::string trackID = std::string(name);
    std::string trackPath = std::string("/tracks/") + trackID;


    imp->file.readAndResize(trackPath, tmp);


    //int track_id = std::atoi(dsets[ii].c_str());
    //std::cout << "injam004 " << tmp(1,0)<<","<<tmp(2,0) <<"\n";

    if (tmp.shape(1) == 0)
        std::cerr << "Dataset #" << imp->ii << " : " << trackID << " is empty!" << std::endl;

    /* and link the track to all frames that are linked in the old file */
    for (int jj = 0; jj < tmp.shape(1); ++jj)
    {
        /* frames in voodo file are one-based indices */
        int frame_Number = tmp(0, jj)-1;

        /* check if the frame number is in range. if not, don't link it */
        if(frame_Number < 0 || frame_Number >= (int)imp->frames.size())
            continue;

        Vector2d posInFrame(tmp(1, jj), tmp(2, jj));
        Frame frame = imp->frames[frame_Number];
        frame.linkTrack(imp->newTrack, posInFrame);

        /* add each frame we process here to the cache.
         * this object cache makes sure not all the frame data is loaded
         * and unloaded each time it is used */
        imp->frameCache.add(frame);


    }

    /* finally check if the track has been annotated and
     * link it to the corresponding landmark as well */
    if (imp->file.existsAttribute(trackPath, "link")) {
        //std::cout << "track should be linked" << std::endl;
        imp->LinkTrackLandmarks(imp->parts, trackPath, imp->newTrack, imp->file);
    }

//        std::cout << "track " << trackID << ": " << imp->ii << "/" << imp->nobj << " is imported..." << std::endl;
    imp->ii++;
    return 0;
}

void Importer::Imp_Tracks()
{

    vigra::HDF5Handle grpHandle = file.getGroupHandle("/tracks");
    herr_t err = H5Gget_num_objs(grpHandle, &nobj);
    std::cout << "total number of tracks are : " << nobj << "/" << err << std::endl;


    /* calls iterate_tracks as callback handler */
    H5Literate(grpHandle,
               H5_INDEX_NAME,
               H5_ITER_NATIVE,
               nullptr,
               iterate_tracks,
               this);

}


void Importer::Imp_Parts(Project &project, vigra::HDF5File & file, bool PartOW)
{

    /*Import parts to a project from HDF file*/
    std::vector<std::string> partNames;
    get_dataset_names(file, partNames, "/parts");

    PartList parts = project.parts();

    if (parts.size() == 0)
        std::cout << "[INFO] Sequence has no parts yet." << std::endl;

    /* walk through each part and check if we need to add it to the project */
    for (size_t i = 0; i < partNames.size(); i++)
    {
        Part p = parts.byLabel(partNames[i]);

        std::string featurePath = std::string("/parts/")
                + partNames[i]
                + std::string("/features");

        std::vector<std::string> landmarkNames;
        get_dataset_names(file, landmarkNames, featurePath);

        if(p.isValid()) {
            /* there is such a part. now check if the number of landmarks in voodoo file
             * and in project file is the same. If not, stop processing */

            if(p.landmarks().size() != landmarkNames.size())
                throw std::runtime_error("Number of landmarks mismatch for part:  " + partNames[i]);

            /* everything is okay, we can go on to the next part */
            continue;
        }

        /* there is no such a part. create it */
//        p = project.addPart(partNames[i]);

        if(!p.isValid())
            return;


        for (size_t ii = 0; ii < landmarkNames.size(); ii++)
        {
            /*instead of using land_markname directly ss drive to import them in correct order*/
            std::stringstream ss;
            ss << (ii + 1);
            std::string landmarkNumber = ss.str();

            vigra::MultiArray<2, float> tmp;
            std::string landmarkPath = featurePath + std::string("/") + landmarkNumber;

            file.readAndResize(landmarkPath, tmp);
            vigra::TinyVector<float, 3> pose(tmp(0, 0), tmp(1, 0), tmp(2, 0));
            /** @todo enable this again via nearest neighbour search */
//            p.addLandmark(pose);
        }

        std::cout << "[STATUS] Added " << landmarkNames.size()
                  << " for landmarks for part " << partNames[i] << std::endl;

    }

}


