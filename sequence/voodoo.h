#ifndef BOSCHGT_PROJECT_TOOLS_SEQUENCE_VOODOOIMPORTER_H
#define BOSCHGT_PROJECT_TOOLS_SEQUENCE_VOODOOIMPORTER_H


#include <regex>
#include <set>
#include <iostream>
#include <string>
#include <tclap/CmdLine.h>
#include <sstream>

/*//Qt libraries
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QDebug>*/

//project designed library
#include <hookers/interface/Project.h>
#include <hookers/interface/ProjectData.h>
#include <hookers/interface/Sequence.h>
#include <hookers/interface/SequenceList.h>
#include <hookers/interface/SequenceData.h>
#include <hookers/interface/Frame.h>
#include <hookers/interface/FrameList.h>
#include <hookers/interface/FrameData.h>
#include <hookers/interface/Track.h>
#include <hookers/interface/TrackList.h>
#include <hookers/interface/TrackData.h>
#include <hookers/interface/Landmark.h>
#include <hookers/interface/LandmarkList.h>
#include <hookers/interface/LandmarkData.h>
#include <hookers/interface/backends/hdf/HDFGroup.h>
#include <hookers/interface/backends/hdf/HDFAttribute.h>

//
#include <hdf5.h>

//Vigra libraries
#include <vigra/multi_array.hxx>
#include <vigra/linear_algebra.hxx>
#include <vigra/tinyvector.hxx>
#include <vigra/hdf5impex.hxx>
#include <vigra/timing.hxx>

//OpenCV libreries
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv/cv.hpp>


using namespace hookers;
using namespace hookers::interface;

/**
 * This class is designed to take HDF files which have information about tracks
 * and annotated landmarks that were used in former versions of the datastorage.
 * Then convert it to new data interface.
 * @author Mohsen RM
 * @date April 2015
 */
class Importer {

public:
    Project project;
    Sequence sequence;
    FrameList frames;
    PartList parts;
    vigra::HDF5File file;

    Track newTrack;
    int ii = 0;
    hsize_t nobj;

    /* this is a set which holds a certain number of frames.
     * so that they don't go out of scope for each track iteration.
     * this avoids cache pruning etc.. */
    ObjectCache<Frame, 1000> frameCache;

    Importer(std::vector<std::string> & args);

    ~Importer();

    void get_dataset_names(vigra::HDF5File & file,
                           std::vector<std::string> & output,
                           std::string subgroup = "/");


    std::string ReadAttr(vigra::HDF5Handle m_attr, vigra::HDF5Handle T_handle,
                         std::string attName);

    void LinkTrackLandmarks(PartList  & parts, std::string trackId,
                            Track track, vigra::HDF5File & file);

    Sequence AddingSequence_CheckExistance(std::string seqeunceName,
                                           bool SeqOW, Project & project);

    void Imp_Frame();

    static herr_t iterate_tracks (
            hid_t loc_id,
            const char *name,
            const H5L_info_t *linfo,
            void *opdata);

    void Imp_Tracks();

    void Imp_Parts(Project &project, vigra::HDF5File & file, bool PartOW);

};





#endif // BOSCHGT_PROJECT_TOOLS_SEQUENCE_VOODOOIMPORTER_H
